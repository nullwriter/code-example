"use strict";

import jQuery from 'jquery';
import Alert from 'lib/Alert.js';

jQuery('button[name=labLocation]').on('click', (event) => {
    var button = jQuery(event.currentTarget);

    jQuery.post({
        url: '/staff/store-lab-location',
        data: {csrf: csrfToken, labLocation: button.val()}
    }).done((data) => {
        window.location.href = '/staff';
    }).fail((e) => {
        new Alert(e.responseText);
    });
});
