"use strict";

import 'css/staff.css';
import Alert from 'lib/Alert.js';


if(!window.labLocationId){
    jQuery('.change-location-btn').hide();
}

jQuery('.change-location-btn').on('click', function() {
    let changeLocationBtns = jQuery('.change-location-btn');
    let selectLabLocations = '#lab-location-options';
    let closeBtn = '#close-lab-location-options';

    jQuery.get({
        url: '/lab-location/get-all'
    }).done((data) => {
        let labLocations = data.labLocations;
        jQuery(changeLocationBtns).hide();

        addLabLocationsToSelect(labLocations, selectLabLocations);

        jQuery(selectLabLocations).show();
        jQuery(closeBtn).show();
    }).fail((e) => {
        new Alert(e.responseText);
    });
});

jQuery('#close-lab-location-options').on('click', function() {
    jQuery('#lab-location-options').hide();
    jQuery(this).hide();
    jQuery('.change-location-btn').show();
});

jQuery('#lab-location-options').on('change', function() {

    let locationId = jQuery(this).val();
    if (!locationId) {
        return;
    }

    jQuery.post({
        url: '/staff/store-lab-location',
        data: {csrf:csrfToken, labLocation:locationId}
    }).done((data) => {
        window.location.reload();
    }).fail((e) => {
        new Alert(e.responseText);
    });
});

function addLabLocationsToSelect(labLocations, selectElement) {

    if (jQuery(selectElement + ' option').length > 0) {
        return;
    }

    jQuery(selectElement).append(new Option('Select Lab Location', '', true, true));
    jQuery.each(labLocations, function (val, text) {
        if (text['id'] !== window.labLocationId) {
            jQuery(selectElement).append(new Option(text['name'], text['id']));
        }
    });
}
