"use strict";

import jQuery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import 'css/set-billboard-message.css';
import Alert from 'lib/Alert.js';
import Howler from 'howler';
import errorAudioUrl from 'audio/error.mp3';

const { Howl } = Howler;

var errorSound = new Howl({src: [errorAudioUrl]});

var addErrorMessage = (errorMessage) => {
    new Alert(errorMessage);
    errorSound.play();
};

jQuery((() => {
    var currentMessage = jQuery('#currentMessage');
    var customMessage = jQuery('#customMessage');
    var selectLabLocation = jQuery('#labLocation');
    var currentLabLocationId = jQuery( "#labLocation option:selected" ).data('id');
    var currentLabLocationMessage = jQuery( "#labLocation option:selected" ).val();

    /**
     * New message to be saved
     * @param message
     */
    var updateCustomMessage = (message) => {
        customMessage.text(message);
    };

    /**
     * Current message saved in Lab Location
     * @param message
     */
    var updateCurrentMessage = (message) => {
        currentMessage.text(message);
    };

    /**
     * Action to send new message into Database
     * @param message
     */
    var saveCustomMessage = (message) => {

        jQuery.post({
            url: '/staff/admin/api/update-lab-location-message',
            data: {csrf: csrfToken, labLocationId:currentLabLocationId, message:message}
        }).done((data) => {
            updateCurrentMessage(data);
            jQuery('option[data-id="'+currentLabLocationId+'"]').val(message);
        }).fail((e) => {
            addErrorMessage(
                'Error trying to update custom message.'
            );
            console.log("Error trying to update custom message.");
        });
    };

    /**
     * On change event capture for LabLocation
     */
    selectLabLocation.on('change', (event) => {
        currentLabLocationId = jQuery("#labLocation option:selected").data('id');
        currentLabLocationMessage = jQuery("#labLocation option:selected").val();
        customMessage.text('');
        updateCurrentMessage(currentLabLocationMessage);
    });

    /**
     * Form submit event capture
     */
    jQuery('form#new-custom-message').on('submit', (event) => {
        event.preventDefault();
        saveCustomMessage(jQuery('#customMessage').val());
    });

    /**
     * Pre-set messages event capture
     */
    jQuery(document).on('click', '.set-message-btn', (event) => {
        event.preventDefault();
        var button = jQuery(event.currentTarget);
        updateCustomMessage(button.data('text').trim());
    });

}));
