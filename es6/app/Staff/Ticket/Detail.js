"use strict";

import jQuery from 'jquery';
import ChangeStatus from 'lib/Staff/Ticket/ChangeStatus.js';
import Create from 'lib/Staff/Ticket/Create.js';
import Edit from 'lib/Staff/Ticket/Edit.js';
import Tabs from 'lib/Staff/Ticket/Tabs.js';

jQuery((() => {
    jQuery('#createTicket').on('click', function (e) {
        e.preventDefault();
        new Create(createFormDefaults);
    });

    jQuery('a.statusActionLink').on('click', function (e) {
        e.preventDefault();
        new ChangeStatus();
    });

    jQuery('#editTicket').on('click', function (e) {
        e.preventDefault();
        new Edit(createFormDefaults, editForm);
    });

    new Tabs(editForm, unit, workRequestTypes);
}));
