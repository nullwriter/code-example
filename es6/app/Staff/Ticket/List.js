"use strict";

import jQuery from 'jquery';
import List from 'lib/Staff/List';
import Create from 'lib/Staff/Ticket/Create.js';

jQuery((() => {
    new List(
        jQuery('#tickets'),
        '/staff/ticket/api/list-search',
        {
            'ticket_id': {
                render: (ticketId) => {
                    return '';
                },
                searchable: false,
                orderable: false
            },
            'ticket_display_id': {
                render: function (ticketDisplayId, type, row) {
                    return ' <a href="/staff/ticket/detail/' + row['ticket_id'] + '">'
                        + ticketDisplayId
                        + '</a>';
                },
                class: 'entity-hyperlink',
            },
            'unit_identifier': {
                render: (unitIdentifier, type, row) => {
                    return '<a href="/staff/unit/detail/' + row['unit_id'] + '">'
                        + unitIdentifier
                        + '</a>';
                }
            }
        },
        {
            'check_in_date': 'desc'
        }
    );
}));

jQuery('#createTicket').on('click', function (e) {
    new Create(createFormDefaults);
});
