"use strict";

import jQuery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import 'css/project-detail.css';
import 'css/create-project.css';
import Create from 'lib/Staff/Project/Create.js';
import Edit from "lib/Staff/Project/Edit.js";
import Common from 'lib/Staff/Project/Common.js';
import AddBuild from 'lib/Staff/Project/Build/Add.js';
import EditBuild from 'lib/Staff/Project/Build/Edit.js';


jQuery((() => {

    jQuery('a.change-project-status').on('click', (e) => {
        let button = jQuery(e.currentTarget);
        let projectId = jQuery('#project-id').val();
        let status = button.data('status');

        jQuery.post({
            url: '/staff/project/api/change-status/' + projectId,
            data: {
                csrf: csrfToken,
                projectStatus: status
            }
        }).done(() => {
            window.location.reload();
        }).fail((e) => {
            Common.addErrorMessage(e.responseText);
        });
    });

    jQuery('i.toggle-content').on('click', (e) => {
        let toggle = jQuery(e.currentTarget);
        toggle.closest('tr').next('tr').fadeToggle().children('td').children('table').slideToggle();

        let removeIcon = toggle.parent('td').parent('tr').children('td.build-list-actions').children('i.remove-build');
        let titleWrapper = jQuery(this).parent('td').parent('tr');

        // @todo why is the CSS styles handled in here? That should be a class in a CSS file.
        if (toggle.hasClass('glyphicon-triangle-right')) {
            toggle.removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
            titleWrapper.css('background-color','rgba(62, 168, 233, 0.19)');

            if (removeIcon.length > 0) {
                removeIcon.show();
            }

            return;
        }

        toggle.removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
        titleWrapper.css('background-color','');

        if (removeIcon.length > 0) {
            removeIcon.hide();
        }
    });

    jQuery('#add-project-btn').on('click', function(e) {
        e.preventDefault();
        new Create();
    });

    jQuery('#edit-project-btn').on('click', function(e) {
        e.preventDefault();
        new Edit(editFormDefaults);
    });

    jQuery('#add-build-level').on('click', (e) => {
        e.preventDefault();
        new AddBuild();
    });

    jQuery('.edit-build').on('click', function(e) {
        e.preventDefault();
        new EditBuild(jQuery(this).data('id'));
    });
}));
