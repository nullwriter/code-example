"use strict";

import jQuery from 'jquery';
import 'css/create-project.css';
import List from 'lib/Staff/List';
import Create from 'lib/Staff/Project/Create.js';

jQuery((() => {
    new List(
        jQuery('#projects'),
        '/staff/project/api/list-search',
        {
            'project_id': {
                render: (projectId) => {
                    return '';
                },
                searchable: false,
                orderable: false
            },
            'project': {
                render: (projectNamem, type, row) => {
                    return ' <a' +
                        ' href="/staff/project/detail/' + row['project_id'] + '">'
                        + row['project']
                        + '</a>';
                },
                class: 'entity-hyperlink',
            },
        },
        {
            'project': 'asc'
        }
    );
}));

jQuery('#add-project-btn').on('click', function(e) {
    new Create();
});
