"use strict";

import instructionsTab from 'html/Staff/Rework/InstructionsTab.html';
import attachmentsTab from 'html/Staff/Rework/AttachmentsTab.html';
import Edit from 'lib/Staff/Rework/Edit.js';
import Create from 'lib/Staff/Rework/Create.js';
import Alert from 'lib/Alert.js';

jQuery((() => {
    showInstructionsTab();

    jQuery('a[data-toggle="pill"]').on('show.bs.tab', function (e) {
        switch (jQuery(e.target).attr("href")) {
            case '#attachmentsTab':
                showAttachmentsTab();
                break;

            case '#instructionsTab':
            default:
                showInstructionsTab();
                break;
        }
    });

    jQuery('button#editRework').on('click', function () {
        if (isStaffPerson) {
            new Edit(rework);
        } else {
            new Alert('Only EPMs or Admin/Managers can edit a Rework.');
        }
    });

    jQuery('button#createRework').on('click', function () {
        if (isStaffPerson) {
            new Create();
        } else {
            new Alert('Only EPMs or Admin/Managers can create a Rework.');
        }
    });
}));

let tabContainer = jQuery('.tab-content');

function showAttachmentsTab() {
    tabContainer.html(attachmentsTab);
}

function showInstructionsTab() {
    tabContainer.html(instructionsTab);

    if (rework.instructions.areInstructionsSupplied) {
        tabContainer.find('input[name="instructions-supplied"]').prop('checked', 'checked');
    }

    tabContainer.find('div.apple-window-body').html(rework.instructions.description);
    tabContainer.find('#received-how-select').html(rework.instructions.source);

    if (rework.instructions.arePartsRequired) {
        tabContainer.find('input[name="parts-required"]').prop('checked', 'checked');
    }

    if (rework.instructions.wasInspected) {
        tabContainer.find('input[name="inspected"]').prop('checked', 'checked');
    }

    tabContainer
        .find(`input[name="inspection-type"][value="${rework.instructions.inspectionType}"]`)
        .prop('checked', 'checked');

    tabContainer.find('#radar-issue-number').html(rework.instructions.radarIssueNumber);
}