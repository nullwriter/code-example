"use strict";

import jQuery from 'jquery';
import List from 'lib/Staff/List';
import Create from 'lib/Staff/Rework/Create.js';
import Alert from 'lib/Alert.js';

jQuery((() => {
    new List(
        jQuery('#reworks'),
        '/staff/rework/api/list-search',
        {
            'rework_id': {
                render: (reworkId) => {
                    return '<a href="/staff/rework/detail/' + reworkId + '">'
                        + '<span class="glyphicon glyphicon-eye-open"></span>'
                        + '</a>';
                },
                searchable: false,
                orderable: false
            }
        },
        {
            'name': 'asc'
        }
    );

   jQuery('#create-rework-btn').on('click', function(){
       jQuery.get({
           url: '/staff/get-is-staff-person',
       }).done((data, textStatus, xhr) => {
           new Create();
       }).fail((e) => {
           new Alert('Only EPMs or Admin/Managers can create a Rework.');
       });
   });
}));
