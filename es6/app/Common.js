"use strict";

// @todo may be more fine-grained to only include what's needed
import 'bootstrap/dist/css/bootstrap.css';
import 'css/glyphicon-custom.css';
import 'css/datatable-custom.css';
import 'css/fonts.css';
import 'css/common.css';
import 'css/modal.css';
import confirmExitModalHtml from 'html/common/ConfirmExitModal.html';

var modal = jQuery(confirmExitModalHtml);
jQuery('body').append(confirmExitModalHtml);
jQuery(document).on("click", "#confirm-exit", function () {
    jQuery("#exit-form").submit();
});
