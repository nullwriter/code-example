"use strict";

import jQuery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import MessageFormat from 'messageformat/messageformat.js';

const templates = {
    // Build in messages from Formidable
    'error.required': 'This field is required',
    'error.empty': 'Value must not be empty',
    'error.integer': 'Integer value expected',
    'error.float': 'Float value expected',
    'error.boolean': 'Boolean value expected',
    'error.date': 'Date value expected',
    'error.time': 'Time value expected',
    'error.date-time': 'Datetime value expected',
    'error.email-address': 'Valid email address required',
    'error.min-length': 'Minimum length is {lengthLimit, plural, one {# character} other {# characters}}',
    'error.max-length': 'Maximum length is {lengthLimit, plural, one {# character} other {# characters}}',
    'error.min-number': 'Minimum value is {limit, number}',
    'error.max-number': 'Maximum value is {limit, number}',
    'error.step-number': 'Value is invalid, closest valid values are {lowValue, number} and {highValue, number}',

    // Messages from our own constraints
    'error.staff-person': 'Person is not a staff member',
    'error.status-action': 'Invalid status action',
    'error.work-request-type': 'Invalid work request type',
    'error.work-request-incomplete': 'One or more work requests are incomplete.',
    'error.unit-not-picked-up': 'Unit has not been picked up.',
    'error.multiple-distribute-unit-work-requests': 'There must only be one "Distribute Unit" work request',
    'error.multiple-build-unit-work-requests': 'There must only be one "Build Unit" work request',
    'error.required-unit-identifier': 'At least one unit identifier is required',
    'error.superfluous-unit-identifier': 'An invalid unit identifier has been submitted',
    'error.unit-i-tracked': 'Unit is already iTracked',
    'error.hrp-number-exists': 'Unit with this HRP number already exists',
    'error.override-password': 'Valid override password required',
    'error.unit-project-change': 'Unit is not on the project',
    'error.unit-number-exists-in-build': 'Unit number exists in build',
    'error.work-request-completed-by-empty': 'Completed By must be filled in when the Work Request is marked complete.',
    'error.work-request-completed-by-not-empty':
        'Completed By must be empty when the Work Request is not marked complete.',

    // Messages from our own formatters
    'error.ecid': 'Valid ECID required',
    'error.mac-address': 'Valid MAC address required',
    'error.aou-number': 'Valid AOU number required',
    'error.hrp-number': 'Valid HRP number required',
    'error.mlb-number': 'Valid MLB number required',
    'error.mm-number': 'Valid MM number required',
    'error.mt-number': 'Valid MT number required',
    'error.pse-number': 'Valid PSE number required',
    'error.pt-number': 'Valid PT number required',
    'error.serial-number': 'Valid serial number required',
    'error.build': 'Valid build required',
    'error.lab-location': 'Valid lab location required',
    'error.person': 'Valid person required',
    'error.priority': 'Valid priority required',
    'error.rework': 'Valid rework required',

    // Messages from middlewares
    'error.transition-ticket': 'Could not transition ticket',
};

let formatter = new MessageFormat('en').setIntlSupport(true);

export default class FormHelpers {
    addErrors(form, errors) {
        // remove global errors
        let globalErrorsList = form.find('ul.global-errors');
        let globalErrorsWrapper = globalErrorsList.closest('div.form-group');

        globalErrorsList.find('li').remove();
        globalErrorsWrapper.hide();

        // remove form errors
        form.find('.form-group.has-error').removeClass('has-error');
        form.find('.form-group').find('.help-block').remove();

        let hasGlobalErrors = false;

        if (undefined !== errors && errors.length) {
            errors.forEach((error) => {
                if ('' === error.key) {
                    hasGlobalErrors = true;
                    globalErrorsList.append(jQuery('<li/>').text(this._formatError(error)));
                    return;
                }

                let input = form.find(':input#input\\.' + error.key + ':first');

                if (0 === input.length) {
                    input = form.find(':input[name="' + error.key + '"]:first');
                }

                let formGroup = input.closest('.form-group');

                if (0 === formGroup.length) {
                    return;
                }

                formGroup.addClass('has-error');
                formGroup.append(jQuery('<span class="help-block"/>').text(this._formatError(error)));
            });
        }

        if (hasGlobalErrors) {
            globalErrorsWrapper.show();
        }
    }

    addGlobalError(form, errorText) {
        // remove global errors
        let globalErrorsList = form.find('ul.global-errors');
        let globalErrorsWrapper = globalErrorsList.closest('div.form-group');

        globalErrorsList.find('li').remove();
        globalErrorsWrapper.hide();

        // remove form errors
        form.find('.form-group.has-error').removeClass('has-error');
        form.find('.form-group').find('.help-block').remove();

        globalErrorsList.append(jQuery('<li/>').text(errorText));
        globalErrorsWrapper.show();
    }

    _formatError(error) {
        if (!(error.message in templates)) {
            // Message key was either not defined in the templates or is actually a human-readable message.
            return error.message;
        }

        return formatter.compile(templates[error.message])(error.arguments);
    }

    setSelectOptions(select, options) {
        select.find('option').remove();
        jQuery.each(options, function (index, option) {
            let optionVars = {
                value: option.value,
                text: option.label
            };
            if (option.disabled) {
                optionVars.disabled = 'disabled';
            }
            select.append(jQuery('<option>', optionVars));
        });

    }
};
