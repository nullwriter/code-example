"use strict";

import jQuery from 'jquery';
import 'css/alert.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'remarkable-bootstrap-notify';

export default class Alert {
    constructor(message, type, callback) {
        this.type = type || 'error';
        this.message = message;
        this.callback = callback;

        if ('error' === this.type) {
            this._errorNotify();
        } else {
            this._successNotify();
        }
    }

    _errorNotify() {
        jQuery.notify(
            {
                icon: 'glyphicon glyphicon-exclamation-sign glyphicon-2x',
                message: this.message
            },
            {
                type: 'danger',
                delay: 6000,
                z_index: 50000,
                onClosed: this.callback,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                template: '<div role="alert" data-notify="container" class="ns-box ns-bar ns-effect-slidetop ns-type-error ns-show">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> <span data-notify="title">{1}</span>' +
                '<p data-notify="message">{2}</p>' +
                '<a href="#" target="_blank" data-notify="url"></a></div>'
            }
        );
    }

    _successNotify() {
        jQuery.notify(
            {
                icon: 'glyphicon glyphicon-ok-sign glyphicon-2x',
                message: this.message
            },
            {
                type: 'success',
                delay: 6000,
                z_index: 50000,
                onClosed: this.callback,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                template: '<div role="alert" data-notify="container" class="ns-box ns-bar ns-effect-slidetop ns-type-success ns-show">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> <span data-notify="title">{1}</span>' +
                '<p data-notify="message">{2}</p>' +
                '<a href="#" target="_blank" data-notify="url"></a></div>'
            }
        );
    }
};
