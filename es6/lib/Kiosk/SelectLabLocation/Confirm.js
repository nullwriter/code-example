"use strict";

import jQuery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import BadgeScan from 'lib/Kiosk/BadgeScan.js';
import Alert from 'lib/Alert.js';
import confirmModalHtml from 'html/Kiosk/ConfirmLabLocationModal.html';

export default class Confirm {
    constructor(labLocationId, labLocationName) {
        this._confirmModal = jQuery(confirmModalHtml);
        this._labLocationId = labLocationId;
        this._badgeScan = null;

        this._confirmModal.off('hidden.bs.modal').on('hidden.bs.modal', () => {
            this._disableBadgeScan();
        });

        this._confirmModal.find('.lab-location').text(labLocationName);

        this._enableBadgeScan();
        this._confirmModal.modal();
    }

    _handleResponse(success, errorMessage) {
        if (!success) {
            new Alert(errorMessage);
            return;
        }

        window.location.href = '/kiosk';
    }

    _enableBadgeScan() {
        this._badgeScan = new BadgeScan(
            '/kiosk/store-lab-location',
            csrfToken,
            this._handleResponse.bind(this),
            {
                labLocation: this._labLocationId
            }
        );
    }

    _disableBadgeScan() {
        if (null === this._badgeScan) {
            return;
        }

        this._badgeScan.destroy();
        this._badgeScan = null;
    }
};
