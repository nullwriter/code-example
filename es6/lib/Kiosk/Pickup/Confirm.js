"use strict";

import jQuery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import BadgeScan from 'lib/Kiosk/BadgeScan.js';
import Alert from 'lib/Alert.js';
import confirmModalHtml from 'html/Kiosk/ConfirmPickupUnitsModal.html';

export default class Confirm {
    constructor(ticketIds, backCallback, successCallback) {
        this._confirmModal = jQuery(confirmModalHtml);
        this._ticketIds = ticketIds;
        this._backCallback = backCallback;
        this._successCallback = successCallback;
        this._badgeScan = null;

        this._confirmModal.off('hidden.bs.modal').on('hidden.bs.modal', () => {
            this._disableBadgeScan();
            this._backCallback();
        });

        this._confirmModal.find('.total-units').text(this._ticketIds.length);

        this._enableBadgeScan();
        this._confirmModal.modal();
    }

    _handleResponse(success, errorMessage, data) {
        if (!success) {
            new Alert(errorMessage);
            return;
        }

        this._successCallback();
        window.location.href = '/kiosk/pickup/summary/' + this._ticketIds.length;
    }

    _enableBadgeScan() {
        this._badgeScan = new BadgeScan(
            '/kiosk/pickup/confirm',
            csrfToken,
            this._handleResponse.bind(this),
            {
                tickets: this._ticketIds
            }
        );
    }

    _disableBadgeScan() {
        if (null === this._badgeScan) {
            return;
        }

        this._badgeScan.destroy();
        this._badgeScan = null;
    }
};
