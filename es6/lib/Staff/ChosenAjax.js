"use strict";

import jQuery from 'jquery';
import 'chosen-js/chosen.min.css';
import 'chosen-js/chosen.jquery.js';

export default class ChosenAjax {
    constructor(selectElement, url, callback, options) {
        this._currentRequest = null;

        selectElement.chosen({
            width: "100%",
            no_results_text: '<i class="glyphicon glyphicon-refresh glyphicon-spin" aria-hidden="true"></i> Searching for '
        });

        let chosenElement = selectElement.next();
        let selectFilter = chosenElement.find('input.chosen-search-input');

        if (options) {
            jQuery.each(options, (index, value) => {
                selectElement.append(jQuery('<option>', callback(index, value)));
            });
            selectElement.trigger("chosen:updated");
        } else {
            selectFilter.on('keyup', (e) => {
                let searchValue = jQuery(e.currentTarget).val();

                if ('' === searchValue) {
                    return;
                }

                this._currentRequest = jQuery.ajax({
                    type: 'GET',
                    url: url + searchValue,
                    beforeSend: (e) => {
                        if (this._currentRequest !== null) {
                            this._currentRequest.abort();
                        }
                    },
                    success: (data) => {
                        if (data.persons.length) {
                            selectElement.find('option').remove();
                            jQuery.each(data.persons, (index, person) => {
                                selectElement.append(jQuery('<option>', callback(index, person)));
                            });

                            selectElement.trigger("chosen:updated");
                            selectFilter.val(searchValue);
                        } else {
                            chosenElement.find('li.no-results')
                                .html('<span class="text-danger">No results found.</span>');
                        }
                    }
                });
            });
        }
    }
};
