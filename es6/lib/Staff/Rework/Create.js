"use strict";

import createReworkModal from 'html/Staff/Rework/CreateEditModal.html';
import ChosenAjax from 'lib/Staff/ChosenAjax.js';
import Alert from 'lib/Alert.js';
import FormHelpers from 'lib/FormHelpers.js';

export default class Create {

    constructor() {
        let body = jQuery('body');
        this._createReworkModal = jQuery(createReworkModal);
        body.append(this._createReworkModal);

        this._createReworkModal.find('.modal-title').html('Create Rework');
        this._formHelpers = new FormHelpers();

        new ChosenAjax(
            this._createReworkModal.find('#submitted-by'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        new ChosenAjax(
            this._createReworkModal.find('#technical-contact'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        new ChosenAjax(
            this._createReworkModal.find('#approved-by'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        this._createReworkModal.find('#project').trigger('change');
        this._createReworkModal
            .find('#project').off('change').on('change', this._updateBuildList.bind(this));

        new ChosenAjax(
            this._createReworkModal.find('#project'),
            '',
            function(index, project) {
                return {
                    value: index,
                    text: project.name
                };
            },
            projects
        );

        this._createReworkModal
            .find('button#save-rework').off('click').on('click', this._submitForm.bind(this));

        this._createReworkModal.modal();
    }

    _submitForm() {
        this._createReworkModal.find('button#save-rework').prop('disabled', true);
        jQuery.post({
            url: '/staff/rework/api/create',
            data: {csrf: csrfToken,
                name: this._createReworkModal.find('#name').val(),
                active: this._createReworkModal.find('#is-active').is(':checked'),
                project: this._createReworkModal.find('#project').chosen().val(),
                componentRework: this._createReworkModal.find('#component-rework').is(':checked'),
                builds: this._getBuilds(),
                submittedBy: this._createReworkModal.find('#submitted-by').chosen().val(),
                technicalContact: this._createReworkModal.find('#technical-contact').chosen().val(),
                approvedBy: this._createReworkModal.find('#approved-by').chosen().val(),
                instructions: {
                    instructionsSupplied: this._createReworkModal.find('#instructions-supplied').is(':checked'),
                    source: this._createReworkModal.find('#received-how-select').val().toUpperCase(),
                    partsRequired: this._createReworkModal.find('#parts-required').is(':checked'),
                    inspected: this._createReworkModal.find('#inspected').is(':checked'),
                    inspectionType: this._createReworkModal.find('input[name="instructions[inspectionType]"]:checked').val(),
                    radarIssueNumber: this._createReworkModal.find('#radar-issue-no').val(),
                    description: this._createReworkModal.find('#instructions').val(),
                }
            }
        }).done((data, textStatus, xhr) => {
            new Alert('Rework has been created successfully.', 'success', function(){
                window.location.href = xhr.getResponseHeader('Location');
            });
        }).fail((e) => {
            let formErrors = JSON.parse(e.responseText).errors;
            this._formHelpers.addErrors(this._createReworkModal.find('form'), formErrors);
            this._createReworkModal.find('button#save-rework').prop('disabled', false);
        });
    }

    _getBuilds() {
        let builds = [];
        let selectedBuilds = this._createReworkModal.find('input[name="build-item"]:checkbox:checked');

        jQuery.each(selectedBuilds, function (index, input) {
            builds.push(jQuery(input).data('id'));
        });

        return builds;
    }

    _updateBuildList() {
        this._createReworkModal.find('div#builds-wrapper').html('');
        let projectId = this._createReworkModal.find('#project').chosen().val();

        if (!projectId) {
            return;
        }

        let builds = projects[projectId].builds;
        let buildList = jQuery('<ul/>');
        jQuery.each(builds, function(index, val){
            let item = `<li><input name="build-item" type="checkbox" data-id="${index}" checked />${val}</li>`;
            buildList.append(item);
        });
        this._createReworkModal.find('div#builds-wrapper').append(buildList);
    }
};