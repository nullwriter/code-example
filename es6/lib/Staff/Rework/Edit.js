"use strict";

import editReworkModal from 'html/Staff/Rework/CreateEditModal.html';
import ChosenAjax from 'lib/Staff/ChosenAjax.js';
import Alert from 'lib/Alert.js';
import FormHelpers from 'lib/FormHelpers.js';

export default class Edit {

    constructor(editFormDefaults) {
        let body = jQuery('body');
        this._editReworkModal = jQuery(editReworkModal);
        body.append(this._editReworkModal);

        this._rework = editFormDefaults;
        this._addProjectDefaults();
        this._formHelpers = new FormHelpers();

        new ChosenAjax(
            this._editReworkModal.find('#submitted-by'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        new ChosenAjax(
            this._editReworkModal.find('#technical-contact'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        new ChosenAjax(
            this._editReworkModal.find('#approved-by'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        this._editReworkModal.find('#project').trigger('change');
        this._editReworkModal
            .find('#project').off('change').on('change', this._updateBuildList.bind(this));

        new ChosenAjax(
            this._editReworkModal.find('#project'),
            '',
            function(index, project) {
                return {
                    value: index,
                    text: project.name
                };
            },
            projects
        );

        this._editReworkModal.find(`select#project option[value="${this._rework.project.id}"]`).attr('selected', true);
        this._editReworkModal
            .find('button#save-rework').off('click').on('click', this._submitForm.bind(this));

        this._editReworkModal
            .find('input[name="build-item"]').off('change').on('change', this._validateBuildsChecked.bind(this));

        this._validateBuildsChecked();
        this._editReworkModal.modal();
    }

    _validateBuildsChecked() {
        let selectedBuilds = this._editReworkModal.find('input[name="build-item"]:checkbox:checked');
        if (0 >= selectedBuilds.length) {
            this._editReworkModal.find('#project').attr('disabled', false).trigger("chosen:updated");
        } else {
            this._editReworkModal.find('#project').attr('disabled', true).trigger("chosen:updated");
        }
    }

    _updateBuildList() {
        this._editReworkModal.find('div#builds-wrapper').html('');
        let projectId = this._editReworkModal.find('#project').chosen().val();

        if (!projectId) {
            return;
        }

        let builds = projects[projectId].builds;
        let buildList = jQuery('<ul/>');
        jQuery.each(builds, function(index, val){
            let item = `<li><input name="build-item" type="checkbox" data-id="${index}" checked />${val}</li>`;
            buildList.append(item);
        });

        this._editReworkModal.find('div#builds-wrapper').append(buildList);
        this._validateBuildsChecked();
        this._editReworkModal
            .find('input[name="build-item"]').off('change').on('change', this._validateBuildsChecked.bind(this));
    }

    _submitForm() {
        this._editReworkModal.find('button#save-rework').prop('disabled', true);
        jQuery.post({
            url: '/staff/rework/api/update/' + this._rework.id,
            data: {csrf: csrfToken,
                name: this._editReworkModal.find('#name').val(),
                active: this._editReworkModal.find('#is-active').is(':checked'),
                project: this._editReworkModal.find('#project').chosen().val(),
                componentRework: this._editReworkModal.find('#component-rework').is(':checked'),
                builds: this._getBuilds(),
                submittedBy: this._editReworkModal.find('#submitted-by').chosen().val(),
                technicalContact: this._editReworkModal.find('#technical-contact').chosen().val(),
                approvedBy: this._editReworkModal.find('#approved-by').chosen().val(),
                instructions: {
                    instructionsSupplied: this._editReworkModal.find('#instructions-supplied').is(':checked'),
                    source: this._editReworkModal.find('#received-how-select').val().toUpperCase(),
                    partsRequired: this._editReworkModal.find('#parts-required').is(':checked'),
                    inspected: this._editReworkModal.find('#inspected').is(':checked'),
                    inspectionType: this._editReworkModal.find('input[name="instructions[inspectionType]"]:checked').val(),
                    radarIssueNumber: this._editReworkModal.find('#radar-issue-no').val(),
                    description: this._editReworkModal.find('#instructions').val(),
                }
            }
        }).done(() => {
            new Alert('Rework has been updated successfully.', 'success', window.location.reload());
        }).fail((e) => {
            let formErrors = JSON.parse(e.responseText).errors;
            this._formHelpers.addErrors(this._editReworkModal.find('form'), formErrors);
            this._editReworkModal.find('button#save-rework').prop('disabled', false);
        });
    }

    _getBuilds() {
        let builds = [];
        let selectedBuilds = this._editReworkModal.find('input[name="build-item"]:checkbox:checked');

        jQuery().each(selectedBuilds, function (index, input) {
            builds.push(jQuery(input).data('id'));
        });

        return builds;
    }

    _addProjectDefaults() {
        this._editReworkModal.find('.modal-title').html('Edit Rework');
        this._editReworkModal.find('input#name').val(this._rework.name);

        let option = jQuery('<option/>');
        option.attr('value', this._rework.submittedBy.dsid);
        option.attr('selected', 'selected');
        option.text(this._rework.submittedBy.name);
        this._editReworkModal.find('select#submitted-by').append(option);

        option = jQuery('<option/>');
        option.attr('value', this._rework.technicalContact.dsid);
        option.attr('selected', 'selected');
        option.text(this._rework.technicalContact.name);
        this._editReworkModal.find('select#technical-contact').append(option);

        option = jQuery('<option/>');
        option.attr('value', this._rework.approvedBy.dsid);
        option.attr('selected', 'selected');
        option.text(this._rework.approvedBy.name);
        this._editReworkModal.find('select#approved-by').append(option);

        let buildList = jQuery('<ul/>');
        jQuery.each(this._rework.builds, function(text, val){
            let item = `<li><input name="build-item" type="checkbox" data-id="${val.id}" checked />${val.name}</li>`;
            buildList.append(item);
        });
        this._editReworkModal.find('div#builds-wrapper').append(buildList);

        if (this._rework.componentRework) {
            this._editReworkModal.find('input#component-rework').prop('checked', 'checked');
        }

        if (this._rework.instructions.areInstructionsSupplied) {
            this._editReworkModal.find('input#instructions-supplied').prop('checked', 'checked');
        }

        if (this._rework.instructions.arePartsRequired) {
            this._editReworkModal.find('input#parts-required').prop('checked', 'checked');
        }

        if (this._rework.active) {
            this._editReworkModal.find('input#is-active').prop('checked', 'checked');
        }

        this._editReworkModal
            .find(`select#received-how-select option[value="${this._rework.instructions.source}"]`)
            .prop('selected', true);

        this._editReworkModal
            .find(`input[name="instructions[inspectionType]"][value="${this._rework.instructions.inspectionType}"]`)
            .prop('checked', 'checked');

        if (this._rework.instructions.wasInspected) {
            this._editReworkModal.find('input#inspected').prop('checked', 'checked');
        }

        this._editReworkModal.find('input#radar-issue-no').val(this._rework.instructions.radarIssueNumber);
        this._editReworkModal.find('textarea#instructions').append(this._rework.instructions.description);
    }

};