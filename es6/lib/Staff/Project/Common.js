"use strict";

import Alert from 'lib/Alert.js';

function addErrorMessage(errorMessage) {
    new Alert(errorMessage);
}

function addTypes(modal) {
    let projectTypeSelect = modal.find('#project-types');

    jQuery.each(projectTypes, function(index, val) {
        projectTypeSelect.append(new Option(val.label, val.value));
    });
}

function addPriorities(modal) {
    let prioritySelect = modal.find('#priority');

    jQuery.each(priorities, function(index, val) {
        prioritySelect.append(new Option(val.label, val.value));
    });
}

function obtainTeamMembers(modal, inputs) {
    let teamMembers = [];
    let teamInputs = modal.find(inputs);

    jQuery.each(teamInputs, function(index, input) {
        if (! jQuery(input).chosen().val()) {
            return;
        }

        let member = {
            person: jQuery(input).chosen().val(),
            role: jQuery(input).data('role'),
            position: jQuery(input).data('position') || 0,
            team: jQuery(input).data('team')
        };

        teamMembers.push(member);
    });

    return teamMembers;
}

function addTeamMemberInputs(modal) {
    let systemTeamTable = "table#system-team";
    let pseTeamTable = "table#pse-team";

    let memberInputElement = function (id, role, position, team, value) {
        id = id || '';
        role = role || '';
        position = position || 0;
        team = team || '';
        value = value || '';

        return `<select class="form-control chosen apple-window-info-select project-team-input" type="text" \
                value="${value}" \
                data-id="${id}" \
                data-role="${role}" \
                data-position="${position}" \
                data-team="${team}" \
            ><option value=""></option></select>`;
    };

    let htmlInsert = '';
    jQuery.each(teamRoles, function(text, val){
        htmlInsert += '<tr><td>'+val.label+'</td>';
        for (let i = 0; i < 2; i++) {
            htmlInsert +='<td>';
            htmlInsert += memberInputElement('', val.role, i, systemTeam, '');
            htmlInsert += '</td>';
        }
        htmlInsert += '</tr>';
    });
    modal.find(systemTeamTable).append(htmlInsert);

    htmlInsert = '';
    jQuery.each(teamRoles, function(text, val){
        htmlInsert += '<tr><td>'+val.label+'</td>';
        htmlInsert +='<td>';
        htmlInsert += memberInputElement('', val.role, '', pseTeam, '');
        htmlInsert += '</td>';
        htmlInsert += '</tr>';
    });
    modal.find(pseTeamTable).append(htmlInsert);
}

export default {
    obtainTeamMembers,
    addTeamMemberInputs,
    addTypes,
    addPriorities,
    addErrorMessage,
};