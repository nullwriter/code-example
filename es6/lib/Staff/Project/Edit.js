"use strict";

import editProjectModal from 'html/Staff/Project/CreateEditModal.html';
import Common from 'lib/Staff/Project/Common.js';
import ChosenAjax from 'lib/Staff/ChosenAjax.js';

export default class Edit {

    constructor(editFormDefaults) {
        let body = jQuery('body');
        body.append(editProjectModal);

        this._project = editFormDefaults.project;
        this._editBuildModal = jQuery(editProjectModal);
        this._editBuildModal.find('.modal-title').html('Edit Project');

        Common.addTeamMemberInputs(this._editBuildModal);
        Common.addTypes(this._editBuildModal);
        Common.addPriorities(this._editBuildModal);

        this._addProjectDefaults();
        this._editBuildModal.modal();

        this._editBuildModal.find('.project-team-input').each(function (index, input) {
            new ChosenAjax(
                jQuery(input),
                '/person/search-persons-by-partial-name?staffOnly=true&query=',
                function(index, person) {
                    return {
                        value: person.dsId,
                        text: person.fullName
                    };
                }
            );
        });

        new ChosenAjax(
            this._editBuildModal.find('#default-ess-tech'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        this._editBuildModal.find('button#save-create-project').off('click').on('click', this._submitForm.bind(this));
    }

    _submitForm() {
        let teamMembers = Common.obtainTeamMembers(this._editBuildModal, 'select.project-team-input');

        jQuery.post({
            url: '/staff/project/api/update/' + this._project.id,
            data: {
                csrf: csrfToken,
                name: this._editBuildModal.find('#project-name').val(),
                type: this._editBuildModal.find('#project-types').val(),
                defaultTicketPriority: this._editBuildModal.find('#priority').val(),
                defaultEssTech: this._editBuildModal.find('#default-ess-tech').chosen().val(),
                teamMembers: teamMembers
            }
        }).done(() => {
            window.location.reload();
        }).fail((e) => {
            Common.addErrorMessage(e.responseText);
        });
    }

    _addProjectDefaults() {
        this._addCurrentTeamMembers();

        if (this._project.isItracked) {
            this._editBuildModal.find('input#project-name').attr('value', this._project.name);
            this._editBuildModal.find('input#project-name').attr('disabled', 'disabled');
        } else {
            this._editBuildModal.find('input#project-name').attr('value', this._project.name);
        }

        if (null !== this._project.typeId) {
            this._editBuildModal
                .find(`#project-types option[value="${this._project.typeId}"]`).attr('selected','selected');
    }

        this._editBuildModal
            .find(`#priority option[value="${this._project.priorityId}"]`).attr('selected','selected');

        if (null !== this._project.defaultEssTech) {
            let defaultEssTechSelect = this._editBuildModal.find('#default-ess-tech');
            defaultEssTechSelect.html('');

            let option = jQuery('<option/>');
            option.attr('value', this._project.defaultEssTech.id);
            option.attr('selected', 'selected');
            option.text(this._project.defaultEssTech.name);

            defaultEssTechSelect.append(option);
        }
    }

    _addCurrentTeamMembers() {
        let Edit_this = this;

        jQuery.each(this._project.systemTeam, function(index, val) {
        if (! val.members) {
            return;
        }

        for (let position = 0; position < 2; position++) {
                let teamMemberSelect = Edit_this._editBuildModal.find(
                `select.project-team-input[data-role="${val.role}"]` +
                `[data-team="${systemTeam}"][data-position="${position}"]`
            );

                if (teamMemberSelect && typeof(val.members[position]) !== 'undefined') {
                    teamMemberSelect.html('');
                    let option = jQuery('<option/>');
                    option.attr('value', val.members[position].id);
                    option.attr('selected', 'selected');
                    option.text(val.members[position].name);

                    teamMemberSelect.append(option);
            }
        }
        });

        jQuery.each(this._project.pseTeam, function(index, val) {
            if (! val.members) {
                return;
    }

            let position = 0;
            let teamMemberSelect = Edit_this._editBuildModal.find(
                `select.project-team-input[data-role="${val.role}"]` +
                `[data-team="${pseTeam}"]`
            );

            if (teamMemberSelect && typeof(val.members[position]) !== 'undefined') {
                teamMemberSelect.html('');
                let option = jQuery('<option/>');
                option.attr('value', val.members[position].id);
                option.attr('selected', 'selected');
                option.text(val.members[position].name);

                teamMemberSelect.append(option);
            }
        });
    }

};
