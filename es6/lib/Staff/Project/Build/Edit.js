"use strict";

import editBuildModal from 'html/Staff/Project/Build/AddEditModal.html';
import Common from 'lib/Staff/Project/Common.js';

export default class Edit {

    constructor(buildId) {
        let body = jQuery('body');
        body.append(editBuildModal);

        this._buildId = buildId;
        this._editBuildModal = jQuery(editBuildModal);
        this._editBuildModal.find('.modal-title').html('Edit Build');

        this._addBuildDefaultsAndShowModal();
        this._editBuildModal.find('button#save-add-build').off('click').on('click', this._submitForm.bind(this));
    }

    _submitForm() {
        let buildName = this._editBuildModal.find('#build-name').val().trim();

        if ('' === buildName) {
            Common.addErrorMessage('You need to add a Build Name.');
            return;
        }

        let reworks = [];
        this._editBuildModal.find('input.added-reworks:checkbox:checked').each(function () {
            reworks.push(jQuery(this).data('id'));
        });

        jQuery.post({
            url: '/staff/project/api/update-build/' + this._buildId,
            data: {
                csrf: csrfToken,
                name: buildName,
                reworks: reworks
            }
        }).done((data) => {
            window.location.reload();
        }).fail((e) => {
            Common.addErrorMessage(e.responseText);
        });
    }

    _addBuildDefaultsAndShowModal() {
        jQuery.get({
            url: '/staff/project/api/get-build/' + this._buildId
        }).done((data) => {
            this._populateModalElements(data.build, data.reworks);
            this._editBuildModal.modal();
        }).fail((e) => {
            Common.addErrorMessage(e.responseText);
        });
    }

    _populateModalElements(build, buildReworks) {
        this._editBuildModal.find('#build-name').val(build.name);

        let reworkTable = this._editBuildModal.find('.build-content');
        reworkTable.html('');
        reworkTable.append('<tbody></tbody>');
        let tbody = reworkTable.children('tbody');

        let reworkPrototype = jQuery('<tr/>');
        reworkPrototype.append('<td><input type="checkbox" class="added-reworks"/></td>');
        reworkPrototype.append('<td data-name="name"/>');
        reworkPrototype.append('<td data-name="status"/>');

        if (projectReworks.length > 0) {

            projectReworks.forEach(function (rework) {
                let tr = reworkPrototype.clone();
                tr.find('input').data('id', rework.id);
                tr.find('td[data-name="name"]').text(rework.name);
                tr.find('td[data-name="status"]').text(rework.active ? 'Active' : 'Inactive');

                if (Edit.isReworkInBuild(buildReworks, rework)) {
                    tr.find('input').attr('checked', 'checked');
                }

                tbody.append(tr);
            });
        } else {
            tbody.append('<tr><td colspan="3">No reworks available.</td></tr>');
        }
    }

    static isReworkInBuild(buildReworks, rework) {
        for (let i = 0; i < buildReworks.length; i++) {
            if (rework.id === buildReworks[i].id) {
                return true;
            }
        }

        return false;
    }

}