"use strict";

import addBuildModal from 'html/Staff/Project/Build/AddEditModal.html';
import Common from 'lib/Staff/Project/Common.js';

export default class Add {

    constructor() {
        let body = jQuery('body');
        body.append(addBuildModal);
        this._addBuildModal = jQuery(addBuildModal);
        this._addBuildModal.find('.modal-title').html('Add Build');
        this._addReworks();

        this._addBuildModal.modal();
        this._addBuildModal.find('button#save-add-build').off('click').on('click', this._submitForm.bind(this));
    }

    _addReworks() {
        let reworksTable = 'table.build-content';
        this._addBuildModal.find(reworksTable).html('<tbody></tbody>')

        if (projectReworks) {
            let htmlInsert = '';

            jQuery.each(projectReworks, function (index, val){
                let activeStr = (val.isActive ? 'Active' : 'Inactive');

                htmlInsert += '<tr>';
                htmlInsert += `<td><input type="checkbox" class="added-reworks" data-id="${val.id}" /></td>`;
                htmlInsert += `<td><a href>${val.name}</a></td>`;
                htmlInsert += `<td><a href>${activeStr}</a></td>`;
                htmlInsert += '</tr>';
            });

            this._addBuildModal.find(reworksTable).append(htmlInsert);
        }
    }

    _submitForm() {
        let buildName = this._addBuildModal.find('#build-name').val().trim();

        if ('' === buildName) {
            Common.addErrorMessage('You need to add a Build Name.');
            return;
        }

        let reworks = [];
        jQuery.each(this._addBuildModal.find('input.added-reworks:checkbox:checked'), function (index, input) {
            reworks.push(jQuery(input).data('id'));
        });

        jQuery.post({
            url: '/staff/project/api/add-build/' + window.projectId,
            data: {
                csrf: csrfToken,
                name: buildName,
                reworks: reworks
            }
        }).done((data) => {
            window.location.reload();
        }).fail((e) => {
            Common.addErrorMessage(e.responseText);
        });
    }
}