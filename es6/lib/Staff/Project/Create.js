"use strict";

import createProjectModal from 'html/Staff/Project/CreateEditModal.html';
import Common from 'lib/Staff/Project/Common.js';
import ChosenAjax from 'lib/Staff/ChosenAjax.js';

export default class Create {

    constructor() {
        let body = jQuery('body');
        body.append(createProjectModal);
        this._createProjectModal = jQuery(createProjectModal);
        this._createProjectModal.find('.modal-title').html('Create Project');

        Common.addTeamMemberInputs(this._createProjectModal);
        Common.addTypes(this._createProjectModal);
        Common.addPriorities(this._createProjectModal);
        this._createProjectModal.modal();

        this._createProjectModal.find('.project-team-input').each(function (index, input) {
            new ChosenAjax(
                jQuery(input),
                '/person/search-persons-by-partial-name?staffOnly=true&query=',
                function(index, person) {
                    return {
                        value: person.dsId,
                        text: person.fullName
                    };
                }
            );
        });

        new ChosenAjax(
            this._createProjectModal.find('#default-ess-tech'),
            '/person/search-persons-by-partial-name?staffOnly=true&query=',
            function(index, person) {
                return {
                    value: person.dsId,
                    text: person.fullName
                };
            }
        );

        this._createProjectModal
            .find('button#save-create-project').off('click').on('click', this._submitForm.bind(this));
    }

    _submitForm() {
        let teamMembers = Common.obtainTeamMembers(this._createProjectModal, 'select.project-team-input');

        jQuery.post({
            url: '/staff/project/api/create',
            data: {csrf: csrfToken,
                name: this._createProjectModal.find('#project-name').val(),
                type: this._createProjectModal.find('#project-types').val(),
                defaultTicketPriority: this._createProjectModal.find('#priority').val(),
                defaultEssTech: this._createProjectModal.find('#default-ess-tech').chosen().val(),
                teamMembers: teamMembers
            }
        }).done((data, textStatus, xhr) => {
            window.location.href = xhr.getResponseHeader('Location');
        }).fail((e) => {
            Common.addErrorMessage(e.responseText);
        });
    }
};