"use strict";

import jQuery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import 'datatables.net';
import 'datatables.net-bs';
import 'datatables.net-responsive-bs';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
import 'datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import 'pdfmake/build/pdfmake.js';
import vfs from 'pdfmake/build/vfs_fonts.js';
import JSZip from 'jszip/dist/jszip.js';
import 'datatables.net-buttons/js/buttons.html5.js';
import 'datatables.net-buttons-bs';
import 'datatables.net-buttons/js/buttons.colVis.js';
import 'datatables.net-buttons-bs/css/buttons.bootstrap.css';
import 'eonasdan-bootstrap-datetimepicker';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';
import 'moment/moment.js';
import 'moment-timezone';

// This is required so buttons.html5 detects JSZip
window.JSZip = JSZip;

// This is required so PDFMake properly detects the fonts
window.pdfMake.vfs = vfs.pdfMake.vfs;

export default class List {
    constructor(table, url, columnOptions, orders) {
        this._table = table;
        this._url = url;
        this._columnOptions = columnOptions;
        this._orders = orders;
        this._theadRow = table.children('thead').children('tr');
        this._tfoot = jQuery('<tfoot/>');
        this._tfootRow = jQuery('<tr/>')
        this._columnNameToIndexMap = new Map();
        this._columnIndexToNameMap = new Map();
        this._columnIndexToTypeMap = new Map();
        this._columns = [];

        this._setupTable();
        this._initDataTable();
        this._setupColumnFilters();
    }

    _setupTable() {
        this._theadRow.children('th').each((index, element) => {
            let th = jQuery(element);
            let name = th.data('name');
            let fieldType = th.data('field-type');

            this._columns[index] = {
                name: name,
                data: name,
                searchable: true,
                orderable: true
            };
            this._columnNameToIndexMap.set(name, index);
            this._columnIndexToNameMap.set(index, name);
            this._columnIndexToTypeMap.set(index, fieldType);

            let td = jQuery('<td/>');
            this._tfootRow.append(td);

            if (name in this._columnOptions) {
                jQuery.extend(this._columns[index], this._columnOptions[name]);
            }

            if (! this._columns[index].searchable) {
                return;
            }

            let booleanSelect = jQuery('<select class="form-control boolean"/>');
            booleanSelect.append('<option value=""></option>');
            booleanSelect.append('<option value="true">True</option>');
            booleanSelect.append('<option value="false">False</option>');

            let enumSelect = jQuery('<select class="form-control enum"/>');
            enumSelect.append('<option value=""></option>');

            let datetimeFilter = jQuery('<div class="datetime"/>');
            datetimeFilter.append('<div class="input-group date"">'
                + '<input type="text" name="from" class="form-control" placeholder="From" style="width: 180px;"/>'
                + '<span class="input-group-addon">'
                + '<span class="glyphicon glyphicon-calendar"/>'
                + '</span>'
                + '</div>'
            );
            datetimeFilter.append('<div class="input-group date">'
                + '<input type="text" name="to" class="form-control" placeholder="Until" style="width: 180px;"/>'
                + '<span class="input-group-addon">'
                + '<span class="glyphicon glyphicon-calendar"/>'
                + '</span>'
                + '</div>'
            );

            switch (fieldType) {
                case 'boolean':
                    this._columns[index].render = (data) => {
                        if (null === data) {
                            return '';
                        }

                        if (data) {
                            return '<span class="glyphicon glyphicon-check"></span>';
                        }

                        return '<span class="glyphicon glyphicon-unchecked"></span>';
                    };

                    td.append(booleanSelect.clone());
                    break;

                case 'enum':
                    let select = enumSelect.clone();

                    th.data('values').forEach((value) => {
                        select.append(jQuery('<option/>').attr('value', value).text(value));
                    });

                    td.append(select);
                    break;

                case 'datetime':
                    this._columns[index].render = (data) => {
                        if (null === data) {
                            return '';
                        }

                        return moment(data + ' Z').tz(window.timeZone).format('L LT');
                    };

                    td.append(datetimeFilter.clone());
                    break;

                default:
                    td.append('<input type="search" class="form-control"/>');
            }
        });

        this._tfoot.append(this._tfootRow);
        this._table.append(this._tfoot);
    }

    _buttonCommon() {
        return {
            exportOptions: {
                format: {
                    body: (data, rowIndex, columnIndex) => {
                        let fieldType = this._columnIndexToTypeMap.get(columnIndex);

                        switch (fieldType) {
                            case 'boolean':
                                if ('' === data) {
                                    return '';
                                }

                                return (jQuery(data).hasClass('glyphicon-check') ? 'True' : 'False');
                        }

                        return jQuery('<span>').html(data).text();
                    }
                }
            }
        };
    }

    _initDataTable() {
        let buttonCommon = this._buttonCommon();
        let order = [];

        Object.keys(this._orders).forEach((field) => {
            order.push([
                this._columnNameToIndexMap.get(field),
                this._orders[field]
            ]);
        });

        this._api = this._table.DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: this._url,
                type: 'POST',
                data: (data) => {
                    data.csrf = csrfToken
                }
            },
            pageLength: 50,
            columns: this._columns,
            colReorder: true,
            order: order,
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'colvis',
                    columns: ':not(.entity-hyperlink)'
                },
                jQuery.extend(true, {}, buttonCommon, {
                    extend: 'copyHtml5'
                }),
                jQuery.extend(true, {}, buttonCommon, {
                    extend: 'csvHtml5'
                }),
                jQuery.extend(true, {}, buttonCommon, {
                    extend: 'excelHtml5'
                }),
                jQuery.extend(true, {}, buttonCommon, {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A1'
                }),
            ]
        });

        // Add "Clear Filter" button to Filter
        let globalSearchWrapper = this._table.parents('.dataTables_wrapper').find('.dataTables_filter');
        let clearButton = jQuery('<button type="button" class="btn btn-default">Reset Column Filters</button>');
        clearButton.on('click', () => {
            this._resetColumnFilterValues();
            this._api.draw();
        });
        clearButton.appendTo(globalSearchWrapper);

        // Make sure that we don't submit searches with less than three characters
        let globalSearchInput = this._table.parents('.dataTables_wrapper').find('.dataTables_filter input');
        globalSearchInput.off().on('input', () => {
            let value = globalSearchInput.val();

            if (value.length >= 3 || 0 === value.length) {
                this._resetColumnFilterValues();
                this._api.search(value).draw();
            }
        });
    }

    _setupColumnFilters() {
        this._api.columns().every((index) => {
            let column = this._api.column(index);

            // Text filters
            let input = jQuery('input[type="search"]', column.footer());
            input.on('input', () => {
                let value = input.val();

                if (column.search() === value || (value.length > 0 && value.length < 3)) {
                    return;
                }

                column.search(value).draw();
            });
            input.val(column.search());

            // Boolean filters
            let booleanSelect = jQuery('select.boolean', column.footer());
            booleanSelect.on('change', () => {
                column.search(booleanSelect.val()).draw();
            });
            booleanSelect.val(column.search());

            // Enum filters
            let enumSelect = jQuery('select.enum', column.footer());
            enumSelect.on('change', () => {
                column.search(enumSelect.val()).draw();
            });
            enumSelect.val(column.search());

            // Datetime filters
            let datetimeFilter = jQuery('div.datetime', column.footer());

            if (0 === datetimeFilter.length) {
                return;
            }

            let fromInput = datetimeFilter.find('input[name="from"]').parent();
            let toInput = datetimeFilter.find('input[name="to"]').parent();

            fromInput.datetimepicker({
                sideBySide: true,
                allowInputToggle: true,
                showClear: true,
                timeZone: window.timeZone
            });
            toInput.datetimepicker({
                sideBySide: true,
                allowInputToggle: true,
                useCurrent: false,
                showClear: true,
                timeZone: window.timeZone
            });

            let currentRange = column.search();
            let rangeFrom = null;
            let rangeTo = null;

            if ('' !== currentRange) {
                let rangeParts = currentRange.split('-');

                rangeFrom = (Number.isNaN(parseInt(rangeParts[0])) ? null : parseInt(rangeParts[0]));
                rangeTo = (Number.isNaN(parseInt(rangeParts[1])) ? null : parseInt(rangeParts[1]));

                if (null === rangeFrom && null === rangeTo) {
                    currentRange = '';
                } else {
                    currentRange = rangeFrom + '-' + rangeTo;
                }
            }

            if (null !== rangeFrom) {
                fromInput.data('DateTimePicker').date(new Date(rangeFrom * 1000));
            }

            if (null !== rangeTo) {
                toInput.data('DateTimePicker').date(new Date(rangeTo * 1000));
            }

            let updateDateSearch = () => {
                let newRange;

                if (null === rangeFrom && null === rangeTo) {
                    newRange = '';
                } else {
                    newRange = rangeFrom + '-' + rangeTo;
                }

                if (newRange === currentRange) {
                    return;
                }

                currentRange = newRange;
                column.search(currentRange).draw();
            };

            fromInput.on('dp.change', (event) => {
                toInput.data('DateTimePicker').minDate(event.date);
                rangeFrom = (event.date ? event.date.unix() : null);
                updateDateSearch();
            });
            toInput.on('dp.change', (event) => {
                fromInput.data('DateTimePicker').maxDate(event.date);
                rangeTo = (event.date ? event.date.unix() : null);
                updateDateSearch();
            });

            let overflowFix = () => {
                let datepicker = jQuery('body').find('.bootstrap-datetimepicker-widget');
                let position = datepicker.offset();
                let parent = datepicker.parent();
                let parentOffset = parent.offset();
                let width = datepicker.width();
                let parentWidth = parent.width();

                datepicker.appendTo('body');
                datepicker.css({
                    position: 'absolute',
                    top: position.top,
                    bottom: 'auto',
                    left: position.left,
                    right: 'auto'
                });

                if (parentOffset.left + parentWidth < position.left + width) {
                    let newLeft = parentOffset.left;
                    newLeft += parentWidth / 2;
                    newLeft -= width / 2;
                    datepicker.css({left: newLeft});
                }
            };

            fromInput.on('dp.show', overflowFix);
            toInput.on('dp.show', overflowFix);
        });
    }

    _resetColumnFilterValues() {
        this._api.columns().every((index) => {
            let column = this._api.column(index);

            // Text filters
            jQuery('input[type="search"]', column.footer()).val('');

            // Enum filters
            jQuery('select.enum', column.footer()).val('');

            // Boolean filters
            jQuery('select.boolean', column.footer()).val('');

            // Datetime filters
            jQuery('div.datetime input', column.footer()).val('');
        });
        this._api.columns().search('');
    };
};
