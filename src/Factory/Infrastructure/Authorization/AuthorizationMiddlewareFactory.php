<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Authorization;

use DASPRiD\Helios\CookieManagerInterface;
use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Infrastructure\Authorization\AuthorizationMiddleware;
use Zend\Expressive\Helper\UrlHelper;

final class AuthorizationMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new AuthorizationMiddleware(
            $container->get(UrlHelper::class),
            $container->get(CookieManagerInterface::class)
        );
    }
}
