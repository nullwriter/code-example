<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\IsStaffPersonConstraint;

final class IsStaffPersonConstraintFactory
{
    public function __invoke(ContainerInterface $container) : ConstraintInterface
    {
        return new IsStaffPersonConstraint(
            $container->get(IsStaffPersonInterface::class)
        );
    }
}
