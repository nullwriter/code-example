<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\ReworkFormatter;

final class ReworkFormatterFactory
{
    public function __invoke(ContainerInterface $container) : FormatterInterface
    {
        return new ReworkFormatter(
            $container->get(SearchReworkByIdInterface::class)
        );
    }
}
