<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\BuildFormatter;

final class BuildFormatterFactory
{
    public function __invoke(ContainerInterface $container) : FormatterInterface
    {
        return new BuildFormatter(
            $container->get(SearchBuildByIdInterface::class)
        );
    }
}
