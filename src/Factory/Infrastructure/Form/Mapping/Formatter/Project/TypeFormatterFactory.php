<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Formatter\Project;

use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\Project\TypeFormatter;

final class TypeFormatterFactory
{
    public function __invoke(ContainerInterface $container) : FormatterInterface
    {
        return new TypeFormatter(
            $container->get(SearchTypeByIdInterface::class)
        );
    }
}
