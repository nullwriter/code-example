<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Person\LookupAndStorePersonByDsIdInterface;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\PersonFormatter;

final class PersonFormatterFactory
{
    public function __invoke(ContainerInterface $container) : FormatterInterface
    {
        return new PersonFormatter(
            $container->get(LookupAndStorePersonByDsIdInterface::class)
        );
    }
}
