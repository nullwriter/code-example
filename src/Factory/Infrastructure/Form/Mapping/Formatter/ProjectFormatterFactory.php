<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\ProjectFormatter;

final class ProjectFormatterFactory
{
    public function __invoke(ContainerInterface $container) : FormatterInterface
    {
        return new ProjectFormatter(
            $container->get(SearchProjectByIdInterface::class)
        );
    }
}
