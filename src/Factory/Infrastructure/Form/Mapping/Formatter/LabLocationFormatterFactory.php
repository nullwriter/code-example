<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\LabLocationFormatter;

final class LabLocationFormatterFactory
{
    public function __invoke(ContainerInterface $container) : FormatterInterface
    {
        return new LabLocationFormatter(
            $container->get(SearchLabLocationsByIdInterface::class)
        );
    }
}
