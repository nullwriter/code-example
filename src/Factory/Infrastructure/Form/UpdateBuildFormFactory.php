<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormInterface;
use DASPRiD\Formidable\Mapping\FieldMapping;
use DASPRiD\Formidable\Mapping\FieldMappingFactory;
use DASPRiD\Formidable\Mapping\ObjectMapping;
use DASPRiD\Formidable\Mapping\OptionalMapping;
use DASPRiD\Formidable\Mapping\RepeatedMapping;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Form\Data\Project\BuildData;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\ReworkFormatter;

final class UpdateBuildFormFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : FormInterface
    {
        return new Form(new ObjectMapping([
            'name' => FieldMappingFactory::ignored(null),
            'reworks' => new RepeatedMapping(
                new FieldMapping(
                    new ReworkFormatter(
                        $container->get(SearchReworkByIdInterface::class)
                    )
                )
            )
        ], BuildData::class));
    }
}
