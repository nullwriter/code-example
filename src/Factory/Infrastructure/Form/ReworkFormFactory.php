<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormInterface;
use DASPRiD\Formidable\Mapping\FieldMapping;
use DASPRiD\Formidable\Mapping\FieldMappingFactory;
use DASPRiD\Formidable\Mapping\ObjectMapping;
use DASPRiD\Formidable\Mapping\OptionalMapping;
use DASPRiD\Formidable\Mapping\RepeatedMapping;
use Pinpoint\Domain\Rework\Instructions;
use Pinpoint\Infrastructure\Form\Data\Rework\ReworkData;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\Rework\BuildsInReworkProjectConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\BuildFormatter;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\PersonFormatter;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\ProjectFormatter;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\Rework\InspectionTypeFormatter;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\Rework\SourceFormatter;
use Psr\Container\ContainerInterface;

final class ReworkFormFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : FormInterface
    {
        $mapping = new ObjectMapping([
            'name' => FieldMappingFactory::nonEmptyText(),
            'active' => FieldMappingFactory::boolean(),
            'project' => new FieldMapping($container->get(ProjectFormatter::class)),
            'componentRework' => FieldMappingFactory::boolean(),
            'builds' => new RepeatedMapping(new FieldMapping($container->get(BuildFormatter::class))),
            'submittedBy' => new FieldMapping($container->get(PersonFormatter::class)),
            'technicalContact' => new OptionalMapping(new FieldMapping($container->get(PersonFormatter::class))),
            'approvedBy' => new OptionalMapping(new FieldMapping($container->get(PersonFormatter::class))),
            'approvedAt' => new OptionalMapping(FieldMappingFactory::dateTime()),
            'instructions' => new ObjectMapping([
                'instructionsSupplied' => FieldMappingFactory::boolean(),
                'source' => new FieldMapping(new SourceFormatter()),
                'partsRequired' => FieldMappingFactory::boolean(),
                'inspected' => FieldMappingFactory::boolean(),
                'inspectionType' => new FieldMapping(new InspectionTypeFormatter()),
                'radarIssueNumber' => FieldMappingFactory::text(),
                'description' => FieldMappingFactory::text(),
            ], Instructions::class),
        ], ReworkData::class);

        return new Form($mapping->verifying(new BuildsInReworkProjectConstraint()));
    }
}
