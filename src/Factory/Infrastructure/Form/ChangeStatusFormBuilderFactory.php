<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form;

use Interop\Container\ContainerInterface;
use Pinpoint\Infrastructure\Form\ChangeStatusFormBuilder;

final class ChangeStatusFormBuilderFactory
{
    public function __invoke(ContainerInterface $container) : ChangeStatusFormBuilder
    {
        return new ChangeStatusFormBuilder(
            $container->get(ChangeStatusObjectMappingFactory::class)
        );
    }
}
