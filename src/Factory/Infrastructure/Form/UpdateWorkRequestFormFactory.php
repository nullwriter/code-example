<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormInterface;
use DASPRiD\Formidable\Mapping\FieldMapping;
use DASPRiD\Formidable\Mapping\FieldMappingFactory;
use DASPRiD\Formidable\Mapping\ObjectMapping;
use DASPRiD\Formidable\Mapping\OptionalMapping;
use Interop\Container\ContainerInterface;
use Pinpoint\Infrastructure\Form\Data\UpdateTicket\WorkRequestData;
use Pinpoint\Infrastructure\Form\Mapping\Constraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\WorkRequestCompletedByConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Formatter;

final class UpdateWorkRequestFormFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : FormInterface
    {
        return new Form((new ObjectMapping([
            'type' => FieldMappingFactory::text()->verifying(new Constraint\WorkRequestTypeConstraint()),
            'description' => FieldMappingFactory::nonEmptyText(),
            'assignee' => new OptionalMapping(
                (new FieldMapping($container->get(Formatter\PersonFormatter::class)))
                    ->verifying($container->get(Constraint\IsStaffPersonConstraint::class))
            ),
            'complete' => FieldMappingFactory::boolean(),
            'completedBy' => new OptionalMapping(
                (new FieldMapping($container->get(Formatter\PersonFormatter::class)))
                    ->verifying($container->get(Constraint\IsStaffPersonConstraint::class))
            ),
        ], WorkRequestData::class))->verifying(new WorkRequestCompletedByConstraint()));
    }
}
