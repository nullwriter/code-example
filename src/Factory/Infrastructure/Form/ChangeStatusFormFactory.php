<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormInterface;
use DASPRiD\Formidable\Mapping\FieldMapping;
use DASPRiD\Formidable\Mapping\FieldMappingFactory;
use DASPRiD\Formidable\Mapping\ObjectMapping;
use Pinpoint\Infrastructure\Form\Data\UpdateTicket\ChangeStatusData;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\ReasonConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\Ticket\ActionFormatter;
use Psr\Container\ContainerInterface;

final class ChangeStatusFormFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : FormInterface
    {
        $mapping = new ObjectMapping([
            'action' => new FieldMapping(new ActionFormatter()),
            'reason' => FieldMappingFactory::text(),
            'suppressNotifications' => FieldMappingFactory::boolean(),
        ], ChangeStatusData::class);

        return new Form($mapping->verifying(new ReasonConstraint()));
    }
}
