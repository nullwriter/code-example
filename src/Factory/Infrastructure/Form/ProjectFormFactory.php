<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormInterface;
use DASPRiD\Formidable\Mapping\FieldMapping;
use DASPRiD\Formidable\Mapping\FieldMappingFactory;
use DASPRiD\Formidable\Mapping\ObjectMapping;
use DASPRiD\Formidable\Mapping\OptionalMapping;
use DASPRiD\Formidable\Mapping\RepeatedMapping;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Person\LookupAndStorePersonByDsIdInterface;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Infrastructure\Form\Data\Project\ProjectData;
use Pinpoint\Infrastructure\Form\Data\Project\TeamMemberData;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\IsStaffPersonConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\Team\MaxMembersPerRoleConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\Team\TeamConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\Team\TeamMemberRoleConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\PersonFormatter;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\PriorityFormatter;
use Pinpoint\Infrastructure\Form\Mapping\Formatter\Project\TypeFormatter;

final class ProjectFormFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : FormInterface
    {
        return new Form(new ObjectMapping([
            'name' => FieldMappingFactory::nonEmptyText(),
            'defaultTicketPriority' => new FieldMapping(new PriorityFormatter()),
            'type' => new FieldMapping(new TypeFormatter($container->get(SearchTypeByIdInterface::class))),
            'defaultEssTech' => new OptionalMapping(
                (new FieldMapping(new PersonFormatter($container->get(LookupAndStorePersonByDsIdInterface::class))))
                    ->verifying($container->get(IsStaffPersonConstraint::class))
            ),
            'teamMembers' => (new RepeatedMapping(
                new ObjectMapping([
                    'person' => new OptionalMapping(
                        new FieldMapping(
                            new PersonFormatter($container->get(LookupAndStorePersonByDsIdInterface::class))
                        )
                    ),
                    'role' => FieldMappingFactory::nonEmptyText()
                        ->verifying(new TeamMemberRoleConstraint()),
                    'team' => FieldMappingFactory::nonEmptyText()->verifying(new TeamConstraint()),
                    'position' => FieldMappingFactory::integer(),
                ], TeamMemberData::class)
            ))->verifying(new MaxMembersPerRoleConstraint())
        ], ProjectData::class));
    }
}
