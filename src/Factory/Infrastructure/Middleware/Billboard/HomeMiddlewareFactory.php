<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Billboard;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Pinpoint\Infrastructure\Middleware\Billboard\HomeMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class HomeMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new HomeMiddleware(
            $container->get(GetAllLabLocationsInterface::class),
            $container->get(HtmlResponseRenderer::class)
        );
    }
}
