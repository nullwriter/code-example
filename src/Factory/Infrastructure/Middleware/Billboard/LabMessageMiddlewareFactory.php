<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Billboard;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Middleware\Billboard\LabMessageMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class LabMessageMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new LabMessageMiddleware(
            $container->get(SearchLabLocationsByIdInterface::class),
            $container->get(HtmlResponseRenderer::class)
        );
    }
}
