<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Billboard;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\GetLabLocationStatisticsInterface;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Middleware\Billboard\LabDetailsMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class LabDetailsMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new LabDetailsMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(SearchLabLocationsByIdInterface::class),
            $container->get(GetLabLocationStatisticsInterface::class)
        );
    }
}
