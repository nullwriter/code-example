<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\LabLocation;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Pinpoint\Infrastructure\Middleware\LabLocation\JsonGetAllLabLocationsMiddleware;

final class JsonGetAllLabLocationsMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new JsonGetAllLabLocationsMiddleware(
            $container->get(GetAllLabLocationsInterface::class)
        );
    }
}
