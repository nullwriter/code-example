<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\ProcessData;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\ConfirmMiddleware;

final class ConfirmMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new ConfirmMiddleware(
            $container->get(IsStaffPersonInterface::class),
            $container->get(ConfirmMutexFactory::NAME),
            $container->get(EntityManagerInterface::class)->getConnection(),
            $container->get(ProcessData::class)
        );
    }
}
