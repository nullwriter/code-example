<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\GetReworksMiddleware;

final class GetReworksMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new GetReworksMiddleware(
            $container->get(SearchBuildByIdInterface::class)
        );
    }
}
