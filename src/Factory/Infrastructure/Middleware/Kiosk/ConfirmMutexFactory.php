<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use malkusch\lock\mutex\Mutex;
use malkusch\lock\mutex\PHPRedisMutex;
use Redis;

final class ConfirmMutexFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : Mutex
    {
        return new PHPRedisMutex([$container->get(Redis::class)], 'drop-off-confirm');
    }
}
