<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Ticket\PersistTicketInterface;
use Pinpoint\Infrastructure\Mailer\Ticket\NotifyAboutTicketCreation;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\SendNotification;

final class SendNotificationFactory
{
    public function __invoke(ContainerInterface $container) : SendNotification
    {
        return new SendNotification(
            $container->get(NotifyAboutTicketCreation::class),
            $container->get(PersistTicketInterface::class)
        );
    }
}
