<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Ticket\WorkRequest\DisplayIdGenerator;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateWorkRequest;

final class CreateWorkRequestFactory
{
    public function __invoke(ContainerInterface $container) : CreateWorkRequest
    {
        return new CreateWorkRequest(
            $container->get(DisplayIdGenerator::class)
        );
    }
}
