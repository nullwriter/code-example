<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Infrastructure\ITrack\Unit\TransferITrackUnit;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateBatch;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateTicket;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\ProcessData;

final class ProcessDataFactory
{
    public function __invoke(ContainerInterface $container) : ProcessData
    {
        return new ProcessData(
            $container->get(CreateBatch::class),
            $container->get(CreateTicket::class),
            $container->get(TransferITrackUnit::class)
        );
    }
}
