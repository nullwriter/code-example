<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Ticket\Batch\DisplayIdGenerator;
use Pinpoint\Domain\Ticket\Batch\PersistBatchInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateBatch;

final class CreateBatchFactory
{
    public function __invoke(ContainerInterface $container) : CreateBatch
    {
        return new CreateBatch(
            $container->get(DisplayIdGenerator::class),
            $container->get(PersistBatchInterface::class)
        );
    }
}
