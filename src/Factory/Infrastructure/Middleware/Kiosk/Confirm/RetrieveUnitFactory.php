<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Domain\Unit\HasActiveTicketInterface;
use Pinpoint\Domain\Unit\PersistUnitInterface;
use Pinpoint\Domain\Unit\SearchUnitByIdInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\RetrieveUnit;
use Pinpoint\Infrastructure\TransferManager\TransferManager;

final class RetrieveUnitFactory
{
    public function __invoke(ContainerInterface $container) : RetrieveUnit
    {
        return new RetrieveUnit(
            $container->get(TransferManager::class),
            $container->get(SearchUnitByIdInterface::class),
            $container->get(SearchBuildByIdInterface::class),
            $container->get(HasActiveTicketInterface::class),
            $container->get(PersistUnitInterface::class)
        );
    }
}
