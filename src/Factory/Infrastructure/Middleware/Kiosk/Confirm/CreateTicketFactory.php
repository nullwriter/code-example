<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Person\LookupAndStorePersonByDsIdInterface;
use Pinpoint\Domain\Ticket\DisplayIdGenerator;
use Pinpoint\Domain\Ticket\PersistTicketInterface;
use Pinpoint\Infrastructure\Mailer\Ticket\NotifyAboutTicketCreation;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateRework;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateTicket;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateWorkRequest;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\RetrieveUnit;

final class CreateTicketFactory
{
    public function __invoke(ContainerInterface $container) : CreateTicket
    {
        return new CreateTicket(
            $container->get(DisplayIdGenerator::class),
            $container->get(PersistTicketInterface::class),
            $container->get(RetrieveUnit::class),
            $container->get(CreateWorkRequest::class),
            $container->get(CreateRework::class),
            $container->get(LookupAndStorePersonByDsIdInterface::class),
            $container->get(NotifyAboutTicketCreation::class)
        );
    }
}
