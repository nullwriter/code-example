<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff\Confirm;

use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Domain\Ticket\WorkRequest\DisplayIdGenerator;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\Confirm\CreateRework;

final class CreateReworkFactory
{
    public function __invoke(ContainerInterface $container) : CreateRework
    {
        return new CreateRework(
            $container->get(SearchReworkByIdInterface::class),
            $container->get(DisplayIdGenerator::class)
        );
    }
}
