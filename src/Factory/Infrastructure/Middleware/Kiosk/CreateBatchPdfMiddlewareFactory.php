<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Ticket\Batch\SearchBatchByIdInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\CreateBatchPdfMiddleware;
use Pinpoint\Infrastructure\Pdf\PdfGenerator;
use Pinpoint\Infrastructure\Service\Ticket\Pdf\PdfBatchGenerator;
use Pinpoint\Infrastructure\Service\Ticket\Pdf\PdfTicketGenerator;
use Pinpoint\Infrastructure\Service\Ticket\Pdf\XmlBatchGenerator;

final class CreateBatchPdfMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new CreateBatchPdfMiddleware(
            $container->get(PdfGenerator::class),
            $container->get(SearchBatchByIdInterface::class),
            $container->get(XmlBatchGenerator::class)
        );
    }
}
