<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\GetAllActiveBuildsInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\AddUnitsMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class AddUnitsMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new AddUnitsMiddleware(
            $container->get(GetAllActiveBuildsInterface::class),
            $container->get(HtmlResponseRenderer::class)
        );
    }
}
