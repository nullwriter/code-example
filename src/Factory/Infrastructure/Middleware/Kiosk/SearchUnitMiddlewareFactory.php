<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Rework\GetActiveAssignedReworksForUnitInterface;
use Pinpoint\Domain\Unit\HasActiveTicketInterface;
use Pinpoint\Domain\Unit\SearchUnitByIdentifierInterface;
use Pinpoint\Infrastructure\ITrack\Unit\LookupAndStoreITrackUnitByIdentifier;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\SearchUnitMiddleware;
use Pinpoint\Infrastructure\TransferManager\TransferManager;

final class SearchUnitMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new SearchUnitMiddleware(
            $container->get(LookupAndStoreITrackUnitByIdentifier::class),
            $container->get(SearchUnitByIdentifierInterface::class),
            $container->get(GetActiveAssignedReworksForUnitInterface::class),
            $container->get(HasActiveTicketInterface::class),
            $container->get(TransferManager::class)
        );
    }
}
