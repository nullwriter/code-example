<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Ticket\Batch\SearchBatchByIdInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\CreateTicketPdfMiddleware;
use Pinpoint\Infrastructure\Pdf\PdfGenerator;
use Pinpoint\Infrastructure\Service\Ticket\Pdf\XmlTicketGenerator;

final class CreateTicketPdfMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new CreateTicketPdfMiddleware(
            $container->get(PdfGenerator::class),
            $container->get(SearchBatchByIdInterface::class),
            $container->get(XmlTicketGenerator::class)
        );
    }
}
