<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Kiosk\DropOff;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Ticket\Batch\SearchBatchByIdInterface;
use Pinpoint\Infrastructure\Middleware\Kiosk\DropOff\SummaryMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class SummaryMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new SummaryMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(SearchBatchByIdInterface::class)
        );
    }
}
