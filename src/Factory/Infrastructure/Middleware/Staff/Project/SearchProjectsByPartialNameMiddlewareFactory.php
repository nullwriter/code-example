<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Project\LookupProjectByPartialNameInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Project\SearchProjectsByPartialNameMiddleware;

final class SearchProjectsByPartialNameMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new SearchProjectsByPartialNameMiddleware(
            $container->get(LookupProjectByPartialNameInterface::class)
        );
    }
}
