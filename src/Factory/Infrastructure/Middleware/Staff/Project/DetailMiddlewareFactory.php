<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Project\GetAllProjectsInterface;
use Pinpoint\Domain\Project\GetAllProjectTypesInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Project\DetailMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class DetailMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new DetailMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(SearchProjectByIdInterface::class),
            $container->get(GetAllProjectTypesInterface::class)
        );
    }
}
