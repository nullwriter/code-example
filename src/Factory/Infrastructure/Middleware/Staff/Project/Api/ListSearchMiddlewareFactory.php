<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Factory\Infrastructure\Service\Project\PerformSearchFactory;
use Pinpoint\Infrastructure\ListSearch\JsonListSearchMiddleware;

final class ListSearchMiddlewareFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new JsonListSearchMiddleware(
            $container->get(PerformSearchFactory::NAME)
        );
    }
}
