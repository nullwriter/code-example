<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Infrastructure\ITrack\Build\RefreshITrackBuild;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\RefreshBuildMiddleware;

final class RefreshBuildMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new RefreshBuildMiddleware(
            $container->get(SearchBuildByIdInterface::class),
            $container->get(RefreshITrackBuild::class)
        );
    }
}
