<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\PersistBuildInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Factory\Infrastructure\Form\AddBuildFormFactory;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\AddBuildLevelMiddleware;

final class AddBuildLevelMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new AddBuildLevelMiddleware(
            $container->get(PersistProjectInterface::class),
            $container->get(SearchProjectByIdInterface::class),
            $container->get(PersistBuildInterface::class),
            $container->get(IsStaffPersonInterface::class),
            $container->get(AddBuildFormFactory::NAME)
        );
    }
}
