<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\SearchPersonByDsIdInterface;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Factory\Infrastructure\Form\ProjectFormFactory;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\CreateProjectMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;
use Zend\Expressive\Helper\UrlHelper;

final class CreateProjectMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new CreateProjectMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(PersistProjectInterface::class),
            $container->get(SearchPersonByDsIdInterface::class),
            $container->get(SearchTypeByIdInterface::class),
            $container->get(UrlHelper::class),
            $container->get(ProjectFormFactory::class)
        );
    }
}
