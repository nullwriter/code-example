<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\SearchPersonByDsIdInterface;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Domain\Project\SearchTypeByNameInterface;
use Pinpoint\Factory\Infrastructure\Form\ProjectFormFactory;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\UpdateProjectMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class UpdateProjectMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new UpdateProjectMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(PersistProjectInterface::class),
            $container->get(SearchProjectByIdInterface::class),
            $container->get(SearchPersonByDsIdInterface::class),
            $container->get(SearchTypeByNameInterface::class),
            $container->get(ProjectFormFactory::NAME)
        );
    }
}
