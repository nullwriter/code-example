<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\PersistBuildInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Factory\Infrastructure\Form\UpdateBuildFormFactory;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\UpdateBuildMiddleware;

final class UpdateBuildMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new UpdateBuildMiddleware(
            $container->get(SearchBuildByIdInterface::class),
            $container->get(PersistBuildInterface::class),
            $container->get(UpdateBuildFormFactory::NAME)
        );
    }
}
