<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\GetBuildMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class GetBuildMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new GetBuildMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(SearchBuildByIdInterface::class)
        );
    }
}
