<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Project\Api\ChangeProjectStatusMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class ChangeProjectStatusMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new ChangeProjectStatusMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(PersistProjectInterface::class),
            $container->get(SearchProjectByIdInterface::class),
            $container->get(IsStaffPersonInterface::class)
        );
    }
}
