<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Project;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Project\GetAllProjectTypesInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Project\ListMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class ListMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new ListMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(GetAllProjectTypesInterface::class)
        );
    }
}
