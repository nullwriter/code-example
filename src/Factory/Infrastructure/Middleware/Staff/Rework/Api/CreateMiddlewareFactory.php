<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Rework\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Factory\Infrastructure\Form\ReworkFormFactory;
use Pinpoint\Infrastructure\Middleware\Staff\Rework\Api\CreateMiddleware;
use Zend\Expressive\Helper\UrlHelper;

final class CreateMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new CreateMiddleware(
            $container->get(ReworkFormFactory::NAME),
            $container->get(PersistReworkInterface::class),
            $container->get(UrlHelper::class)
        );
    }
}
