<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Rework\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Factory\Infrastructure\Form\ReworkFormFactory;
use Pinpoint\Infrastructure\Middleware\Staff\Rework\Api\UpdateMiddleware;

final class UpdateMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new UpdateMiddleware(
            $container->get(SearchReworkByIdInterface::class),
            $container->get(ReworkFormFactory::NAME),
            $container->get(PersistReworkInterface::class),
            $container->get(IsStaffPersonInterface::class)
        );
    }
}
