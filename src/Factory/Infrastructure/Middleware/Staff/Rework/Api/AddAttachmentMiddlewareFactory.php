<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Rework\Api;

use DASPRiD\TreeReader\TreeReader;
use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Rework\Api\AddAttachmentMiddleware;

final class AddAttachmentMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        $config = (new TreeReader($container->get('config'), 'config'))->getChildren('rework');

        return new AddAttachmentMiddleware(
            $container->get(SearchReworkByIdInterface::class),
            $container->get(PersistReworkInterface::class),
            $config->getString('temp_path'),
            $config->getString('attachment_path')
        );
    }
}
