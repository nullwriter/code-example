<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Rework;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Project\GetAllProjectsInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Rework\ListMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class ListMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new ListMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(IsStaffPersonInterface::class),
            $container->get(GetAllProjectsInterface::class)
        );
    }
}
