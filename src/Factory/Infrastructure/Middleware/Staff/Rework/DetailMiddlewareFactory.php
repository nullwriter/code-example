<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Rework;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\GetAllActiveBuildsInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Project\GetAllProjectsInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Rework\DetailMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class DetailMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new DetailMiddleware(
            $container->get(HtmlResponseRenderer::class),
            $container->get(SearchReworkByIdInterface::class),
            $container->get(IsStaffPersonInterface::class),
            $container->get(GetAllActiveBuildsInterface::class)
        );
    }
}
