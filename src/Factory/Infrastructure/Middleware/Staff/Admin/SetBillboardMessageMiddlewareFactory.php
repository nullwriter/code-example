<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Admin;

use DASPRiD\TreeReader\TreeReader;
use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Admin\SetBillboardMessageMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class SetBillboardMessageMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): MiddlewareInterface
    {
        $config = (new TreeReader($container->get('config'), 'config'))
            ->getChildren('billboard')
            ->getChildren('message_presets');

        return new SetBillboardMessageMiddleware(
            $container->get(GetAllLabLocationsInterface::class),
            $container->get(HtmlResponseRenderer::class),
            iterator_to_array($config->getIterator())
        );
    }
}
