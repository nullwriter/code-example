<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Admin;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Admin\HomeMiddleware;
use Pinpoint\Infrastructure\Response\HtmlResponseRenderer;

final class HomeMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new HomeMiddleware(
            $container->get(HtmlResponseRenderer::class)
        );
    }
}
