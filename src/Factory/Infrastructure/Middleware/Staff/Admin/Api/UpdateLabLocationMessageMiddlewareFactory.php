<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Staff\Admin\Api;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\PersistLabLocationInterface;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Middleware\Staff\Admin\Api\UpdateLabLocationMessageMiddleware;

final class UpdateLabLocationMessageMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new UpdateLabLocationMessageMiddleware(
            $container->get(PersistLabLocationInterface::class),
            $container->get(SearchLabLocationsByIdInterface::class)
        );
    }
}
