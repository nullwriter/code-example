<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Middleware\Person;

use Interop\Container\ContainerInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\LookupPersonsByPartialNameInterface;
use Pinpoint\Infrastructure\Middleware\Person\SearchPersonsByPartialNameMiddleware;

final class SearchPersonsByPartialNameMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewareInterface
    {
        return new SearchPersonsByPartialNameMiddleware(
            $container->get(LookupPersonsByPartialNameInterface::class)
        );
    }
}
