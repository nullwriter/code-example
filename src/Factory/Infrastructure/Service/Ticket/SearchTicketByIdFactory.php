<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Ticket;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Ticket\SearchTicketByIdInterface;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Service\Ticket\SearchTicketById;

final class SearchTicketByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchTicketByIdInterface
    {
        return new SearchTicketById(
            $container->get(EntityManagerInterface::class)->getRepository(Ticket::class)
        );
    }
}
