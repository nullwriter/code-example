<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Ticket;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Ticket\SearchTicketByDisplayIdInterface;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Service\Ticket\SearchTicketByDisplayId;

final class SearchTicketByDisplayIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchTicketByDisplayIdInterface
    {
        return new SearchTicketByDisplayId(
            $container->get(EntityManagerInterface::class)->getRepository(Ticket::class)
        );
    }
}
