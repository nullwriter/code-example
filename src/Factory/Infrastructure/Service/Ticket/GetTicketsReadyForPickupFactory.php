<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Ticket;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Ticket\GetTicketsReadyForPickupInterface;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Service\Ticket\GetTicketsReadyForPickup;

final class GetTicketsReadyForPickupFactory
{
    public function __invoke(ContainerInterface $container) : GetTicketsReadyForPickupInterface
    {
        return new GetTicketsReadyForPickup(
            $container->get(EntityManagerInterface::class)->getRepository(Ticket::class)
        );
    }
}
