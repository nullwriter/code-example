<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Person;

use DASPRiD\TreeReader\TreeReader;
use Http\Client\HttpClient;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Person\RetrieveDsIdByBadgeNumberInterface;
use Pinpoint\Infrastructure\Service\Person\RetrieveDsIdByBadgeNumber;
use Zend\Diactoros\Uri;

final class RetrieveDsIdByBadgeNumberFactory
{
    public function __invoke(ContainerInterface $container) : RetrieveDsIdByBadgeNumberInterface
    {
        $config = (new TreeReader($container->get('config'), 'config'))->getChildren('badge_service');

        return new RetrieveDsIdByBadgeNumber(
            $container->get(HttpClient::class),
            new Uri($config->getString('base_uri'))
        );
    }
}
