<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Person;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Person\SearchPersonByIdInterface;
use Pinpoint\Infrastructure\Service\Person\SearchPersonById;

final class SearchPersonByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchPersonByIdInterface
    {
        return new SearchPersonById(
            $container->get(EntityManagerInterface::class)->getRepository(Person::class)
        );
    }
}
