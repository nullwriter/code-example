<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Build;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Infrastructure\Service\Build\SearchBuildById;

final class SearchBuildByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchBuildByIdInterface
    {
        return new SearchBuildById(
            $container->get(EntityManagerInterface::class)->getRepository(Build::class)
        );
    }
}
