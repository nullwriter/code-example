<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Build;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Build\GetAllActiveBuildsInterface;
use Pinpoint\Infrastructure\Service\Build\GetAllActiveBuilds;

final class GetAllBuildsFactory
{
    public function __invoke(ContainerInterface $container) : GetAllActiveBuildsInterface
    {
        return new GetAllActiveBuilds(
            $container->get(EntityManagerInterface::class)->getRepository(Build::class)
        );
    }
}
