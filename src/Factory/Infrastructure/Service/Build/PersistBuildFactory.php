<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Build;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Build\PersistBuildInterface;
use Pinpoint\Infrastructure\Service\Build\PersistBuild;

final class PersistBuildFactory
{
    public function __invoke(ContainerInterface $container) : PersistBuildInterface
    {
        return new PersistBuild(
            $container->get(EntityManagerInterface::class)
        );
    }
}
