<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Rework;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Rework\Rework;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Service\Rework\SearchReworkById;

final class SearchReworkByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchReworkByIdInterface
    {
        return new SearchReworkById(
            $container->get(EntityManagerInterface::class)->getRepository(Rework::class)
        );
    }
}
