<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Rework;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Infrastructure\ListSearch\Field;
use Pinpoint\Infrastructure\ListSearch\PerformSearch;

final class PerformSearchFactory
{
    public const NAME = self::class;

    public function __invoke(ContainerInterface $container) : PerformSearch
    {
        return new PerformSearch(
            $container->get(EntityManagerInterface::class)->getConnection(),
            'rework_search',
            new Field('rework_id', Field::TEXT, true),
            new Field('name', Field::TEXT),
            new Field('active', Field::BOOLEAN),
            new Field('project', Field::TEXT),
            new Field('builds', Field::TEXT),
            new Field('last_activity_date', Field::DATETIME)
        );
    }
}
