<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Rework;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Rework\GetActiveAssignedReworksForUnitInterface;
use Pinpoint\Domain\Rework\Rework;
use Pinpoint\Infrastructure\Service\Rework\GetActiveAssignedReworksForUnit;

final class GetActiveAssignedReworksForUnitFactory
{
    public function __invoke(ContainerInterface $container) : GetActiveAssignedReworksForUnitInterface
    {
        return new GetActiveAssignedReworksForUnit(
            $container->get(EntityManagerInterface::class)->getRepository(Rework::class)
        );
    }
}
