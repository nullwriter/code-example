<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Rework;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Infrastructure\Service\Rework\PersistRework;

final class PersistReworkFactory
{
    public function __invoke(ContainerInterface $container) : PersistReworkInterface
    {
        return new PersistRework(
            $container->get(EntityManagerInterface::class)
        );
    }
}
