<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\LabLocation;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\LabLocation\GetLabLocationStatisticsInterface;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Service\LabLocation\GetLabLocationStatistics;

final class GetLabLocationStatisticsFactory
{
    public function __invoke(ContainerInterface $container) : GetLabLocationStatisticsInterface
    {
        return new GetLabLocationStatistics(
            $container->get(EntityManagerInterface::class)->getRepository(Ticket::class)
        );
    }
}
