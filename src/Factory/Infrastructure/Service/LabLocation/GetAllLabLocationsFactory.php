<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\LabLocation;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Pinpoint\Domain\LabLocation\LabLocation;
use Pinpoint\Infrastructure\Service\LabLocation\GetAllAllLabLocations;

final class GetAllLabLocationsFactory
{
    public function __invoke(ContainerInterface $container) : GetAllLabLocationsInterface
    {
        return new GetAllAllLabLocations(
            $container->get(EntityManagerInterface::class)->getRepository(LabLocation::class)
        );
    }
}
