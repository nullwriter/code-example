<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\LabLocation;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\LabLocation\LabLocation;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Service\LabLocation\SearchLabLocationById;

final class SearchLabLocationsByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchLabLocationsByIdInterface
    {
        return new SearchLabLocationById(
            $container->get(EntityManagerInterface::class)->getRepository(LabLocation::class)
        );
    }
}
