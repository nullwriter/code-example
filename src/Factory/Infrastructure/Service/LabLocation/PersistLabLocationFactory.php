<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\LabLocation;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\LabLocation\PersistLabLocationInterface;
use Pinpoint\Infrastructure\Service\LabLocation\PersistLabLocation;

final class PersistLabLocationFactory
{
    public function __invoke(ContainerInterface $container) : PersistLabLocationInterface
    {
        return new PersistLabLocation(
            $container->get(EntityManagerInterface::class)
        );
    }
}
