<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Project;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Project\GetAllProjectsInterface;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Infrastructure\Service\Project\GetAllProjects;

final class GetAllProjectsFactory
{
    public function __invoke(ContainerInterface $container) : GetAllProjectsInterface
    {
        return new GetAllProjects(
            $container->get(EntityManagerInterface::class)->getRepository(Project::class)
        );
    }
}
