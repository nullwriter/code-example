<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Project;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Domain\Project\Type;
use Pinpoint\Infrastructure\Service\Project\SearchTypeById;

final class SearchTypeByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchTypeByIdInterface
    {
        return new SearchTypeById(
            $container->get(EntityManagerInterface::class)->getRepository(Type::class)
        );
    }
}
