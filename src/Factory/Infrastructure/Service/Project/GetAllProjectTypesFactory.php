<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Project;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Project\Type;
use Pinpoint\Infrastructure\Service\Project\GetAllProjectTypes;

final class GetAllProjectTypesFactory
{
    public function __invoke(ContainerInterface $container) : GetAllProjectTypes
    {
        return new GetAllProjectTypes(
            $container->get(EntityManagerInterface::class)->getRepository(Type::class)
        );
    }
}
