<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Service\Project;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Service\Project\SearchProjectById;

final class SearchProjectByIdFactory
{
    public function __invoke(ContainerInterface $container) : SearchProjectByIdInterface
    {
        return new SearchProjectById(
            $container->get(EntityManagerInterface::class)->getRepository(Project::class),
            $container->get(EntityManagerInterface::class)
        );
    }
}
