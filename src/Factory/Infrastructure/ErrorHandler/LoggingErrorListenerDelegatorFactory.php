<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\ErrorHandler;

use Interop\Container\ContainerInterface;
use Pinpoint\Infrastructure\ErrorHandler\LoggingErrorListener;
use Psr\Log\LoggerInterface;
use Zend\Stratigility\Middleware\ErrorHandler;

final class LoggingErrorListenerDelegatorFactory
{
    public function __invoke(ContainerInterface $container, string $name, callable $callback) : ErrorHandler
    {
        $errorHandler = $callback();
        assert($errorHandler instanceof ErrorHandler);

        $errorHandler->attachListener(new LoggingErrorListener($container->get(LoggerInterface::class)));
        return $errorHandler;
    }
}
