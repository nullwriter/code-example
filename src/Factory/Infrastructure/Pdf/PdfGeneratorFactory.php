<?php
declare(strict_types = 1);

namespace Pinpoint\Factory\Infrastructure\Pdf;

use DASPRiD\TreeReader\TreeReader;
use Interop\Container\ContainerInterface;
use Pinpoint\Infrastructure\Pdf\PdfGenerator;

final class PdfGeneratorFactory
{
    public function __invoke(ContainerInterface $container) : PdfGenerator
    {
        $config = (new TreeReader($container->get('config'), 'config'))->getChildren('fop');

        return new PdfGenerator(
            $config->getString('path'),
            $config->getString('base_dir')
        );
    }
}
