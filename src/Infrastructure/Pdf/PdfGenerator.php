<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Pdf;

use Pinpoint\Infrastructure\Pdf\Exception\FopException;

final class PdfGenerator
{
    /**
     * @var string
     */
    public $fopPath;

    /**
     * @var string
     */
    private $baseDirectory;

    public function __construct(string $fopPath, string $baseDirectory)
    {
        $this->fopPath = $fopPath;
        $this->baseDirectory = realpath($baseDirectory);
    }

    public function __invoke(string $xml, string $templateFilename) : string
    {
        $xslPath = implode('/', [$this->baseDirectory, $templateFilename]);

        $command = sprintf(
            '%s -xml - -xsl %s -pdf - -c %s',
            escapeshellcmd($this->fopPath),
            escapeshellarg($xslPath),
            escapeshellarg(implode('/', [$this->baseDirectory, 'fop.xml']))
        );

        $descriptorSpecs = [
            ['pipe', 'r'],
            ['pipe', 'w'],
            ['pipe', 'w'],
        ];

        $process = proc_open(
            $command,
            $descriptorSpecs,
            $pipes,
            $this->baseDirectory
        );

        if (! is_resource($process)) {
            throw FopException::fromProcessCreateFailure();
        }

        fwrite($pipes[0], $xml);
        fclose($pipes[0]);

        stream_set_blocking($pipes[1], false);
        stream_set_blocking($pipes[2], false);

        $stderr = '';
        $stdout = '';

        while (! feof($pipes[1]) || ! feof($pipes[2])) {
            if (! feof($pipes[1])) {
                $stdout .= stream_get_contents($pipes[1]);
            }

            if (! feof($pipes[2])) {
                $stderr .= stream_get_contents($pipes[2]);
            }
        }

        fclose($pipes[1]);
        fclose($pipes[2]);

        $exitCode = proc_close($process);

        if ($exitCode > 0) {
            throw FopException::fromRunError($command, $stderr);
        }

        return $stdout;
    }
}
