<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Pdf\Exception;

use DomainException;

final class FopException extends DomainException implements ExceptionInterface
{
    public static function fromProcessCreateFailure() : self
    {
        return new self('Could not create process');
    }

    public static function fromRunError(string $command, string $errorMessage) : self
    {
        return new self(sprintf(
            "Error while running `%s`:\n%s",
            $command,
            $errorMessage
        ));
    }
}
