<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Pdf\Exception;

use Throwable;

interface ExceptionInterface extends Throwable
{
}
