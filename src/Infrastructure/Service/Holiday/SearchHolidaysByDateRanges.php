<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Holiday;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Holiday\DateRange;
use Pinpoint\Domain\Holiday\Holiday;
use Pinpoint\Domain\Holiday\SearchHolidaysByDateRangesInterface;

final class SearchHolidaysByDateRanges implements SearchHolidaysByDateRangesInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @return Holiday[]
     */
    public function __invoke(DateRange ...$dateRanges) : array
    {
        if (empty($dateRanges)) {
            return [];
        }

        $queryBuilder = $this->entityRepository->createQueryBuilder('holiday');
        $parameterIndex = 0;

        foreach ($dateRanges as $dateRange) {
            $queryBuilder->andWhere(sprintf(
                'holiday.date BETWEEN :date%d AND :date%d',
                $parameterIndex,
                $parameterIndex + 1
            ));
            $queryBuilder->setParameter('date' . $parameterIndex, $dateRange->getStartDate());
            $queryBuilder->setParameter('date' . ($parameterIndex + 1), $dateRange->getEndDate());
            $parameterIndex += 2;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
