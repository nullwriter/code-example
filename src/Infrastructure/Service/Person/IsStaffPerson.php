<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Person;

use Pinpoint\Domain\Person\Exception\InvalidTypeException;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;
use Psr\SimpleCache\CacheInterface;
use Zend\Ldap\Filter\AndFilter;
use Zend\Ldap\Filter\OrFilter;
use Zend\Ldap\Ldap;

final class IsStaffPerson implements IsStaffPersonInterface
{
    private const CACHE_TTL = 60 * 60 * 12;

    /**
     * @var Ldap
     */
    private $ldap;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var int
     */
    private $techGroupId;

    /**
     * @var int
     */
    private $epmGroupId;

    /**
     * @var int
     */
    private $managerGroupId;

    public function __construct(
        Ldap $ldap,
        CacheInterface $cache,
        int $techGroupId,
        int $epmGroupId,
        int $managerGroupId
    ) {
        $this->ldap = $ldap;
        $this->cache = $cache;
        $this->techGroupId = $techGroupId;
        $this->epmGroupId = $epmGroupId;
        $this->managerGroupId = $managerGroupId;
    }

    public function __invoke(Person $person, int $minLevel = self::TECH) : bool
    {
        $groupIds = [];

        if ($minLevel <= self::TECH) {
            $groupIds[] = $this->techGroupId;
        }

        if ($minLevel <= self::EPM) {
            $groupIds[] = $this->epmGroupId;
        }

        if ($minLevel <= self::MANAGER) {
            $groupIds[] = $this->managerGroupId;
        }

        $cacheId = sprintf('is_staff_person_%s_%s', $person->getDsId(), implode('_', $groupIds));

        if ($this->cache->has($cacheId)) {
            return $this->cache->get($cacheId);
        }

        $filter = new AndFilter([
            sprintf('appleUniqueMember=%s', $person->getDsId()),
            new OrFilter(array_map(function (int $groupId) : string {
                return sprintf('appleGroupID=%d', $groupId);
            }, $groupIds)),
        ]);

        $result = $this->ldap->search($filter, 'ou=groupmembers,ou=Groups,o=apple', Ldap::SEARCH_SCOPE_ONE, [
            'appleUniqueMember'
        ]);

        $isStaffPerson = count($result) > 0;

        $this->cache->set($cacheId, $isStaffPerson, self::CACHE_TTL);
        return $isStaffPerson;
    }
}
