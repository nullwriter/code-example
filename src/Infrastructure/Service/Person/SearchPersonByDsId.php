<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Person;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Person\DsId;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Person\SearchPersonByDsIdInterface;

final class SearchPersonByDsId implements SearchPersonByDsIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(DsId $dsId) : ?Person
    {
        return $this->entityRepository->findOneBy(['dsId' => $dsId]);
    }
}
