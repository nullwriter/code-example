<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Person;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Person\SearchPersonByIdInterface;

final class SearchPersonById implements SearchPersonByIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(Id $personId) : ?Person
    {
        return $this->entityRepository->find($personId);
    }
}
