<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Build;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Build\GetAllActiveBuildsInterface;

final class GetAllActiveBuilds implements GetAllActiveBuildsInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke() : array
    {
        $queryBuilder = $this->entityRepository->createQueryBuilder('build');
        $queryBuilder->innerJoin('build.project', 'project');
        $queryBuilder->where('project.active = true');

        return $queryBuilder->getQuery()->getResult();
    }
}
