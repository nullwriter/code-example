<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Build;

use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Build\PersistBuildInterface;

final class PersistBuild implements PersistBuildInterface
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function __invoke(Build $build) : void
    {
        $this->objectManager->persist($build);
        $this->objectManager->flush();
    }
}
