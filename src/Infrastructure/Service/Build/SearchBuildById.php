<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Build;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Domain\Id;

final class SearchBuildById implements SearchBuildByIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(Id $buildId) : ?Build
    {
        return $this->entityRepository->find($buildId);
    }
}
