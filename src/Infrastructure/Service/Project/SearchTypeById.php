<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Project;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Domain\Project\Type;

final class SearchTypeById implements SearchTypeByIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(Id $id) : ?Type
    {
        return $this->entityRepository->find($id);
    }
}
