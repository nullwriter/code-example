<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Project;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Project\SearchTypeByNameInterface;
use Pinpoint\Domain\Project\Type;

final class SearchTypeByName implements SearchTypeByNameInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(string $name) : ?Type
    {
        return $this->entityRepository->findOneBy([
            'name' => $name,
        ]);
    }
}
