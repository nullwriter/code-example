<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Project;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Project\GetAllProjectsInterface;

final class GetAllProjects implements GetAllProjectsInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke() : array
    {
        return $this->entityRepository->findAll();
    }
}
