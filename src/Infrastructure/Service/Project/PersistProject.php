<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Project;

use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\Project;

final class PersistProject implements PersistProjectInterface
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function __invoke(Project $project) : void
    {
        $this->objectManager->persist($project);
        $this->objectManager->flush();
    }
}
