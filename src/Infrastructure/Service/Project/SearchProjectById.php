<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Project;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;

final class SearchProjectById implements SearchProjectByIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityRepository $entityRepository, EntityManagerInterface $entityManager)
    {
        $this->entityRepository = $entityRepository;
        $this->entityManager = $entityManager;
    }

    public function __invoke(Id $id) : ?Project
    {
        return $this->entityRepository->findOneBy([
            'id' => $id,
        ]);
    }
}
