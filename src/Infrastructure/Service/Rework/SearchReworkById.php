<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Rework;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Rework\Rework;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;

final class SearchReworkById implements SearchReworkByIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(Id $reworkId) : ?Rework
    {
        return $this->entityRepository->find($reworkId);
    }
}
