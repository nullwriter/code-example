<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Ticket\SearchTicketForUnitReadyToPickUpInterface;
use Pinpoint\Domain\Ticket\Status;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Domain\Unit\Unit;
use Pinpoint\Infrastructure\Doctrine\Type\Ticket\StatusType;

final class SearchTicketForUnitReadyToPickUp implements SearchTicketForUnitReadyToPickUpInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityRepository $entityRepository, EntityManagerInterface $entityManager)
    {
        $this->entityRepository = $entityRepository;
        $this->entityManager = $entityManager;
    }

    public function __invoke(Unit $unit) : ?Ticket
    {
        $queryBuilder = $this->entityRepository->createQueryBuilder('ticket');
        $queryBuilder->innerJoin('ticket.currentState', 'currentState');
        $queryBuilder->where('currentState.status = :readyForPickup');
        $queryBuilder->andWhere('ticket.unit = :unit');

        $queryBuilder->setParameter('unit', $unit);
        $queryBuilder->setParameter('readyForPickup', Status::READY_FOR_PICKUP(), StatusType::NAME);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
