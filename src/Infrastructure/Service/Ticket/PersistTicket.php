<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket;

use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Ticket\PersistTicketInterface;
use Pinpoint\Domain\Ticket\Ticket;

final class PersistTicket implements PersistTicketInterface
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function __invoke(Ticket $ticket) : void
    {
        $this->objectManager->persist($ticket);
        $this->objectManager->flush();
    }
}
