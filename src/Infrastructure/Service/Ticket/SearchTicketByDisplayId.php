<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Ticket\DisplayId;
use Pinpoint\Domain\Ticket\SearchTicketByDisplayIdInterface;
use Pinpoint\Domain\Ticket\Ticket;

final class SearchTicketByDisplayId implements SearchTicketByDisplayIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(DisplayId $displayId) : ?Ticket
    {
        return $this->entityRepository->findOneBy([
            'displayId' => $displayId,
        ]);
    }
}
