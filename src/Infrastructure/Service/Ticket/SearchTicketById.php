<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket;

use Doctrine\ORM\EntityRepository;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Ticket\SearchTicketByIdInterface;
use Pinpoint\Domain\Ticket\Ticket;

final class SearchTicketById implements SearchTicketByIdInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(Id $id) : ?Ticket
    {
        return $this->entityRepository->findOneBy([
            'id' => $id,
        ]);
    }
}
