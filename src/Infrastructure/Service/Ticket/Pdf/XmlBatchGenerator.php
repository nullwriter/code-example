<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket\Pdf;

use Pinpoint\Domain\Ticket\Batch\Batch;
use XMLWriter;

final class XmlBatchGenerator
{
    public function __invoke(Batch $batch) : string
    {
        $tickets = $batch->getTickets();

        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'utf-8');
        $writer->startElement('data');

        $writer->startElement('batch');
        $writer->writeElement('batch-id', (string) $batch->getDisplayId());
        $writer->writeElement('timestamp', date('m-d-Y H:i:s'));
        $writer->writeElement('location', (string) $tickets[0]->getUnit()->getLabLocation());
        $writer->writeElement('numUnits', (string) count($tickets));
        $writer->writeElement(
            'customer-person',
            (string) $tickets[0]->getUnit()->getOwner()->getFullName()
        );
        $writer->writeElement('barcode', '*' . $batch->getDisplayId() . '*');

        $writer->startElement('ticket-list');

        foreach ($tickets as $ticket) {
            $writer->writeElement('ticket-id', (string) $ticket->getDisplayId());
        }

        $writer->endElement();
        $writer->endElement();

        $writer->endElement();
        $writer->endDocument();

        return $writer->outputMemory();
    }
}
