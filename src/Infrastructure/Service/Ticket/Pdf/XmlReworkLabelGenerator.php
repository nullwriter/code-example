<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket\Pdf;

use Pinpoint\Domain\Ticket\Ticket;
use XMLWriter;

final class XmlReworkLabelGenerator
{
    public function __invoke(Ticket $ticket) : string
    {
        $unit = $ticket->getUnit();
        $owner = $unit->getOwner();
        $build = $unit->getBuild();
        $project = $build->getProject();

        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'utf-8');
        $writer->startElement('data');
        $writer->startElement('ticket');
        $writer->writeElement('ticket-id', (string) $ticket->getDisplayId());
        $writer->writeElement('ticket-type', '@todo Other');
        $writer->writeElement('timestamp', date('m/d/Y'));
        $writer->writeElement('location', (string) $unit->getLabLocation());
        $writer->writeElement('customer-person', $owner->getFullName());
        $writer->writeElement('project-id', (string) $project->getITrackId());
        $writer->writeElement('project-name', $project->getName());
        $writer->writeElement('build-name', $build->getName());
        $writer->writeElement('unit-id', (string) $ticket->getUnit()->getITrackId());

        $writer->startElement('description-list');

        for ($i = 1; $i < 5; ++$i) {
            $writer->startElement('description');
            $writer->writeElement('text', '@todo Lorem ipsum dolor sit amet, consectetur adipiscing ' . $i);
            $writer->endElement();
        }

        $writer->endElement();

        $writer->writeElement('sysname', $owner->getDepartment()->getName());
        $writer->writeElement('sysname-short', 'ESS');
        $writer->endElement();
        $writer->endElement();
        $writer->endDocument();

        return $writer->outputMemory();
    }
}
