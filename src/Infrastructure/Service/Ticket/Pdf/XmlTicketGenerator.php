<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Service\Ticket\Pdf;

use Pinpoint\Domain\Ticket\Ticket;
use XMLWriter;

final class XmlTicketGenerator
{
    public function __invoke(Ticket ...$tickets) : string
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'utf-8');
        $writer->startElement('data');

        foreach ($tickets as $ticket) {
            $unit = $ticket->getUnit();
            $owner = $unit->getOwner();
            $build = $unit->getBuild();
            $project = $build->getProject();

            $writer->startElement('ticket');
            $writer->writeElement('ticket-id', (string) $ticket->getDisplayId());
            $writer->writeElement('ticket-type', 'Other');
            $writer->writeElement('timestamp', date('m-d-Y H:i:s'));
            $writer->writeElement('location', (string) $unit->getLabLocation());
            $writer->writeElement('customer-person', $owner->getFullName());
            $writer->writeElement('project-id', (string) $project->getITrackId());
            $writer->writeElement('project-name', $project->getName());
            $writer->writeElement('build-name', $build->getName());
            $writer->writeElement('unit-id', (string) $unit->getITrackId());

            $descriptions = '';
            foreach ($ticket->getWorkRequests() as $workRequest) {
                $descriptions .= $workRequest->getDescription() . "\n";
            }

            $writer->writeElement('description', $descriptions);
            $writer->writeElement('barcode', '*' . $ticket->getDisplayId() . '*');
            $writer->writeElement('sysname', $owner->getDepartment()->getName());
            $writer->endElement();
        }

        $writer->endElement();
        $writer->endDocument();

        return $writer->outputMemory();
    }
}
