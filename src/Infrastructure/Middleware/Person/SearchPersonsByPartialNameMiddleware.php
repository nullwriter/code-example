<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Person;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\LookupPersonsByPartialNameInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

final class SearchPersonsByPartialNameMiddleware implements MiddlewareInterface
{
    /**
     * @var LookupPersonsByPartialNameInterface
     */
    private $lookupPersonsByPartialName;

    public function __construct(LookupPersonsByPartialNameInterface $lookupPersonsByPartialName)
    {
        $this->lookupPersonsByPartialName = $lookupPersonsByPartialName;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        if (! array_key_exists('query', $queryParams) || ! is_string($queryParams['query'])) {
            return new JsonResponse(['persons' => []]);
        }

        $staffOnly = 'true' === ($queryParams['staffOnly'] ?? 'false');

        $persons = $this->lookupPersonsByPartialName->__invoke($queryParams['query'], $staffOnly);
        $result = [];

        foreach ($persons as $person) {
            $department = $person->getDepartment();

            $result[] = [
                'dsId' => (string) $person->getDsId(),
                'fullName' => $person->getFullName(),
                'emailAddress' => (string) $person->getEmailAddress(),
                'department' => [
                    'name' => $department->getName(),
                    'number' => $department->getNumber(),
                ],
            ];
        }

        return new JsonResponse([
            'persons' => $result,
        ]);
    }
}
