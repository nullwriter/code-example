<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use DASPRiD\Formidable\Form;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Person\SearchPersonByDsIdInterface;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Domain\Project\SearchTypeByNameInterface;
use Pinpoint\Infrastructure\Form\Data\ProjectData;
use Pinpoint\Infrastructure\Response\FormErrorJsonResponse;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

final class UpdateProjectMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var SearchProjectByIdInterface
     */
    private $searchProjectById;

    /**
     * @var PersistProjectInterface
     */
    private $persistProject;

    /**
     * @var SearchPersonByDsIdInterface
     */
    private $searchPersonByDsId;

    /**
     * @var SearchTypeByNameInterface
     */
    private $searchTypeByName;

    /**
     * @var Form
     */
    private $projectForm;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        PersistProjectInterface $persistProject,
        SearchProjectByIdInterface $searchProjectById,
        SearchPersonByDsIdInterface $searchPersonByDsId,
        SearchTypeByNameInterface $searchTypeByName,
        Form $projectForm
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->searchProjectById = $searchProjectById;
        $this->persistProject = $persistProject;
        $this->searchPersonByDsId = $searchPersonByDsId;
        $this->searchTypeByName = $searchTypeByName;
        $this->projectForm = $projectForm;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $projectId = $request->getAttribute('projectId');
        $project = $this->searchProjectById->__invoke(Id::fromString($projectId));

        if (null === $project) {
            return new EmptyResponse(404);
        }

        $projectForm = $this->projectForm->bindFromRequest($request);

        if ($projectForm->hasErrors()) {
            return new FormErrorJsonResponse($projectForm);
        }

        /* @var $projectData ProjectData */
        $projectData = $projectForm->getValue();
        $project = $projectData->updateProject($project);
        $this->persistProject->__invoke($project);

        return new EmptyResponse();
    }
}
