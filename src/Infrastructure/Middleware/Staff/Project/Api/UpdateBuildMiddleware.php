<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use DASPRiD\Formidable\Form;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\PersistBuildInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Infrastructure\Form\Data\Project\BuildData;
use Pinpoint\Infrastructure\Response\FormErrorJsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

final class UpdateBuildMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchBuildByIdInterface
     */
    private $searchBuildById;

    /**
     * @var PersistBuildInterface
     */
    private $persistBuild;

    /**
     * @var Form
     */
    private $updateBuildForm;

    public function __construct(
        SearchBuildByIdInterface $searchBuildById,
        PersistBuildInterface $persistBuild,
        Form $updateBuildForm
    ) {
        $this->searchBuildById = $searchBuildById;
        $this->persistBuild = $persistBuild;
        $this->updateBuildForm = $updateBuildForm;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $buildId = $request->getAttribute('buildId');
        $build = $this->searchBuildById->__invoke(Id::fromString($buildId));

        if (null === $build) {
            return new EmptyResponse(404);
        }

        $updateBuildForm = $this->updateBuildForm->bindFromRequest($request);

        if ($updateBuildForm->hasErrors()) {
            return new FormErrorJsonResponse($updateBuildForm);
        }

        /* @var $buildData BuildData */
        $buildData = $updateBuildForm->getValue();
        $buildData->updateBuild($build);
        $this->persistBuild->__invoke($build);

        return new EmptyResponse();
    }
}
