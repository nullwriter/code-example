<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

final class GetBuildMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var SearchBuildByIdInterface
     */
    private $searchBuildById;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        SearchBuildByIdInterface $searchBuildById
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->searchBuildById = $searchBuildById;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $buildId = $request->getAttribute('buildId');
        $build = $this->searchBuildById->__invoke(Id::fromString($buildId));

        if (null === $build) {
            return new EmptyResponse(404);
        }

        $reworks = [];

        foreach ($build->getReworks() as $rework) {
            $reworks[] = [
                'id' => (string) $rework->getId(),
                'name' => $rework->getName(),
                'active' => $rework->isActive(),
            ];
        }

        return new JsonResponse([
            'build' => [
                'id' => $buildId,
                'name' => $build->getName(),
                'iTrackId' => $build->getITrackId()
            ],
            'reworks' => $reworks
        ]);
    }
}
