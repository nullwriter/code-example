<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use DASPRiD\Formidable\Form;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\SearchPersonByDsIdInterface;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Infrastructure\Form\Data\Project\ProjectData;
use Pinpoint\Infrastructure\Response\FormErrorJsonResponse;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Expressive\Helper\UrlHelper;

final class CreateProjectMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var PersistProjectInterface
     */
    private $persistProject;

    /**
     * @var SearchPersonByDsIdInterface
     */
    private $searchPersonByDsId;

    /**
     * @var UrlHelper
     */
    private $urlHelper;

    /**
     * @var SearchTypeByIdInterface
     */
    private $searchTypeById;

    /**
     * @var Form
     */
    private $projectForm;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        PersistProjectInterface $persistProject,
        SearchPersonByDsIdInterface $searchPersonByDsId,
        SearchTypeByIdInterface $searchTypeById,
        UrlHelper $urlHelper,
        Form $projectForm
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->persistProject = $persistProject;
        $this->searchPersonByDsId = $searchPersonByDsId;
        $this->searchTypeById = $searchTypeById;
        $this->urlHelper = $urlHelper;
        $this->projectForm = $projectForm;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $projectForm = $this->projectForm->bindFromRequest($request);

        if ($projectForm->hasErrors()) {
            return new FormErrorJsonResponse($projectForm);
        }

        /* @var $projectData ProjectData */
        $projectData = $projectForm->getValue();
        $project = $projectData->toProject();
        $this->persistProject->__invoke($project);

        return new EmptyResponse(
            201,
            [
                'Location' => $this->urlHelper->generate('staff/project/detail', ['projectId' => $project->getId()])
            ]
        );
    }
}
