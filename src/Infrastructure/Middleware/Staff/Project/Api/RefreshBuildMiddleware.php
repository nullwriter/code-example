<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Infrastructure\ITrack\Build\RefreshITrackBuild;
use Pinpoint\Infrastructure\ITrack\Exception\ErrorResponseException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\TextResponse;

final class RefreshBuildMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchBuildByIdInterface
     */
    private $searchBuildById;

    /**
     * @var RefreshITrackBuild
     */
    private $refreshITrackBuild;

    public function __construct(SearchBuildByIdInterface $searchBuildById, RefreshITrackBuild $refreshITrackBuild)
    {
        $this->searchBuildById = $searchBuildById;
        $this->refreshITrackBuild = $refreshITrackBuild;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $build = $this->searchBuildById->__invoke(Id::fromString($request->getAttribute('buildId')));

        if (null === $build) {
            return new EmptyResponse(404);
        }

        try {
            $this->refreshITrackBuild->__invoke($build);
        } catch (ErrorResponseException $e) {
            return new TextResponse($e->getMessage(), 400);
        }

        return new EmptyResponse();
    }
}
