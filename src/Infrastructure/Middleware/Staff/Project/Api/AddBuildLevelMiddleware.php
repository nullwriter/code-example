<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Helios\IdentityMiddleware;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\PersistBuildInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Project\Exception\DuplicateBuildException;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Form\Data\Project\BuildData;
use Pinpoint\Infrastructure\Response\FormErrorJsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\TextResponse;

final class AddBuildLevelMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchProjectByIdInterface
     */
    private $searchProjectById;

    /**
     * @var PersistProjectInterface
     */
    private $persistProject;

    /**
     * @var PersistBuildInterface
     */
    private $persistBuild;

    /**
     * @var IsStaffPersonInterface
     */
    private $isStaffPerson;

    /**
     * @var Form
     */
    private $addBuildForm;

    public function __construct(
        PersistProjectInterface $persistProject,
        SearchProjectByIdInterface $searchProjectById,
        PersistBuildInterface $persistBuild,
        IsStaffPersonInterface $isStaffPerson,
        Form $addBuildForm
    ) {
        $this->searchProjectById = $searchProjectById;
        $this->persistProject = $persistProject;
        $this->persistBuild = $persistBuild;
        $this->isStaffPerson = $isStaffPerson;
        $this->addBuildForm = $addBuildForm;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        /* @var $loggedInPerson Person */
        $loggedInPerson = $request->getAttribute(IdentityMiddleware::IDENTITY_ATTRIBUTE)->getPerson();

        if (! $this->isStaffPerson->__invoke($loggedInPerson, IsStaffPersonInterface::EPM)
            && ! $this->isStaffPerson->__invoke($loggedInPerson, IsStaffPersonInterface::MANAGER)
        ) {
            return new TextResponse('Only EPMs and managers can add Builds', 403);
        }

        $addBuildForm = $this->addBuildForm->bindFromRequest($request);

        if ($addBuildForm->hasErrors()) {
            return new FormErrorJsonResponse($addBuildForm);
        }

        /* @var $buildData BuildData */
        $buildData = $addBuildForm->getValue();

        $projectId = $request->getAttribute('projectId');
        $project = $this->searchProjectById->__invoke(Id::fromString($projectId));

        try {
            $buildData->toBuild($project);
            $this->persistProject->__invoke($project);
        } catch (DuplicateBuildException $e) {
            // @todo this should be checked by by a form constraint, not the domain validation logic!
            return new FormErrorJsonResponse(
                $addBuildForm->withError(
                    new FormError('error.duplicate-build', 'Duplicate Build Exception.')
                )
            );
        }

        return new EmptyResponse();
    }
}
