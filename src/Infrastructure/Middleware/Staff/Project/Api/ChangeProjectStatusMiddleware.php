<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project\Api;

use DASPRiD\Helios\IdentityMiddleware;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Project\PersistProjectInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

final class ChangeProjectStatusMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var SearchProjectByIdInterface
     */
    private $searchProjectById;

    /**
     * @var PersistProjectInterface
     */
    private $persistProject;

    /**
     * @var IsStaffPersonInterface
     */
    private $isStaffPerson;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        PersistProjectInterface $persistProject,
        SearchProjectByIdInterface $searchProjectById,
        IsStaffPersonInterface $isStaffPerson
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->searchProjectById = $searchProjectById;
        $this->persistProject = $persistProject;
        $this->isStaffPerson = $isStaffPerson;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        /* @var $loggedInPerson Person */
        $loggedInPerson = $request->getAttribute(IdentityMiddleware::IDENTITY_ATTRIBUTE)->getPerson();

        if (! $this->isStaffPerson->__invoke($loggedInPerson, IsStaffPersonInterface::EPM)) {
            return new EmptyResponse(403);
        }

        $projectId = $request->getAttribute('projectId');
        $project = $this->searchProjectById->__invoke(Id::fromString($projectId));

        if (null === $project) {
            return new EmptyResponse(404);
        }

        $postData = $request->getParsedBody();

        if ($postData['status'] ?? false) {
            $project->activate();
        } else {
            $project->deactivate();
        }

        $this->persistProject->__invoke($project);
        return new EmptyResponse();
    }
}
