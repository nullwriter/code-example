<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Project\GetAllProjectTypesInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var GetAllProjectTypesInterface
     */
    private $getAllProjectTypes;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        GetAllProjectTypesInterface $getAllProjectTypes
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->getAllProjectTypes = $getAllProjectTypes;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        return $this->responseRenderer->render('staff-project::list', $request, [
            'projectTypes' => $this->getAllProjectTypes->__invoke()
        ]);
    }
}
