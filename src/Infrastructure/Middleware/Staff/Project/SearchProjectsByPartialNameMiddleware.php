<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Project\LookupProjectByPartialNameInterface;
use Pinpoint\Domain\Project\Project;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

final class SearchProjectsByPartialNameMiddleware implements MiddlewareInterface
{
    /**
     * @var LookupProjectByPartialNameInterface
     */
    private $lookupProjectsByPartialName;

    public function __construct(LookupProjectByPartialNameInterface $lookupProjectByPartialName)
    {
        $this->lookupProjectsByPartialName = $lookupProjectByPartialName;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        if (! array_key_exists('query', $queryParams) || ! is_string($queryParams['query'])) {
            return new JsonResponse(['persons' => []]);
        }

        /* @var $projects Project */
        $projects = $this->lookupProjectsByPartialName->__invoke($queryParams['query']);
        $result = [];

        foreach ($projects as $project) {
            $result[] = [
                'id' => (string) $project->getId(),
                'name' => $project->getName(),
            ];
        }

        return new JsonResponse([
            'persons' => $result,
        ]);
    }
}
