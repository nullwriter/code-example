<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Project;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Project\GetAllProjectTypesInterface;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

final class DetailMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var SearchProjectByIdInterface
     */
    private $searchProjectById;

    /**
     * @var GetAllProjectTypesInterface
     */
    private $getAllProjectTypes;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        SearchProjectByIdInterface $searchProjectById,
        GetAllProjectTypesInterface $getAllProjectTypes
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->searchProjectById = $searchProjectById;
        $this->getAllProjectTypes = $getAllProjectTypes;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $projectId = $request->getAttribute('projectId');
        $project = $this->searchProjectById->__invoke(Id::fromString($projectId));

        if (null === $project) {
            return new EmptyResponse(404);
        }

        return $this->responseRenderer->render('staff-project::detail', $request, [
            'project' => $project,
            'projectTypes' => $this->getAllProjectTypes->__invoke()
        ]);
    }
}
