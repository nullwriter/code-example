<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Admin;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Finder\Tests\Iterator\Iterator;

final class SetBillboardMessageMiddleware implements MiddlewareInterface
{
    /**
     * @var GetAllLabLocationsInterface
     */
    private $getAllLabLocations;

    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    private $messagePresets;

    public function __construct(
        GetAllLabLocationsInterface $getAllLabLocations,
        ResponseRendererInterface $responseRenderer,
        array $messagePresets
    ) {
        $this->getAllLabLocations = $getAllLabLocations;
        $this->responseRenderer = $responseRenderer;
        $this->messagePresets = $messagePresets;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $currentLabLocation = $request->getAttribute('labLocation');
        $labLocations = $this->getAllLabLocations->__invoke();

        return $this->responseRenderer->render('staff-admin::set-billboard-message', $request, [
            'labLocations' => $labLocations,
            'currentLabLocation' => $currentLabLocation,
            'messagePresets' => $this->messagePresets
        ]);
    }
}
