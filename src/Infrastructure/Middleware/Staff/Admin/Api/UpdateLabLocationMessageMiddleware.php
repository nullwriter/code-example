<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Admin\Api;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\LabLocation\PersistLabLocationInterface;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

final class UpdateLabLocationMessageMiddleware implements MiddlewareInterface
{
    /**
     * @var PersistLabLocationInterface
     */
    private $persistLabLocation;

    /**
     * @var SearchLabLocationsByIdInterface
     */
    private $searchLabLocationById;

    public function __construct(
        PersistLabLocationInterface $persistLabLocation,
        SearchLabLocationsByIdInterface $searchLabLocationsById
    ) {
        $this->persistLabLocation = $persistLabLocation;
        $this->searchLabLocationById = $searchLabLocationsById;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $postData = $request->getParsedBody();
        $newLabMessage = $postData['message'] ?? '';
        $labLocationId = $postData['labLocationId'] ?? '';

        $labLocation = $this->searchLabLocationById->__invoke(Id::fromString(trim($labLocationId)));

        if (null === $labLocation) {
            return new JsonResponse('Lab Location was not found', 500);
        }

        $labLocation->updateMessage($newLabMessage);
        $this->persistLabLocation->__invoke($labLocation);

        return new JsonResponse($newLabMessage);
    }
}
