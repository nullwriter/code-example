<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Admin;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class HomeMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    public function __construct(ResponseRendererInterface $responseRenderer)
    {
        $this->responseRenderer = $responseRenderer;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        return $this->responseRenderer->render('staff-admin::home', $request, []);
    }
}
