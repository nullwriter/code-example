<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Rework\Api;

use finfo;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\TextResponse;

final class AddAttachmentMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchReworkByIdInterface
     */
    private $searchReworkById;

    /**
     * @var PersistReworkInterface
     */
    private $persistRework;

    /**
     * @var string
     */
    private $tempPath;

    /**
     * @var string
     */
    private $attachmentPath;

    public function __construct(
        SearchReworkByIdInterface $searchReworkById,
        PersistReworkInterface $persistRework,
        string $tempPath,
        string $attachmentPath
    ) {
        $this->searchReworkById = $searchReworkById;
        $this->persistRework = $persistRework;
        $this->tempPath = $tempPath;
        $this->attachmentPath = $attachmentPath;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $rework = $this->searchReworkById->__invoke(Id::fromString($request->getAttribute('reworkId')));

        if (null === $rework) {
            return new EmptyResponse(404);
        }

        $uploadedFiles = $request->getUploadedFiles();

        if (! array_key_exists('file', $uploadedFiles)) {
            return new TextResponse('No file has been uploaded', 400);
        }

        /* @var $file UploadedFileInterface */
        $file = $uploadedFiles['files'];

        if (UPLOAD_ERR_OK !== $file->getError()) {
            return new TextResponse('An error occurred during upload', 400);
        }

        $tempName = tempnam($this->tempPath, 'rework-attachment');
        $file->moveTo($tempName);

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $attachment = $rework->addAttachment(
            $file->getClientFilename(),
            $file->getSize(),
            $finfo->file($tempName)
        );
        rename($tempName, sprintf('%s/%s', $this->attachmentPath, $attachment->getId()));
        $this->persistRework->__invoke($rework);

        return new EmptyResponse();
    }
}
