<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Rework\Api;

use DASPRiD\Formidable\FormInterface;
use DASPRiD\Helios\IdentityMiddleware;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Form\Data\Rework\ReworkData;
use Pinpoint\Infrastructure\Response\FormErrorJsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\TextResponse;

final class UpdateMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchReworkByIdInterface
     */
    private $searchReworkById;

    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var PersistReworkInterface
     */
    private $persistRework;

    /**
     * @var IsStaffPersonInterface
     */
    private $isStaffPerson;

    public function __construct(
        SearchReworkByIdInterface $searchReworkById,
        FormInterface $form,
        PersistReworkInterface $persistRework,
        IsStaffPersonInterface $isStaffPerson
    ) {
        $this->searchReworkById = $searchReworkById;
        $this->form = $form;
        $this->persistRework = $persistRework;
        $this->isStaffPerson = $isStaffPerson;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        /* @var $loggedInPerson Person */
        $loggedInPerson = $request->getAttribute(IdentityMiddleware::IDENTITY_ATTRIBUTE)->getPerson();

        if (! $this->isStaffPerson->__invoke($loggedInPerson, IsStaffPersonInterface::EPM)) {
            return new TextResponse('Only EPMs and Admin/Managers can edit a Rework.', 403);
        }

        $rework = $this->searchReworkById->__invoke(Id::fromString($request->getAttribute('reworkId')));

        if (null === $rework) {
            return new EmptyResponse(404);
        }

        $form = $this->form->bindFromRequest($request);

        if ($form->hasErrors()) {
            return new FormErrorJsonResponse($form);
        }

        /* @var $reworkData ReworkData */
        $reworkData = $form->getValue();
        $reworkData->updateRework($rework);
        $this->persistRework->__invoke($rework);

        return new EmptyResponse();
    }
}
