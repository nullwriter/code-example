<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Rework\Api;

use DASPRiD\Formidable\FormInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Rework\PersistReworkInterface;
use Pinpoint\Infrastructure\Form\Data\Rework\ReworkData;
use Pinpoint\Infrastructure\Response\FormErrorJsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Expressive\Helper\UrlHelper;

final class CreateMiddleware implements MiddlewareInterface
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var PersistReworkInterface
     */
    private $persistRework;

    /**
     * @var UrlHelper
     */
    private $urlHelper;

    public function __construct(FormInterface $form, PersistReworkInterface $persistRework, UrlHelper $urlHelper)
    {
        $this->form = $form;
        $this->persistRework = $persistRework;
        $this->urlHelper = $urlHelper;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $form = $this->form->bindFromRequest($request);

        if ($form->hasErrors()) {
            return new FormErrorJsonResponse($form);
        }

        /* @var $reworkData ReworkData */
        $reworkData = $form->getValue();
        $rework = $reworkData->toRework();
        $this->persistRework->__invoke($rework);

        return new EmptyResponse(
            201,
            [
                'Location' => $this->urlHelper->generate('staff/rework/detail', [
                    'reworkId' => (string) $rework->getId()
                ])
            ]
        );
    }
}
