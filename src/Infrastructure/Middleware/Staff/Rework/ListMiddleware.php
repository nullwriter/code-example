<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Rework;

use DASPRiD\Helios\IdentityMiddleware;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Project\GetAllProjectsInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var IsStaffPersonInterface
     */
    private $isStaffPerson;

    /**
     * @var GetAllProjectsInterface
     */
    private $getAllProjects;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        IsStaffPersonInterface $isStaffPerson,
        GetAllProjectsInterface $getAllProjects
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->isStaffPerson = $isStaffPerson;
        $this->getAllProjects = $getAllProjects;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        /* @var $loggedInPerson Person */
        $loggedInPerson = $request->getAttribute(IdentityMiddleware::IDENTITY_ATTRIBUTE)->getPerson();

        return $this->responseRenderer->render('staff-rework::list', $request, [
            'isStaffPerson' => $this->isStaffPerson->__invoke($loggedInPerson, IsStaffPersonInterface::EPM),
            'projects' => $this->getAllProjects->__invoke(),
        ]);
    }
}
