<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Staff\Rework;

use DASPRiD\Helios\IdentityMiddleware;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Build\GetAllActiveBuildsInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

final class DetailMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchReworkByIdInterface
     */
    private $searchReworkById;

    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var IsStaffPersonInterface
     */
    private $isStaffPerson;

    /**
     * @var GetAllActiveBuildsInterface
     */
    private $getAllActiveBuilds;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        SearchReworkByIdInterface $searchReworkById,
        IsStaffPersonInterface $isStaffPerson,
        GetAllActiveBuildsInterface $getAllActiveBuilds
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->searchReworkById = $searchReworkById;
        $this->isStaffPerson = $isStaffPerson;
        $this->getAllActiveBuilds = $getAllActiveBuilds;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $reworkId = $request->getAttribute('reworkId');
        $rework = $this->searchReworkById->__invoke(Id::fromString($reworkId));

        if (null === $rework) {
            return new EmptyResponse(404);
        }

        /* @var $loggedInPerson Person */
        $loggedInPerson = $request->getAttribute(IdentityMiddleware::IDENTITY_ATTRIBUTE)->getPerson();

        return $this->responseRenderer->render('staff-rework::detail', $request, [
            'rework' => $rework,
            'isStaffPerson' => $this->isStaffPerson->__invoke($loggedInPerson, IsStaffPersonInterface::EPM),
            'builds' => $this->getAllActiveBuilds->__invoke(),
        ]);
    }
}
