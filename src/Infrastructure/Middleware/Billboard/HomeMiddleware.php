<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Billboard;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class HomeMiddleware implements MiddlewareInterface
{
    /**
     * @var GetAllLabLocationsInterface
     */
    private $getAllLabLocations;

    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    public function __construct(
        GetAllLabLocationsInterface $getAllLabLocations,
        ResponseRendererInterface $responseRenderer
    ) {
        $this->getAllLabLocations = $getAllLabLocations;
        $this->responseRenderer = $responseRenderer;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        return $this->responseRenderer->render('billboard::select-lab-location', $request, [
            'labLocations' => $this->getAllLabLocations->__invoke()
        ]);
    }
}
