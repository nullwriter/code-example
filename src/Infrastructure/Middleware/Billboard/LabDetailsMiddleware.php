<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Billboard;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\LabLocation\GetLabLocationStatisticsInterface;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class LabDetailsMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    /**
     * @var SearchLabLocationsByIdInterface
     */
    private $searchLabLocation;

    /**
     * @var GetLabLocationStatisticsInterface
     */
    private $getLabStatistics;

    public function __construct(
        ResponseRendererInterface $responseRenderer,
        SearchLabLocationsByIdInterface $searchLabLocation,
        GetLabLocationStatisticsInterface $getLabStatistics
    ) {
        $this->responseRenderer = $responseRenderer;
        $this->searchLabLocation = $searchLabLocation;
        $this->getLabStatistics = $getLabStatistics;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $labLocationId = $request->getAttribute('labLocationId');

        $labLocation = $this->searchLabLocation->__invoke(Id::fromString($labLocationId));

        if (null === $labLocation) {
            return $delegate->process($request);
        }

        return $this->responseRenderer->render('billboard::lab-statistics', $request, [
            'labLocationStatistics' => $this->getLabStatistics->__invoke($labLocation)
        ]);
    }
}
