<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\Billboard;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;
use Pinpoint\Infrastructure\Response\ResponseRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class LabMessageMiddleware implements MiddlewareInterface
{
    /**
     * @var SearchLabLocationsByIdInterface
     */
    private $searchLabLocation;

    /**
     * @var ResponseRendererInterface
     */
    private $responseRenderer;

    public function __construct(
        SearchLabLocationsByIdInterface $searchLabLocation,
        ResponseRendererInterface $responseRenderer
    ) {
        $this->searchLabLocation = $searchLabLocation;
        $this->responseRenderer = $responseRenderer;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $labLocationId = $request->getAttribute('labLocationId');

        $labLocation = $this->searchLabLocation->__invoke(Id::fromString($labLocationId));

        if (null === $labLocation) {
            return $delegate->process($request);
        }

        return $this->responseRenderer->render('billboard::lab-message', $request, [
            'labLocation' => $labLocation
        ]);
    }
}
