<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Middleware\LabLocation;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Pinpoint\Domain\LabLocation\GetAllLabLocationsInterface;
use Zend\Diactoros\Response\JsonResponse;

final class JsonGetAllLabLocationsMiddleware implements MiddlewareInterface
{
    /**
     * @var GetAllLabLocationsInterface
     */
    private $getAllLablLocations;

    public function __construct(GetAllLabLocationsInterface $getAllLabLocations)
    {
        $this->getAllLablLocations = $getAllLabLocations;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        $labLocationsJson = [];
        $labLocations = $this->getAllLablLocations->__invoke();

        foreach ($labLocations as $location) {
            array_push($labLocationsJson, [
                'id' => $location->getId()->__toString(),
                'name' => (string) $location
            ]);
        }

        return new JsonResponse([
            'labLocations' => $labLocationsJson
        ]);
    }
}
