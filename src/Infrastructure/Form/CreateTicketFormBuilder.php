<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\FormInterface;
use DASPRiD\Formidable\Mapping\MappingInterface;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Form\Mapping\Constraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\Unit\UnitNumberUniqueInITrackBuildConstraint;

final class CreateTicketFormBuilder
{
    /**
     * @var MappingInterface
     */
    private $baseMapping;

    /**
     * @var Constraint\CreateTicket\HrpNumberNotInUseConstraint
     */
    private $hrpNumberNotInUseConstraint;

    /**
     * @var UnitNumberUniqueInITrackBuildConstraint
     */
    private $unitNumberUniqueInITrackBuildConstraint;

    /**
     * @var Constraint\Unit\UnitNotInITrackConstraint
     */
    private $unitNotInITrackConstraint;

    public function __construct(
        MappingInterface $baseMapping,
        Constraint\CreateTicket\HrpNumberNotInUseConstraint $hrpNumberNotInUseConstraint,
        UnitNumberUniqueInITrackBuildConstraint $unitNumberUniqueInITrackBuildConstraint,
        Constraint\Unit\UnitNotInITrackConstraint $unitNotInITrackConstraint
    ) {
        $this->baseMapping = $baseMapping;
        $this->hrpNumberNotInUseConstraint = $hrpNumberNotInUseConstraint;
        $this->unitNumberUniqueInITrackBuildConstraint = $unitNumberUniqueInITrackBuildConstraint;
        $this->unitNotInITrackConstraint = $unitNotInITrackConstraint;
    }

    public function __invoke() : FormInterface
    {
        $mapping = $this->baseMapping
            ->verifying($this->hrpNumberNotInUseConstraint)
            ->verifying($this->unitNumberUniqueInITrackBuildConstraint)
            ->verifying(new Constraint\Unit\UnitIdentifierValidityConstraint())
            ->verifying($this->unitNotInITrackConstraint)
            ->verifying(new Constraint\CreateTicket\AtLeastOneWorkRequestConstraint());

        return new Form($mapping);
    }
}
