<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Person\IsStaffPersonInterface;
use Pinpoint\Domain\Person\Person;

final class IsStaffPersonConstraint implements ConstraintInterface
{
    /**
     * @var IsStaffPersonInterface
     */
    private $isStaffPerson;

    public function __construct(IsStaffPersonInterface $isStaffPerson)
    {
        $this->isStaffPerson = $isStaffPerson;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof Person);

        if ($this->isStaffPerson->__invoke($value)) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.staff-person'));
    }
}
