<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\Action;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Form\Data\UpdateTicket\ChangeStatusData;

final class WorkRequestsCompleteConstraint implements ConstraintInterface
{
    /**
     * @var array
     */
    private $ticket;

    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof ChangeStatusData);

        if (Action::HOLD() === $value->getAction()) {
            return new ValidationResult();
        }

        foreach ($this->ticket->getWorkRequests() as $workRequest) {
            if (! $workRequest->isCompleted()) {
                return new ValidationResult(new ValidationError('error.work-request-incomplete'));
            }
        }

        return new ValidationResult();
    }
}
