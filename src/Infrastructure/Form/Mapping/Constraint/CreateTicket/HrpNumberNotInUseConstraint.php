<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\CreateTicket;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\SearchTicketByIdInterface;
use Pinpoint\Domain\Unit\SearchUnitByIdentifierInterface;
use Pinpoint\Infrastructure\Form\Data\CreateTicket\TicketData;

final class HrpNumberNotInUseConstraint implements ConstraintInterface
{
    /**
     * @var SearchUnitByIdentifierInterface
     */
    private $searchUnitByIdentifier;

    /**
     * @var SearchTicketByIdInterface
     */
    private $searchTicketById;

    /**
     * @var string|null
     */
    private $ignoredHrpNumber;

    public function __construct(
        SearchUnitByIdentifierInterface $searchUnitByIdentifier,
        SearchTicketByIdInterface $searchTicketById
    ) {
        $this->searchUnitByIdentifier = $searchUnitByIdentifier;
        $this->searchTicketById = $searchTicketById;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof TicketData);

        if ($value->isOverrideActive()) {
            return new ValidationResult();
        }

        $unitIdentifiers = $value->getUnitIdentifiers();

        if (null === $unitIdentifiers->getHrpNumber()) {
            return new ValidationResult();
        }

        if ((string) $this->ignoredHrpNumber === (string) $unitIdentifiers->getHrpNumber()) {
            return new ValidationResult();
        }

        // It is safe to search for existing HRP numbers this way, as they are prefixed.
        $unit = $this->searchUnitByIdentifier->__invoke($unitIdentifiers->getHrpNumber());

        if (null === $unit) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.hrp-number-exists'));
    }

    public function withIgnoredHrpNumber(string $hrpNumber) : self
    {
        $constraint = clone $this;
        $constraint->ignoredHrpNumber = $hrpNumber;
        return $constraint;
    }
}
