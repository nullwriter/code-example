<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\CreateTicket;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;

final class OverridePasswordConstraint implements ConstraintInterface
{
    /**
     * @var string[]
     */
    private $overridePasswordHashes;

    public function __construct(string ...$overridePasswordHashes)
    {
        $this->overridePasswordHashes = $overridePasswordHashes;
    }

    public function __invoke($value) : ValidationResult
    {
        assert(is_string($value));

        foreach ($this->overridePasswordHashes as $overridePasswordHash) {
            if (password_verify($value, $overridePasswordHash)) {
                return new ValidationResult();
            }
        }

        return new ValidationResult(new ValidationError('error.override-password'));
    }
}
