<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\CreateTicket;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\WorkRequest\WorkRequest;
use Pinpoint\Infrastructure\Form\Data\CreateTicket\WorkRequestData;

final class UniqueSpecialWorkRequestsConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        /* @var $value WorkRequestData[] */
        assert(is_array($value));
        $hasDistributeUnit = false;
        $hasBuildUnit = false;

        foreach ($value as $workRequestData) {
            if (WorkRequest::DISTRIBUTE_UNIT === $workRequestData->getType()) {
                if ($hasDistributeUnit) {
                    return new ValidationResult(new ValidationError('error.multiple-distribute-unit-work-requests'));
                }

                $hasDistributeUnit = true;
            }

            if (WorkRequest::BUILD_UNIT === $workRequestData->getType()) {
                if ($hasBuildUnit) {
                    return new ValidationResult(new ValidationError('error.multiple-build-unit-work-requests'));
                }

                $hasBuildUnit = true;
            }
        }

        return new ValidationResult();
    }
}
