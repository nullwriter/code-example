<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\CreateTicket;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\CreateTicket\TicketData;

final class AtLeastOneWorkRequestConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof TicketData);

        if (! $value->hasWorkRequests()) {
            return new ValidationResult(new ValidationError('error.missing-work-requests'));
        }

        return new ValidationResult();
    }
}
