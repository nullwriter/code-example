<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\UpdateTicket\WorkRequestData;

final class WorkRequestCompletedByConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof WorkRequestData);

        if ($value->isComplete() && null === $value->getCompletedBy()) {
            return new ValidationResult(
                new ValidationError('error.work-request-completed-by-empty', [], 'completedBy')
            );
        }

        if (! $value->isComplete() && null !== $value->getCompletedBy()) {
            return new ValidationResult(
                new ValidationError('error.work-request-completed-by-not-empty', [], 'completedBy')
            );
        }

        return new ValidationResult();
    }
}
