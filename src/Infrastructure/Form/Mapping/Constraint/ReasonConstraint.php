<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\Action;
use Pinpoint\Infrastructure\Form\Data\UpdateTicket\ChangeStatusData;

final class ReasonConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof ChangeStatusData);

        if ('' !== $value->getReason() || ! in_array(
            $value->getAction(),
            [Action::CANCEL(), Action::CLOSE(), Action::HOLD(), Action::SCRAP(), Action::WORK_COMPLETE()],
            true
        )) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.required', [], 'reason'));
    }
}
