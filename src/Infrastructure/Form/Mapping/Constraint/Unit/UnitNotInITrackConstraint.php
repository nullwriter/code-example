<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Unit;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\CreateTicket\TicketData;
use Pinpoint\Infrastructure\ITrack\Unit\SearchITrackUnitByIdentifier;

final class UnitNotInITrackConstraint implements ConstraintInterface
{
    /**
     * @var SearchITrackUnitByIdentifier
     */
    private $searchITrackUnitByIdentifier;

    public function __construct(SearchITrackUnitByIdentifier $searchITrackUnitByIdentifier)
    {
        $this->searchITrackUnitByIdentifier = $searchITrackUnitByIdentifier;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof TicketData);

        if ($value->isOverrideActive()) {
            return new ValidationResult();
        }

        $unitIdentifiers = $value->getUnitIdentifiers();

        if (! $unitIdentifiers->hasITrackedIdentifier(true)) {
            return new ValidationResult();
        }

        $iTrackUnit = $this->searchITrackUnitByIdentifier->__invoke(
            (string) $unitIdentifiers->getITrackedIdentifier(true)
        );

        if (null === $iTrackUnit) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.unit-i-tracked'));
    }
}
