<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Unit;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\Unit\UnitDataInterface;
use Pinpoint\Infrastructure\ITrack\Unit\HasUnitNumberInBuild;

final class UnitNumberUniqueInITrackBuildConstraint implements ConstraintInterface
{
    /**
     * @var HasUnitNumberInBuild
     */
    private $hasUnitNumberInBuild;

    /**
     * @var string|null
     */
    private $ignoredUnitNumber;

    public function __construct(HasUnitNumberInBuild $hasUnitNumberInBuild)
    {
        $this->hasUnitNumberInBuild = $hasUnitNumberInBuild;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof UnitDataInterface);

        $build = $value->getBuild();

        if (! $build->isITracked()) {
            return new ValidationResult();
        }

        $unitAttributes = $value->getUnitAttributes();

        if (null === $unitAttributes->getUnitNumber()) {
            return new ValidationResult();
        }

        if ($this->ignoredUnitNumber === $unitAttributes->getUnitNumber()) {
            return new ValidationResult();
        }

        if ($this->hasUnitNumberInBuild->__invoke($unitAttributes->getUnitNumber(), $build)) {
            return new ValidationResult(new ValidationError('error.unit-number-exists-in-build'));
        }

        return new ValidationResult();
    }

    public function withIgnoredUnitNumber(string $unitNumber) : self
    {
        $constraint = clone $this;
        $constraint->ignoredUnitNumber = $unitNumber;
        return $constraint;
    }
}
