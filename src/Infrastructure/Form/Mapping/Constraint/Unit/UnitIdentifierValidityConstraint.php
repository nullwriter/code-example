<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Unit;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\Unit\UnitDataInterface;
use Pinpoint\Infrastructure\Form\Data\Unit\UnitIdentifiersData;

final class UnitIdentifierValidityConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof UnitDataInterface);

        $isITracked = $value->isITracked();
        $unitIdentifiers = $value->getUnitIdentifiers();

        if (! $this->hasRequiredIdentifier($isITracked, $unitIdentifiers)) {
            return new ValidationResult(new ValidationError('error.required-unit-identifier'));
        }

        if ($this->hasSuperfluousIdentifier($isITracked, $unitIdentifiers)) {
            return new ValidationResult(new ValidationError('error.superfluous-unit-identifier'));
        }

        return new ValidationResult();
    }

    private function hasRequiredIdentifier(bool $isITracked, UnitIdentifiersData $unitIdentifiers) : bool
    {
        if ($isITracked) {
            return $unitIdentifiers->hasITrackedIdentifier(true);
        }

        return $unitIdentifiers->hasNonITrackedIdentifier(true);
    }

    private function hasSuperfluousIdentifier(bool $isITracked, UnitIdentifiersData $unitIdentifiers) : bool
    {
        if ($isITracked) {
            return $unitIdentifiers->hasNonITrackedIdentifier(false);
        }

        return $unitIdentifiers->hasITrackedIdentifier(false);
    }
}
