<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Unit;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Infrastructure\Form\Data\Unit\UnitData;

final class DisallowProjectChangeConstraint implements ConstraintInterface
{
    /**
     * @var Project
     */
    private $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof UnitData);

        if ($this->project === $value->getBuild()->getProject()) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.unit-project-change'));
    }
}
