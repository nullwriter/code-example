<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\Action;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\Form\Data\UpdateTicket\ChangeStatusData;

final class CompleteForPickupConstraint implements ConstraintInterface
{
    /**
     * @var array
     */
    private $ticket;

    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof ChangeStatusData);

        if (Action::CLOSE() === $value->getAction() && ! $this->ticket->hasBeenPickedUp()) {
            return new ValidationResult(new ValidationError('error.unit-not-picked-up'));
        }

        return new ValidationResult();
    }
}
