<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Relocate;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\Ticket;

final class TicketInTransitConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof Ticket);

        if (! $value->getUnit()->isInTransit()) {
            return new ValidationResult(new ValidationError('error.ticket-not-in-transit'));
        }

        return new ValidationResult();
    }
}
