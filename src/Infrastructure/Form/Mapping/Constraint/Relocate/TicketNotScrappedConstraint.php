<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Relocate;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Infrastructure\ITrack\Unit\SearchITrackUnitById;

final class TicketNotScrappedConstraint implements ConstraintInterface
{
    /**
     * @var SearchITrackUnitById
     */
    private $searchITrackUnitById;

    public function __construct(SearchITrackUnitById $searchITrackUnitById)
    {
        $this->searchITrackUnitById = $searchITrackUnitById;
    }

    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof Ticket);

        $unit = $value->getUnitForWriting();

        if (! $unit->isITracked()) {
            return new ValidationResult();
        }

        $iTrackUnit = $this->searchITrackUnitById->__invoke($unit->getITrackId());

        if ($iTrackUnit->isScrapped()) {
            return new ValidationResult(new ValidationError('error.unit-scrapped'));
        }

        return new ValidationResult();
    }
}
