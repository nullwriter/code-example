<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Relocate;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\Relocate\RelocateData;

final class CurrentLocationDoesNotEqualDestinationConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof RelocateData);

        if ($value->getCurrentLocation() === $value->getDestination()) {
            return new ValidationResult(new ValidationError('error.current-location-equals-destination'));
        }

        return new ValidationResult();
    }
}
