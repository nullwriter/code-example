<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Relocate;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\Status;
use Pinpoint\Domain\Ticket\Ticket;

final class TicketRelocatableConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof Ticket);

        if (in_array($value->getCurrentState()->getStatus(), [Status::CLOSED(), Status::READY_FOR_PICKUP()], true)) {
            return new ValidationResult(new ValidationError('error.ticket-not-relocatable'));
        }

        return new ValidationResult();
    }
}
