<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Relocate;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\Relocate\RelocateData;

final class TicketsLocationEqualsCurrentLocationConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof RelocateData);

        $currentLocation = $value->getCurrentLocation();

        foreach ($value->getTickets() as $key => $ticket) {
            if ($ticket->getUnit()->getLabLocation() === $currentLocation) {
                return new ValidationResult(new ValidationError(
                    'error.ticket-location-not-matching-current-location',
                    [],
                    sprintf('tickets[%d]', $key)
                ));
            }
        }

        return new ValidationResult();
    }
}
