<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Relocate;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\Relocate\RelocateData;

final class TicketsLocationEqualsDestinationConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof RelocateData);

        $destination = $value->getDestination();

        foreach ($value->getTickets() as $key => $ticket) {
            if ($ticket->getUnit()->getLabLocation() === $destination) {
                return new ValidationResult(new ValidationError(
                    'error.ticket-location-not-matching-destination',
                    [],
                    sprintf('tickets[%d]', $key)
                ));
            }
        }

        return new ValidationResult();
    }
}
