<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Rework;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Infrastructure\Form\Data\Rework\ReworkData;

final class BuildsInReworkProjectConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert($value instanceof ReworkData);
        $reworkProject = $value->getProject();

        foreach ($value->getBuilds() as $build) {
            if ($build->getProject() !== $reworkProject) {
                return new ValidationResult(new ValidationError('error.rework-project-mismatch'));
            }
        }

        return new ValidationResult();
    }
}
