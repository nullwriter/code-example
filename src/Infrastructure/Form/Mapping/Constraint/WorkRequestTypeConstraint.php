<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Ticket\WorkRequest\WorkRequest;

final class WorkRequestTypeConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert(is_string($value));

        if (array_key_exists($value, WorkRequest::getTypeMap(true))) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.work-request-type'));
    }
}
