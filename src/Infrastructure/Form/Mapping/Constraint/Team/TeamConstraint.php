<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Team;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Project\Team;

final class TeamConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert(is_string($value));

        if (Team::SYSTEM === $value || Team::PSE === $value) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.invalid-team'));
    }
}
