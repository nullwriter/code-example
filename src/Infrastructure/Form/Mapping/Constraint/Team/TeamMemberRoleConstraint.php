<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Team;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Project\TeamMember;

final class TeamMemberRoleConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert(is_string($value));

        if (in_array($value, TeamMember::VALID_ROLES)) {
            return new ValidationResult();
        }

        return new ValidationResult(new ValidationError('error.invalid-role'));
    }
}
