<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Constraint\Team;

use DASPRiD\Formidable\Mapping\Constraint\ConstraintInterface;
use DASPRiD\Formidable\Mapping\Constraint\ValidationError;
use DASPRiD\Formidable\Mapping\Constraint\ValidationResult;
use Pinpoint\Domain\Project\Team;
use Pinpoint\Infrastructure\Form\Data\Project\TeamMemberData;

final class MaxMembersPerRoleConstraint implements ConstraintInterface
{
    public function __invoke($value) : ValidationResult
    {
        assert(is_array($value));

        /* @var $value TeamMemberData[] */

        foreach ($value as $teamMember) {
            $position = $teamMember->getPosition();

            if (Team::SYSTEM === $teamMember->getTeam()) {
                if (1 !== $position && 0 !== $position) {
                    return new ValidationResult(new ValidationError('error.system-team-position'));
                }
            } else {
                if (0 !== $position) {
                    return new ValidationResult(new ValidationError('error.pse-team-position'));
                }
            }
        }

        return new ValidationResult();
    }
}
