<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter\Rework;

use DASPRiD\Enum\Exception\IllegalArgumentException;
use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Rework\InspectionType;

final class InspectionTypeFormatter implements FormatterInterface
{
    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        $value = $data->getValue($key);

        try {
            return BindResult::fromValue(InspectionType::valueOf($value));
        } catch (IllegalArgumentException $e) {
            return BindResult::fromFormErrors(new FormError($key, 'error.inspection-type'));
        }
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof InspectionType);
        return Data::fromFlatArray([$key => $value->name()]);
    }
}
