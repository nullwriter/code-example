<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Person\DsId;
use Pinpoint\Domain\Person\Exception\InvalidDsIdException;
use Pinpoint\Domain\Person\LookupAndStorePersonByDsIdInterface;
use Pinpoint\Domain\Person\Person;

final class PersonFormatter implements FormatterInterface
{
    /**
     * @var LookupAndStorePersonByDsIdInterface
     */
    private $lookupAndStorePersonByDsId;

    public function __construct(LookupAndStorePersonByDsIdInterface $lookupAndStorePersonByDsId)
    {
        $this->lookupAndStorePersonByDsId = $lookupAndStorePersonByDsId;
    }

    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        try {
            $personDsId = new DsId($data->getValue($key));
        } catch (InvalidDsIdException $e) {
            return BindResult::fromFormErrors(new FormError($key, 'error.person'));
        }

        $person = $this->lookupAndStorePersonByDsId->__invoke($personDsId);

        if (null === $person) {
            return BindResult::fromFormErrors(new FormError($key, 'error.invalid-dsid'));
        }

        return BindResult::fromValue($person);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof Person);
        return Data::fromFlatArray([$key => (string) $value->getDsId()]);
    }
}
