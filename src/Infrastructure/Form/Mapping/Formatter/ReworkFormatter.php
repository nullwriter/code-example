<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Rework\Rework;
use Pinpoint\Domain\Rework\SearchReworkByIdInterface;

final class ReworkFormatter implements FormatterInterface
{
    /**
     * @var SearchReworkByIdInterface
     */
    private $searchReworkById;

    public function __construct(SearchReworkByIdInterface $searchReworkById)
    {
        $this->searchReworkById = $searchReworkById;
    }

    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        $rework = $this->searchReworkById->__invoke(Id::fromString($data->getValue($key)));

        if (null === $rework) {
            return BindResult::fromFormErrors(new FormError($key, 'error.rework'));
        }

        return BindResult::fromValue($rework);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof Rework);
        return Data::fromFlatArray([$key => (string) $value->getId()]);
    }
}
