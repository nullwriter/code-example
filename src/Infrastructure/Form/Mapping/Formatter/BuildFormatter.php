<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Build\SearchBuildByIdInterface;
use Pinpoint\Domain\Exception\InvalidUuidException;
use Pinpoint\Domain\Id;

final class BuildFormatter implements FormatterInterface
{
    /**
     * @var SearchBuildByIdInterface
     */
    private $searchBuildById;

    public function __construct($searchBuildById)
    {
        $this->searchBuildById = $searchBuildById;
    }

    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        try {
            $buildId = Id::fromString($data->getValue($key));
        } catch (InvalidUuidException $e) {
            return BindResult::fromFormErrors(new FormError($key, 'error.build'));
        }

        $build = $this->searchBuildById->__invoke($buildId);

        if (null === $build) {
            return BindResult::fromFormErrors(new FormError($key, 'error.build'));
        }

        return BindResult::fromValue($build);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof Build);
        return Data::fromFlatArray([$key => (string) $value->getId()]);
    }
}
