<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Project\SearchProjectByIdInterface;

final class ProjectFormatter implements FormatterInterface
{
    /**
     * @var SearchProjectByIdInterface
     */
    private $searchProjectById;

    public function __construct(SearchProjectByIdInterface $searchProjectById)
    {
        $this->searchProjectById = $searchProjectById;
    }

    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        $build = $this->searchProjectById->__invoke(Id::fromString($data->getValue($key)));

        if (null === $build) {
            return BindResult::fromFormErrors(new FormError($key, 'error.project'));
        }

        return BindResult::fromValue($build);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof Project);
        return Data::fromFlatArray([$key => (string) $value->getId()]);
    }
}
