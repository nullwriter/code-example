<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Ticket\Exception\InvalidPriorityException;
use Pinpoint\Domain\Ticket\Priority;

final class PriorityFormatter implements FormatterInterface
{
    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        $value = $data->getValue($key);

        if (! preg_match('(^\d*$)', $value)  || ($value < Priority::MIN  || $value > Priority::MAX)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.priority'));
        }

        try {
            $priority = new Priority((int) $value);
        } catch (InvalidPriorityException $e) {
            return BindResult::fromFormErrors(new FormError($key, 'error.priority'));
        }

        return BindResult::fromValue($priority);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof Priority);
        return Data::fromFlatArray([$key => (string) $value->toInt()]);
    }
}
