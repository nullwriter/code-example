<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter\Project;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Project\SearchTypeByIdInterface;
use Pinpoint\Domain\Project\Type;

final class TypeFormatter implements FormatterInterface
{

    /**
     * @var SearchTypeByIdInterface
     */
    private $searchTypeById;

    public function __construct(SearchTypeByIdInterface $searchTypeById)
    {
        $this->searchTypeById = $searchTypeById;
    }

    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        $type = $this->searchTypeById->__invoke(Id::fromString($data->getValue($key)));

        if (null === $type) {
            return BindResult::fromFormErrors(new FormError($key, 'error.project-type'));
        }

        return BindResult::fromValue($type);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof Type);
        return Data::fromFlatArray([$key => (string) $value->getId()]);
    }
}
