<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Mapping\Formatter;

use DASPRiD\Formidable\Data;
use DASPRiD\Formidable\FormError\FormError;
use DASPRiD\Formidable\Mapping\BindResult;
use DASPRiD\Formidable\Mapping\Formatter\FormatterInterface;
use Pinpoint\Domain\Exception\InvalidUuidException;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\LabLocation\LabLocation;
use Pinpoint\Domain\LabLocation\SearchLabLocationsByIdInterface;

final class LabLocationFormatter implements FormatterInterface
{
    /**
     * @var SearchLabLocationsByIdInterface
     */
    private $searchLabLocationById;

    public function __construct(SearchLabLocationsByIdInterface $searchLabLocationById)
    {
        $this->searchLabLocationById = $searchLabLocationById;
    }

    public function bind(string $key, Data $data) : BindResult
    {
        if (! $data->hasKey($key)) {
            return BindResult::fromFormErrors(new FormError($key, 'error.required'));
        }

        try {
            $labLocationId = Id::fromString($data->getValue($key));
        } catch (InvalidUuidException $e) {
            return BindResult::fromFormErrors(new FormError($key, 'error.lab-location'));
        }

        $labLocation = $this->searchLabLocationById->__invoke($labLocationId);

        if (null === $labLocation) {
            return BindResult::fromFormErrors(new FormError($key, 'error.lab-location'));
        }

        return BindResult::fromValue($labLocation);
    }

    public function unbind(string $key, $value) : Data
    {
        assert($value instanceof LabLocation);
        return Data::fromFlatArray([$key => (string) $value->getId()]);
    }
}
