<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Data\Project;

use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Project\Team;
use Pinpoint\Domain\Project\Type;
use Pinpoint\Domain\Ticket\Priority;

final class ProjectData
{
    /**
     * @var Priority
     */
    private $priority;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var Person
     */
    private $defaultEssTech;

    /**
     * @var string
     */
    private $name;

    /**
     * @var TeamMemberData[]
     */
    private $teamMembers;

    public function __construct(
        string $name,
        Priority $priorityAdd,
        Type $projectAddType,
        ?Person $defaultEssTech,
        array $teamMembers
    ) {
        $this->name = $name;
        $this->priority = $priorityAdd;
        $this->type = $projectAddType;
        $this->defaultEssTech = $defaultEssTech;
        $this->teamMembers = $teamMembers;
    }

    public function toProject() : Project
    {
        $project = new Project(
            $this->name,
            $this->priority,
            $this->defaultEssTech,
            $this->type
        );

        $this->updateTeamMembers($project);
        return $project;
    }

    public function updateProject(Project $project) : Project
    {
        if ($project->isITracked()) {
            $project->updateITrackedProject(
                $this->priority,
                $this->defaultEssTech,
                $this->type
            );
        } else {
            $project->updateNonITrackedProject(
                $this->name,
                $this->priority,
                $this->defaultEssTech,
                $this->type
            );
        }

        $this->updateTeamMembers($project);
        return $project;
    }

    private function updateTeamMembers(Project $project) : void
    {
        $teams = [
            Team::SYSTEM => [],
            Team::PSE => [],
        ];

        foreach ($this->teamMembers as $teamMember) {
            $teamName = $teamMember->getTeam();
            $role = $teamMember->getRole();

            if (! array_key_exists($role, $teams[$teamName])) {
                $teams[$teamName][$role] = [];
            }

            $teams[$teamName][$role][$teamMember->getPosition()] = $teamMember->getPerson();
        }

        foreach ($teams as $teamName => $roles) {
            if (Team::SYSTEM === $teamName) {
                $team = $project->getSystemTeam();
            } else {
                $team = $project->getPseTeam();
            }

            foreach ($roles as $role => $members) {
                ksort($members);
                $team->setMembersForRole($role, ...array_values(array_filter($members)));
            }
        }
    }
}
