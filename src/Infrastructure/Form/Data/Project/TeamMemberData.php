<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Data\Project;

use Pinpoint\Domain\Person\Person;

final class TeamMemberData
{
    /**
     * @var Person|null
     */
    private $person;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     */
    private $team;

    /**
     * @var int
     */
    private $position;

    public function __construct(?Person $person, string $role, string $team, int $position = 0)
    {
        $this->person = $person;
        $this->role = $role;
        $this->team = $team;
        $this->position = $position;
    }

    public function getPerson() : ?Person
    {
        return $this->person;
    }

    public function getRole() : string
    {
        return $this->role;
    }

    public function getTeam() : string
    {
        return $this->team;
    }

    public function getPosition() : int
    {
        return $this->position;
    }
}
