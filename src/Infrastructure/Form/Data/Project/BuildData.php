<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Data\Project;

use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Rework\Rework;
use Pinpoint\Infrastructure\Form\Data\Exception\MissingNameException;

final class BuildData
{
    /**
     * @var null|string
     */
    private $name;

    /**
     * @var Rework[]
     */
    private $reworks;

    public function __construct(?string $name, array $reworks)
    {
        $this->name = $name;
        $this->reworks = $reworks;
    }

    public function toBuild(Project $project) : Build
    {
        if (null === $this->name) {
            throw MissingNameException::fromMissingName();
        }

        $build = $project->addBuild($this->name);
        $build->updateReworks(...$this->reworks);

        return $build;
    }

    public function updateBuild(Build $build) : void
    {
        $build->updateReworks(...$this->reworks);
    }
}
