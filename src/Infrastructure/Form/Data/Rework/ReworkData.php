<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form\Data\Rework;

use DateTimeImmutable;
use Pinpoint\Domain\Build\Build;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Rework\Instructions;
use Pinpoint\Domain\Rework\Rework;

final class ReworkData
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var bool
     */
    private $componentRework;

    /**
     * @var Build[]
     */
    private $builds;

    /**
     * @var Person
     */
    private $submittedBy;

    /**
     * @var Person|null
     */
    private $technicalContact;

    /**
     * @var Person|null
     */
    private $approvedBy;

    /**
     * @var DateTimeImmutable|null
     */
    private $approvedAt;

    /**
     * @var Instructions
     */
    private $instructions;

    public function __construct(
        string $name,
        bool $active,
        Project $project,
        bool $componentRework,
        array $builds,
        Person $submittedBy,
        ?Person $technicalContact,
        ?Person $approvedBy,
        ?DateTimeImmutable $approvedAt,
        Instructions $instructions
    ) {
        $this->name = $name;
        $this->active = $active;
        $this->project = $project;
        $this->componentRework = $componentRework;
        $this->builds = $builds;
        $this->submittedBy = $submittedBy;
        $this->technicalContact = $technicalContact;
        $this->approvedBy = $approvedBy;
        $this->approvedAt = $approvedAt;
        $this->instructions = $instructions;
    }

    public function getProject() : Project
    {
        return $this->project;
    }

    /**
     * @return Build[]
     */
    public function getBuilds() : array
    {
        return $this->builds;
    }

    public function toRework() : Rework
    {
        return new Rework(
            $this->name,
            $this->active,
            $this->project,
            $this->componentRework,
            $this->submittedBy,
            $this->technicalContact,
            $this->approvedBy,
            $this->approvedAt,
            $this->instructions,
            ...$this->builds
        );
    }

    public function updateRework(Rework $rework) : void
    {
        $rework->update(
            $this->name,
            $this->active,
            $this->project,
            $this->componentRework,
            $this->submittedBy,
            $this->technicalContact,
            $this->approvedBy,
            $this->approvedAt,
            $this->instructions,
            ...$this->builds
        );
    }
}
