<?php
declare(strict_types = 1);

namespace Pinpoint\Infrastructure\Form;

use DASPRiD\Formidable\Form;
use DASPRiD\Formidable\Mapping\MappingInterface;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Factory\Infrastructure\Form\ChangeStatusObjectMappingFactory;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\CompleteForPickupConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\ReasonConstraint;
use Pinpoint\Infrastructure\Form\Mapping\Constraint\WorkRequestsCompleteConstraint;

final class ChangeStatusFormBuilder
{
    /**
     * @var MappingInterface
     */
    private $baseMapping;


    public function __construct(MappingInterface $baseMapping)
    {
        $this->baseMapping = $baseMapping;
    }

    public function __invoke(Ticket $ticket)
    {
        return new Form(
            $this->baseMapping
                ->verifying(new ReasonConstraint())
                ->verifying(new WorkRequestsCompleteConstraint($ticket))
                ->verifying(new CompleteForPickupConstraint($ticket))
        );
    }
}
