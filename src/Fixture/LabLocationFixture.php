<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\LabLocation\LabLocation;
use Pinpoint\Domain\Ticket\Age\Age;

final class LabLocationFixture extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $applePark = new LabLocation(
            'Apple Park',
            false,
            'Unix is user friendly...<br/>It\'s just very particular<br/>about who its friends are.'
        );
        $grandCentralStation = new LabLocation(
            'Grand Central Station',
            true,
            'if(sad() === true) {<br/> sad->stop();<br/>beAwesome();<br/>}'
        );

        $il06 = new LabLocation(
            'IL06',
            false,
            'Message entry for IL06'
        );

        $vp02 = new LabLocation(
            'VP02',
            false,
            'Message entry for VP02'
        );

        $applePark->addToAverageTicketTurnAroundTime(new Age(1252600, 9));
        $grandCentralStation->addToAverageTicketTurnAroundTime(new Age(1237200, 9));
        $il06->addToAverageTicketTurnAroundTime(new Age(1448800, 9));
        $vp02->addToAverageTicketTurnAroundTime(new Age(1821070, 9));

        $manager->persist($applePark);
        $manager->persist($grandCentralStation);
        $manager->persist($il06);
        $manager->persist($vp02);
        $manager->flush();

        $this->addReference('apple-park', $applePark);
        $this->addReference('grand-central-station', $grandCentralStation);
        $this->addReference('il06', $il06);
        $this->addReference('vp02', $vp02);
    }
}
