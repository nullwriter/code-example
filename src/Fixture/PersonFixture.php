<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Person\Department;
use Pinpoint\Domain\Person\DsId;
use Pinpoint\Domain\Person\EmailAddress;
use Pinpoint\Domain\Person\Person;
use Pinpoint\Domain\Person\PhoneNumber;

final class PersonFixture extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $patrick = new Person(
            new DsId('2'),
            'Patrick Lam',
            new EmailAddress('patrick@apple.com'),
            null,
            null,
            new Department('Engineering Support Services', '0056/7695'),
            null
        );
        $peggy = new Person(
            new DsId('1'),
            'Peggy Austin',
            new EmailAddress('peggy@apple.com'),
            new PhoneNumber('1-408-9741197'),
            new PhoneNumber('1-408-406-4227'),
            new Department('Engineering Support Services', '0056/7695'),
            $patrick
        );
        $pedro = new Person(
            new DsId('3'),
            'Pedro Rios',
            new EmailAddress('rios.pedro@apple.com'),
            null,
            null,
            new Department('Engineering Support Services', '0056/7697'),
            null
        );

        $kate = new Person(
            new DsId('4'),
            'Kate Bergeron',
            new EmailAddress('kate.gergeron@apple.com'),
            null,
            null,
            new Department('Engineering Support Services', '0056/7698'),
            null
        );

        $manager->persist($peggy);
        $manager->persist($patrick);
        $manager->persist($pedro);
        $manager->persist($kate);
        $manager->flush();

        $this->addReference('person-peggy', $peggy);
        $this->addReference('person-patrick', $patrick);
        $this->addReference('person-pedro', $pedro);
    }
}
