<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Unit\Unit;

final class UnitFixture extends AbstractFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $unit1 = new Unit($this->getReference('person-peggy'), $this->getReference('build-1'));
        $unit1->addPtNumber('PT234567');
        $unit1->updateLabLocation($this->getReference('apple-park'));
        $unit1->confirm();

        $unit2 = new Unit($this->getReference('person-peggy'), $this->getReference('build-1'));
        $unit2->addPtNumber('PT345678');
        $unit2->updateLabLocation($this->getReference('apple-park'));
        $unit2->confirm();

        $unit3 = new Unit($this->getReference('person-peggy'), $this->getReference('build-1'));
        $unit3->addPtNumber('PT456789');
        $unit3->updateLabLocation($this->getReference('grand-central-station'));
        $unit3->confirm();

        $unit4 = new Unit($this->getReference('person-peggy'), $this->getReference('build-1'));
        $unit4->addPtNumber('PT567890');
        $unit4->updateLabLocation($this->getReference('grand-central-station'));
        $unit4->setITrackId(223453, 'state');

        $manager->persist($unit1);
        $manager->persist($unit2);
        $manager->persist($unit3);
        $manager->persist($unit4);
        $manager->flush();

        $this->addReference('unit-1', $unit1);
        $this->addReference('unit-2', $unit2);
        $this->addReference('unit-3', $unit3);
        $this->addReference('unit-4-itrack', $unit4);

        $labLocations = [
            $this->getReference('il06'),
            $this->getReference('vp02'),
            $this->getReference('apple-park'),
            $this->getReference('grand-central-station'),
        ];

        for ($i = 500000; $i < 500650; ++$i) {
            $labLocation = $labLocations[$i % 4];

            $unit = new Unit($this->getReference('person-peggy'), $this->getReference('build-1'));
            $unit->addPtNumber('PT' . $i);
            $unit->updateLabLocation($labLocation);
            $unit->confirm();

            if ($i >= 500250 && $i < 500260) {
                $unit->relocate($labLocations[($i + 1) % 4]);
            }

            $manager->persist($unit);
            $manager->flush();

            $this->addReference('unit-test-' . $i, $unit);
        }
    }

    public function getDependencies() : array
    {
        return [
            PersonFixture::class,
            BuildFixture::class,
        ];
    }
}
