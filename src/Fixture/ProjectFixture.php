<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Ticket\Priority;

final class ProjectFixture extends AbstractFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $project1 = new Project('P01', new Priority(4), null, null, 101);
        $project1->activate();

        $project99 = new Project('P99', new Priority(4), null, null);

        $project2 = new Project('P02', new Priority(3), $this->getReference('person-pedro'), null, 102);

        $manager->persist($project1);
        $manager->persist($project2);
        $manager->persist($project99);
        $manager->flush();

        $this->addReference('project-1', $project1);
        $this->addReference('project-2', $project2);
        $this->addReference('project-99', $project99);
    }

    public function getDependencies() : array
    {
        return [
            PersonFixture::class,
        ];
    }
}
