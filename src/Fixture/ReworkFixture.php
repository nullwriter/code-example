<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Rework\InspectionType;
use Pinpoint\Domain\Rework\Instructions;
use Pinpoint\Domain\Rework\Rework;
use Pinpoint\Domain\Rework\Source;

final class ReworkFixture extends AbstractFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $project = $this->getReference('project-1');
        $submittedBy = $this->getReference('person-patrick');
        $standardInstructions = new Instructions(
            false,
            Source::EMAIL(),
            false,
            false,
            InspectionType::STANDARD(),
            '2394875JIL39',
            'This is an example description.'
        );

        $rework1 = new Rework('Rework 1', true, $project, false, $submittedBy, null, null, null, $standardInstructions);
        $rework2 = new Rework('Rework 2', true, $project, true, $submittedBy, null, null, null, $standardInstructions);
        $rework3 = new Rework('Rework 3', false, $project, true, $submittedBy, null, null, null, $standardInstructions);

        $manager->persist($rework1);
        $manager->persist($rework2);
        $manager->persist($rework3);
        $manager->flush();

        $this->addReference('rework-1', $rework1);
        $this->addReference('rework-2', $rework2);
        $this->addReference('rework-3', $rework3);
    }

    public function getDependencies() : array
    {
        return [
            PersonFixture::class,
            ProjectFixture::class,
        ];
    }
}
