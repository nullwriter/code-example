<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Build\Build;

final class BuildFixture extends AbstractFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $build1 = new Build($this->getReference('project-1'), 'B01');
        $build1->setITrackId(101);
        $build1->updateReworks(...[
            $this->getReference('rework-1'),
            $this->getReference('rework-2'),
            $this->getReference('rework-3'),
        ]);

        $build99 = new Build($this->getReference('project-99'), 'B99');

        $manager->persist($build1);
        $manager->persist($build99);
        $manager->flush();

        $this->addReference('build-1', $build1);
        $this->addReference('build-99', $build99);
    }

    public function getDependencies() : array
    {
        return [
            ProjectFixture::class,
            ReworkFixture::class,
        ];
    }
}
