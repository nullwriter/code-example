<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Ticket\Batch\Batch;
use Pinpoint\Domain\Ticket\Batch\DisplayId;

final class BatchFixture extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $batch1 = new Batch(new DisplayId(900001));
        $batch2 = new Batch(new DisplayId(900002));
        $batch3 = new Batch(new DisplayId(900003));

        $manager->persist($batch1);
        $manager->persist($batch2);
        $manager->persist($batch3);
        $manager->flush();

        $this->addReference('batch-1', $batch1);
        $this->addReference('batch-2', $batch2);
        $this->addReference('batch-3', $batch3);
    }
}
