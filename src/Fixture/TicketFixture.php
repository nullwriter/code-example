<?php
declare(strict_types = 1);

namespace Pinpoint\Fixture;

use DateTimeImmutable;
use DateTimeZone;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Pinpoint\Domain\Email\Address;
use Pinpoint\Domain\Email\AddressList;
use Pinpoint\Domain\Email\Email;
use Pinpoint\Domain\Holiday\DateRange;
use Pinpoint\Domain\Holiday\SearchHolidaysByDateRangesInterface;
use Pinpoint\Domain\Ticket\Action;
use Pinpoint\Domain\Ticket\Age\AgeCalculator;
use Pinpoint\Domain\Ticket\Age\WorkPeriod;
use Pinpoint\Domain\Ticket\Batch\Batch;
use Pinpoint\Domain\Ticket\Batch\DisplayId as BatchDisplayId;
use Pinpoint\Domain\Ticket\DisplayId;
use Pinpoint\Domain\Ticket\Priority;
use Pinpoint\Domain\Ticket\Ticket;
use Pinpoint\Domain\Ticket\WorkRequest\DisplayId as WorkRequestDisplayId;

final class TicketFixture extends AbstractFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $peggy = $this->getReference('person-peggy');
        $patrick = $this->getReference('person-patrick');

        $ticket1 = new Ticket(
            new DisplayId(900001),
            new Priority(1),
            $this->getReference('batch-1'),
            $peggy,
            $peggy,
            $patrick,
            $peggy,
            $this->getReference('unit-1')
        );
        $ticket2 = new Ticket(
            new DisplayId(900002),
            new Priority(1),
            $this->getReference('batch-2'),
            $peggy,
            $peggy,
            $patrick,
            null,
            $this->getReference('unit-2')
        );
        $ticket3 = new Ticket(
            new DisplayId(900003),
            new Priority(2),
            $this->getReference('batch-3'),
            $peggy,
            $peggy,
            $patrick,
            $peggy,
            $this->getReference('unit-3')
        );

        $ticket4 = new Ticket(
            new DisplayId(900004),
            new Priority(5),
            $this->getReference('batch-3'),
            $peggy,
            $peggy,
            $patrick,
            $peggy,
            $this->getReference('unit-4-itrack')
        );

        $ageCalculator = new AgeCalculator(
            new DateTimeZone('UTC'),
            new class implements SearchHolidaysByDateRangesInterface
            {
                public function __invoke(DateRange ...$dateRanges) : array
                {
                    return [];
                }
            },
            new WorkPeriod(8, 12),
            new WorkPeriod(13, 18)
        );

        $ticket1->addRework(
            new WorkRequestDisplayId(900001),
            $this->getReference('rework-1'),
            null
        );

        $ticket2->transitionState($peggy, Action::WORK_COMPLETE(), $ageCalculator);
        $ticket3->addPickupAuthorization($peggy, $patrick);
        $ticket3->markAsPickedUp($peggy, new DateTimeImmutable(), true);

        $ticket1->addPickupAuthorization($peggy, $patrick);
        $ticket2->addPickupAuthorization($peggy, $patrick);

        $ticket1->addEmail((new Email(
            'Test Subject',
            AddressList::fromString('jlavere@soliantconsulting.com'),
            AddressList::fromString(''),
            AddressList::fromString(''),
            Address::fromString('jlavere@soliantconsulting.com'),
            Address::fromString('jlavere@soliantconsulting.com'),
            'Eat <span>greens</span>!',
            'Eat greens!'
        ))->asSent());

        $ticket4->addPickupAuthorization($peggy, $patrick);
        $ticket4->transitionState($peggy, Action::WORK_COMPLETE(), $ageCalculator);
        $ticket4->transitionState($peggy, Action::CLOSE(), $ageCalculator);

        $manager->persist($ticket1);
        $manager->persist($ticket2);
        $manager->persist($ticket3);
        $manager->persist($ticket4);
        $manager->flush();

        $this->addReference('ticket-1', $ticket1);
        $this->addReference('ticket-2', $ticket2);
        $this->addReference('ticket-3', $ticket3);

        for ($i = 500000; $i < 500650; ++$i) {
            $unit = $this->getReference('unit-test-' . $i);
            $batch = new Batch(new BatchDisplayId($i));
            $ticket = new Ticket(new DisplayId($i), new Priority(2), $batch, $peggy, $peggy, $patrick, $peggy, $unit);

            $ticket->addPickupAuthorization($peggy, $patrick);

            if ($i < 500250) {
                $ticket->transitionState($peggy, Action::WORK_COMPLETE(), $ageCalculator);
                $ticket->transitionState($peggy, Action::CLOSE(), $ageCalculator);
            } elseif ($i >= 500250 && $i < 500400) {
                $ticket->addPickupAuthorization($peggy, $patrick);
            } else {
                $ticket->addPickupAuthorization($peggy, $patrick);
                $ticket->transitionState($peggy, Action::WORK_COMPLETE(), $ageCalculator);
            }

            $manager->persist($batch);
            $manager->persist($ticket);
            $manager->flush();
        }

        // For performance testing, we'll add 100k tickets. Only enable if you need it, but keep it disabled within
        // the repository.
        if (true) {
            return;
        }

        $unit = $this->getReference('unit-1');

        for ($i = 800000; $i < 810000; ++$i) {
            $batch = new Batch(new BatchDisplayId($i));
            $ticket = new Ticket(new DisplayId($i), new Priority(2), $batch, $peggy, $peggy, $patrick, $peggy, $unit);
            $manager->persist($batch);
            $manager->persist($ticket);

            if (0 === $i % 1000) {
                $manager->flush();
                echo '.';
            }
        }
    }

    public function getDependencies() : array
    {
        return [
            BatchFixture::class,
            UnitFixture::class,
            PersonFixture::class,
            LabLocationFixture::class,
            ReworkFixture::class,
        ];
    }
}
