<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\LabLocation;

interface GetLabLocationStatisticsInterface
{
    public function __invoke(LabLocation $labLocation) : LabLocationStatistics;
}
