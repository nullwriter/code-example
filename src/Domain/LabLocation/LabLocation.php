<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\LabLocation;

use Pinpoint\Domain\Id;
use Pinpoint\Domain\Ticket\Age\Age;

/**
 * @final
 */
class LabLocation
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $canPickupWithoutScan;

    /**
     * @var Age
     */
    private $averageTicketTurnAroundTime;

    /**
     * @var int
     */
    private $totalTicketsOfAverageTicketTurnAroundTime = 0;

    /**
     * @var string
    */
    private $billboardMessage;

    public function __construct(string $name, bool $canPickupWithoutScan, string $billboardMessage = '')
    {
        $this->id = Id::generate();
        $this->name = $name;
        $this->canPickupWithoutScan = $canPickupWithoutScan;
        $this->averageTicketTurnAroundTime = new Age(0, 1);
        $this->billboardMessage = $billboardMessage;
    }

    public function getId() : Id
    {
        return $this->id;
    }

    public function updateCanPickupWithoutScan(bool $canPickupWithoutScan) : void
    {
        $this->canPickupWithoutScan = $canPickupWithoutScan;
    }

    public function canPickupWithoutScan() : bool
    {
        return $this->canPickupWithoutScan;
    }

    public function addToAverageTicketTurnAroundTime(Age $age) : void
    {
        $this->averageTicketTurnAroundTime = new Age(
            intdiv(
                ($this->averageTicketTurnAroundTime->getSeconds() * $this->totalTicketsOfAverageTicketTurnAroundTime)
                + $age->getSeconds(),
                ++$this->totalTicketsOfAverageTicketTurnAroundTime
            ),
            $age->getHoursPerDay()
        );
    }

    public function subtractFromAverageTicketTurnAroundTime(Age $age) : void
    {
        if (1 === $this->totalTicketsOfAverageTicketTurnAroundTime) {
            $this->averageTicketTurnAroundTime = new Age(0, 1);
            $this->totalTicketsOfAverageTicketTurnAroundTime = 0;
            return;
        }

        $this->averageTicketTurnAroundTime = new Age(
            intdiv(
                ($this->averageTicketTurnAroundTime->getSeconds() * $this->totalTicketsOfAverageTicketTurnAroundTime)
                - $age->getSeconds(),
                --$this->totalTicketsOfAverageTicketTurnAroundTime
            ),
            $age->getHoursPerDay()
        );
    }

    public function getAverageTicketTurnAroundTime() : Age
    {
        return $this->averageTicketTurnAroundTime;
    }

    public function getMessage() : string
    {
        return $this->billboardMessage;
    }

    public function updateMessage(string $billboardMessage) : void
    {
        $this->billboardMessage = $billboardMessage;
    }

    public function __toString() : string
    {
        return $this->name;
    }
}
