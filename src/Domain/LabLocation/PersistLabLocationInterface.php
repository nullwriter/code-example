<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\LabLocation;

interface PersistLabLocationInterface
{
    public function __invoke(LabLocation $labLocation) : void;
}
