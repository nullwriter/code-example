<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\LabLocation;

use Pinpoint\Domain\Id;

interface SearchLabLocationsByIdInterface
{
    public function __invoke(Id $id) : ?LabLocation;
}
