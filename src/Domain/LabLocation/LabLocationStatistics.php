<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\LabLocation;

final class LabLocationStatistics
{
    /**
     * @var LabLocation
     */
    private $labLocation;

    /**
     * @var int
     */
    private $completedTickets;

    /**
     * @var int
     */
    private $completedNonITrackedTickets;

    /**
     * @var int
     */
    private $completedITrackedTickets;

    public function __construct(
        LabLocation $labLocation,
        int $completedTickets,
        int $completedNonITrackedTickets,
        int $completedITrackedTickets
    ) {
        $this->labLocation = $labLocation;
        $this->completedTickets = $completedTickets;
        $this->completedNonITrackedTickets = $completedNonITrackedTickets;
        $this->completedITrackedTickets = $completedITrackedTickets;
    }

    public function getLabLocation() : LabLocation
    {
        return $this->labLocation;
    }

    public function getCompletedTicketsCount() : int
    {
        return $this->completedTickets;
    }

    public function getCompletedNonITrackedTicketsCount() : int
    {
        return $this->completedNonITrackedTickets;
    }

    public function getCompletedITrackedTicketsCount() : int
    {
        return $this->completedITrackedTickets;
    }
}
