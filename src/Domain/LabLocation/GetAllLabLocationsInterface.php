<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\LabLocation;

interface GetAllLabLocationsInterface
{
    /**
     * @return LabLocation[]
     */
    public function __invoke() : array;
}
