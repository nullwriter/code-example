<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Ticket;

interface SearchTicketByDisplayIdInterface
{
    public function __invoke(DisplayId $displayId) : ?Ticket;
}
