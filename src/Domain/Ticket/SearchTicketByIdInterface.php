<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Ticket;

use Pinpoint\Domain\Id;

interface SearchTicketByIdInterface
{
    public function __invoke(Id $id) : ?Ticket;
}
