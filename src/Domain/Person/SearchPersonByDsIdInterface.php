<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Person;

interface SearchPersonByDsIdInterface
{
    public function __invoke(DsId $dsId) : ?Person;
}
