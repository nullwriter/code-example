<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Person;

use Pinpoint\Domain\Id;

interface SearchPersonByIdInterface
{
    public function __invoke(Id $personId) : ?Person;
}
