<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Project;

use Pinpoint\Domain\Id;

interface SearchProjectByIdInterface
{
    public function __invoke(Id $id) : ?Project;
}
