<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Project;

interface PersistProjectInterface
{
    public function __invoke(Project $project) : void;
}
