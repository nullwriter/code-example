<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Project;

interface GetAllProjectTypesInterface
{
    /**
     * @return Type[]
     */
    public function __invoke() : array;
}
