<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Project;

interface GetAllProjectsInterface
{
    /**
     * @return Project[]
     */
    public function __invoke() : array;
}
