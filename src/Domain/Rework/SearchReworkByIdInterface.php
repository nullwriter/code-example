<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Rework;

use Pinpoint\Domain\Id;

interface SearchReworkByIdInterface
{
    public function __invoke(Id $reworkId) : ?Rework;
}
