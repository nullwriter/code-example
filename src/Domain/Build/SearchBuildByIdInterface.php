<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build;

use Pinpoint\Domain\Id;

interface SearchBuildByIdInterface
{
    public function __invoke(Id $buildId) : ?Build;
}
