<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build\Exception;

use DomainException;

final class ITrackIdNotSetException extends DomainException implements ExceptionInterface
{
    public static function fromMissingITrackId() : self
    {
        return new self('The iTrack ID has not been set on this build and thus cannot be refreshed');
    }
}
