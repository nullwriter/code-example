<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build\Exception;

use DomainException;

final class ITrackIdAlreadySetException extends DomainException implements ExceptionInterface
{
    public static function fromAlreadySetITrackId() : self
    {
        return new self('The iTrack ID has already been set on this build');
    }
}
