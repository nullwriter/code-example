<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build\Exception;

use DomainException;
use Pinpoint\Domain\Project\Project;

final class NonITrackedProjectException extends DomainException implements ExceptionInterface
{
    public static function fromNonITrackedProject(Project $project) : self
    {
        return new self(sprintf('The project %s is not iTracked, thus this build cannot be iTracked either', $project));
    }
}
