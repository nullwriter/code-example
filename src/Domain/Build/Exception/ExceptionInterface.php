<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build\Exception;

use Throwable;

interface ExceptionInterface extends Throwable
{
}
