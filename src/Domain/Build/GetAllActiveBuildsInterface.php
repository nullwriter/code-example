<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build;

interface GetAllActiveBuildsInterface
{
    /**
     * @return Build[]
     */
    public function __invoke() : array;
}
