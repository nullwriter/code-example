<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Pinpoint\Domain\Build\Exception\ITrackIdAlreadySetException;
use Pinpoint\Domain\Build\Exception\ITrackIdNotSetException;
use Pinpoint\Domain\Build\Exception\NonITrackedProjectException;
use Pinpoint\Domain\Helper;
use Pinpoint\Domain\Id;
use Pinpoint\Domain\Project\Project;
use Pinpoint\Domain\Rework\Rework;

/**
 * @final
 */
class Build
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var int|null
     */
    private $iTrackId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Rework[]|Collection
     */
    private $reworks;

    public function __construct(Project $project, string $name)
    {
        $this->id = Id::generate();
        $this->project = $project;
        $this->name = $name;
        $this->reworks = new ArrayCollection();
    }

    public function getId() : Id
    {
        return $this->id;
    }

    public function setITrackId(int $iTrackId) : void
    {
        if (! $this->project->isITracked()) {
            throw NonITrackedProjectException::fromNonITrackedProject($this->project);
        }

        if (null !== $this->iTrackId) {
            throw ITrackIdAlreadySetException::fromAlreadySetITrackId();
        }

        $this->iTrackId = $iTrackId;
    }

    public function refreshITrackId(int $iTrackId) : void
    {
        if (null === $this->iTrackId) {
            throw ITrackIdNotSetException::fromMissingITrackId();
        }

        $this->iTrackId = $iTrackId;
    }

    public function isITracked() : bool
    {
        return null !== $this->iTrackId;
    }

    public function getITrackId() : ?int
    {
        return $this->iTrackId;
    }

    public function getProject() : Project
    {
        return $this->project;
    }

    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return Rework[]
     */
    public function getReworks() : array
    {
        return iterator_to_array($this->reworks);
    }

    public function removeRework(Rework $rework) : void
    {
        $this->reworks->removeElement($rework);
    }

    public function updateReworks(Rework ...$reworks) : void
    {
        Helper::replaceOwningCollection($this->reworks, $reworks);
    }

    public function __toString() : string
    {
        return $this->name;
    }
}
