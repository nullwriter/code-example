<?php
declare(strict_types = 1);

namespace Pinpoint\Domain\Build;

interface PersistBuildInterface
{
    public function __invoke(Build $build) : void;
}
