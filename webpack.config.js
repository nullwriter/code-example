const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        'Common': 'app/Common.js',

        'Kiosk/ChoosePath': 'app/Kiosk/ChoosePath.js',
        'Kiosk/DropOff': 'app/Kiosk/DropOff.js',
        'Kiosk/DropOffSummary': 'app/Kiosk/DropOffSummary.js',
        'Kiosk/Home': 'app/Kiosk/Home.js',
        'Kiosk/Pickup': 'app/Kiosk/Pickup.js',
        'Kiosk/ScanMock': 'app/Kiosk/ScanMock.js',
        'Kiosk/SelectLabLocation': 'app/Kiosk/SelectLabLocation.js',
        'Kiosk/Timeout': 'app/Kiosk/Timeout.js',

        'Staff/Admin/SetCustomMessage': 'app/Staff/Admin/SetCustomMessage.js',
        'Staff/Common': 'app/Staff/Common.js',
        'Staff/SelectLabLocation': 'app/Staff/SelectLabLocation.js',
        'Staff/Ticket/List': 'app/Staff/Ticket/List.js',
        'Staff/Ticket/Detail': 'app/Staff/Ticket/Detail.js',
        'Staff/Unit/Detail': 'app/Staff/Unit/Detail.js',
        'Staff/Project/List': 'app/Staff/Project/List.js',
        'Staff/Project/Detail': 'app/Staff/Project/Detail.js',
        'Staff/Rework/List': 'app/Staff/Rework/List.js',
        'Staff/Rework/Detail': 'app/Staff/Rework/Detail.js',
        'Staff/PtNumber/List': 'app/Staff/PtNumber/List.js',

        'Billboard/Billboard': 'app/Billboard/Billboard.js',
    },
    resolve: {
        modules: [
            '.',
            'es6',
            'node_modules',
        ],
    },
    output: {
        path: path.resolve(__dirname, 'public/dist/'),
        filename: '[name].bundle.js',
        publicPath: '/dist/',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015'],
                },
            },
            {
                test: /\.html$/,
                loader: ['html-loader'],
            },
            {
                test: /\.mp3$/,
                loader: ['file-loader'],
            },
            {
                test: /\.jpg$/,
                loader: ['file-loader'],
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader'],
            },
            {
                test: /\.png$/,
                loader: 'url-loader?limit=100000',
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader',
                query: {
                    limit: 10000,
                },
            },
        ],
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'Lib',
            minChunks: 2,
            filename: '[name].js',
        }),

        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            moment: 'moment',
        }),
    ]
};
