<?php
require __DIR__ . '/../vendor/autoload.php';

/* @var $container Psr\Container\ContainerInterface */
$container = require __DIR__ . '/../config/container.php';

/* @var $cache Psr\SimpleCache\CacheInterface */
$cache = $container->get(Psr\SimpleCache\CacheInterface::class);
$cache->clear();
