<?php
require __DIR__ . '/../vendor/autoload.php';

/* @var $container Psr\Container\ContainerInterface */
$container = require __DIR__ . '/../config/container.php';

/* @var $notificationQueueSender Pinpoint\Infrastructure\Mailer\NotificationQueueSender */
$notificationQueueSender = $container->get(Pinpoint\Infrastructure\Mailer\NotificationQueueSender::class);
$notificationQueueSender();
