<?php
require __DIR__ . '/../vendor/autoload.php';

$console = Zend\Console\Console::getInstance();
$console->writeLine('This utility script let\'s you generate password hashes.');

do {
    $password = Zend\Console\Prompt\Password::prompt('Password: ', true);
} while ('' === $password);

do {
    $confirmPassword = Zend\Console\Prompt\Password::prompt('Confirm password: ', true);
} while ('' === $confirmPassword);

if ($password !== $confirmPassword) {
    $console->writeLine('The entered passwords do not match.', Zend\Console\ColorInterface::RED);
    exit(1);
}

$console->writeLine('Resulting password hash:');
$console->writeLine(password_hash($password, PASSWORD_DEFAULT));
