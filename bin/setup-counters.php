<?php
$counters = [
    Pinpoint\Domain\Ticket\DisplayId::class => 1,
    Pinpoint\Domain\Ticket\Batch\DisplayId::class => 1,
    Pinpoint\Domain\Ticket\WorkRequest\DisplayId::class => 1,
];

require __DIR__ . '/../vendor/autoload.php';

/* @var $container Psr\Container\ContainerInterface */
$container = require __DIR__ . '/../config/container.php';

/* @var $entityManager Doctrine\ORM\EntityManagerInterface */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);

$reflectionClass = new ReflectionClass(Pinpoint\Domain\Counter\Counter::class);
$identifierProperty = new ReflectionProperty(Pinpoint\Domain\Counter\Counter::class, 'identifier');
$identifierProperty->setAccessible(true);
$nextValueProperty = new ReflectionProperty(Pinpoint\Domain\Counter\Counter::class, 'nextValue');
$nextValueProperty->setAccessible(true);

foreach ($counters as $identifier => $nextValue) {
    $counter = $entityManager->find(Pinpoint\Domain\Counter\Counter::class, $identifier);

    if (null === $counter) {
        $counter = $reflectionClass->newInstanceWithoutConstructor();
        $identifierProperty->setValue($counter, $identifier);
        $nextValueProperty->setValue($counter, $nextValue);
    } elseif ($nextValueProperty->getValue($counter) < $nextValue) {
        $nextValueProperty->setValue($counter, $nextValue);
    } else {
        continue;
    }

    $entityManager->persist($counter);
}

$entityManager->flush();
