<?php
require __DIR__ . '/../vendor/autoload.php';

/* @var $container Psr\Container\ContainerInterface */
$container = require __DIR__ . '/../config/container.php';

foreach (glob(__DIR__ . '/../data/cache/DoctrineEntityProxy/*') as $proxyPath) {
    unlink($proxyPath);
}

/* @var $entityManager Doctrine\ORM\EntityManagerInterface */
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);
$connection = $entityManager->getConnection();
$connection->exec('DROP SCHEMA IF EXISTS public CASCADE');
$connection->exec('CREATE SCHEMA public');
$connection->exec('GRANT ALL ON SCHEMA public TO postgres');
$connection->exec('GRANT ALL ON SCHEMA public TO public');

$schemaTool = new Doctrine\ORM\Tools\SchemaTool($entityManager);
$schemaTool->createSchema($entityManager->getMetadataFactory()->getAllMetadata());

$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.6__Ticket_Search.sql'));
$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.5__Project_Search.sql'));
$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.7__Rework_Search.sql'));
$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.8__Pt_Number_Search.sql'));

$loader = new Doctrine\Common\DataFixtures\Loader();
$loader->loadFromDirectory(__DIR__ . '/../src/Fixture');

$uuidFactory = new Ramsey\Uuid\UuidFactory();
$uuidFactory->setRandomGenerator(new Pinpoint\Fixture\Uuid\PredictableRandomGenerator());
Ramsey\Uuid\Uuid::setFactory($uuidFactory);

$executor = new Doctrine\Common\DataFixtures\Executor\ORMExecutor(
    $entityManager,
    new Doctrine\Common\DataFixtures\Purger\ORMPurger()
);
$executor->execute($loader->getFixtures());

$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.3__Holidays.sql'));
$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.4__Project_Types.sql'));
$connection->exec(file_get_contents(__DIR__ . '/../database/V1.0.0.2__Constraints.sql'));

/* @var $cache Psr\SimpleCache\CacheInterface */
$cache = $container->get(Psr\SimpleCache\CacheInterface::class);
$cache->clear();

require __DIR__ . '/setup-counters.php';
require __DIR__ . '/load-lab-locations.php';
