<?php
use Pinpoint\Import\Entity;

require __DIR__ . '/../vendor/autoload.php';

// Because of non-unix line endings in CSV exports…
ini_set('auto_detect_line_endings', true);

// The ID maps will take up a lot of memory…
ini_set('memory_limit', '1G');

$logger = new Zend\Log\PsrLoggerAdapter(
    new Zend\Log\Logger([
        'writers' => [
            [
                'name' => 'stream',
                'options' => [
                    'stream' => 'php://stdout',
                ],
            ],
        ],
    ])
);

/* @var $container Psr\Container\ContainerInterface */
$container = require __DIR__ . '/../config/container.php';

/* @var $entityManager Doctrine\ORM\EntityManagerInterface */
$logger->info('Pruning existing schema…');
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);
$connection = $entityManager->getConnection();
$connection->exec('DROP SCHEMA IF EXISTS public CASCADE');
$connection->exec('CREATE SCHEMA public');
$connection->exec('GRANT ALL ON SCHEMA public TO postgres');
$connection->exec('GRANT ALL ON SCHEMA public TO public');

$logger->info('Creating new schema');
$schemaTool = new Doctrine\ORM\Tools\SchemaTool($entityManager);
$schemaTool->createSchema($entityManager->getMetadataFactory()->getAllMetadata());

$importRunner = new Pinpoint\Import\ImportRunner(
    $connection,
    $logger,
    new DateTimeZone('America/Los_Angeles'),
    'ISO-8859-1'
);

$personImport = new Entity\PersonImport($connection);
$projectImport = new Entity\ProjectImport($connection, $personImport);
$buildLevelImport = new Entity\BuildLevelImport($connection, $personImport, $projectImport);
$reworkImport = new Entity\ReworkImport($connection, $personImport, $projectImport);
$reworkBuildLevelImport = new Entity\ReworkBuildLevelImport(
    $connection,
    $personImport,
    $reworkImport,
    $buildLevelImport
);
$reworkSessionImport = new Entity\ReworkSessionImport($connection, $personImport, $reworkImport);
$documentImport = new Entity\DocumentImport($connection, $personImport, $reworkImport);
$ptNumberImport = new Entity\PtNumberImport($connection, $personImport);
$hrpNumberImport = new Entity\HrpNumberImport($connection, $personImport);
$unitImport = new Entity\UnitImport($connection, $personImport, $buildLevelImport, $ptNumberImport, $hrpNumberImport);
$ticketImport = new Entity\TicketImport($connection, $unitImport, $personImport);
$workRequestImport = new Entity\WorkRequestImport($connection, $personImport, $ticketImport);
$radarTicketImport = new Entity\RadarTicketImport($connection, $personImport, $workRequestImport);
$activityEventImport = new Entity\ActivityEventImport($connection, $personImport, $ticketImport);
$componentImport = new Entity\ComponentImport($connection, $personImport, $ticketImport, $workRequestImport);
$photoImport = new Entity\PhotoImport($connection, $personImport, $ticketImport);
$partImport = new Entity\PartImport($connection, $personImport, $ticketImport);
$statusEventImport = new Entity\StatusEventImport($connection, $personImport, $ticketImport);
$pickupAuthorizationImport = new Entity\PickupAuthorizationImport($connection, $personImport, $ticketImport);
$emailImport = new Entity\EmailImport($connection, $personImport, $ticketImport);
$commentImport = new Entity\CommentImport($connection, $personImport, $ticketImport, $workRequestImport);

//$importRunner('persons', $personImport, __DIR__ . '/../import/person.csv');

/* Commented out until required
$importRunner('projects', $projectImport, __DIR__ . '/../import/PRJ__Project.csv');
$importRunner('build levels', $buildLevelImport, __DIR__ . '/../import/BL__BuildLevel.csv');
$importRunner('reworks', $reworkImport, __DIR__ . '/../import/RWK__Rework.csv');
$importRunner('rework build levels', $reworkBuildLevelImport, __DIR__ . '/../import/RWKBL__Rework_BuildLevel.csv');
$importRunner('rework sessions', $reworkSessionImport, __DIR__ . '/../import/SSNRWW__Session_Rework.csv');
$importRunner('documents', $documentImport, __DIR__ . '/../import/DOC__Documents.csv');
$importRunner('PT numbers', $ptNumberImport, __DIR__ . '/../import/PT__Ptnumber.csv');
$importRunner('HRP numbers', $hrpNumberImport, __DIR__ . '/../import/HRP__HRPnumber.csv');
$importRunner('units', $unitImport, __DIR__ . '/../import/UNI__Unit.csv');
$importRunner('tickets', $ticketImport, __DIR__ . '/../import/TIK__Ticket.csv');
$importRunner('work requests', $workRequestImport, __DIR__ . '/../import/WKR__WorkRequest.csv');
$importRunner('radar tickets', $radarTicketImport, __DIR__ . '/../import/RDR__RadarTicket.csv');
$importRunner('activity events', $activityEventImport, __DIR__ . '/../import/AL__ActivityLog.csv');
$importRunner('components', $componentImport, __DIR__ . '/../import/CMP__Component.csv');
$importRunner('photos', $photoImport, __DIR__ . '/../import/PHT__Photo.csv');
$importRunner('parts', $partImport, __DIR__ . '/../import/PRT__Part.csv');
$importRunner('status events', $statusEventImport, __DIR__ . '/../import/SSH__StatusHistory.csv');
$importRunner('pickup authorizations', $pickupAuthorizationImport, __DIR__ . '/../import/PAU__PickupAuth.csv');
$importRunner('emails', $emailImport, __DIR__ . '/../import/EML__Email.csv');
$importRunner('comments', $commentImport, __DIR__ . '/../import/COM__Comment.csv');
*/
