<?php
require __DIR__ . '/../vendor/autoload.php';

/* @var $container Psr\Container\ContainerInterface */
$container = require __DIR__ . '/../config/container.php';

$config = (new DASPRiD\TreeReader\TreeReader($container->get('config'), 'config'))->getChildren('lab_location');
$entityManager = $container->get(Doctrine\ORM\EntityManagerInterface::class);
$repository = $entityManager->getRepository(Pinpoint\Domain\LabLocation\LabLocation::class);

foreach ($config->getChildren('locations') as $locationItem) {
    $locationChildren = $locationItem->getChildren();
    $labLocation = $repository->findOneBy(['name' => $locationChildren->getString('name')]);

    if (null === $labLocation) {
        $entityManager->persist(new Pinpoint\Domain\LabLocation\LabLocation(
            $locationChildren->getString('name'),
            $locationChildren->getBool('can_pick_up_without_scan', false)
        ));
        continue;
    }

    $labLocation->updateCanPickupWithoutScan($locationChildren->getBool('can_pick_up_without_scan', false));
    $entityManager->persist($labLocation);
}

$entityManager->flush();
