<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg">

<!-- Page layout information -->
<xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <fo:layout-master-set>
        <fo:simple-page-master master-name="main" page-height="30cm" page-width="7cm" font-family="sans-serif" margin-top="0.5cm"
                               margin-bottom="0.5cm" margin-left="0.5cm" margin-right="0.5cm">
            <fo:region-body margin-top="0.0cm" margin-bottom="1cm" />
            <fo:region-before extent="1.5cm" />
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="main">
        <fo:flow flow-name="xsl-region-body">
            <xsl:for-each select="data">
                <xsl:apply-templates select="batch" />
            </xsl:for-each>
        </fo:flow>
    </fo:page-sequence>

    </fo:root>
</xsl:template>

<xsl:template match="ticket-id">
    <fo:block>
        <xsl:value-of select="." />
    </fo:block>
</xsl:template>

<xsl:template match="batch">
    <fo:block font-size="10pt" text-align="center">
        <xsl:value-of select="batch-id" /><xsl:text> </xsl:text><xsl:value-of select="batch-id" /><xsl:text> </xsl:text><xsl:value-of select="batch-id" />
    </fo:block>
    <fo:block font-size="12pt" text-align="center" margin-top="0.4cm">
        <xsl:value-of select="timestamp" />
    </fo:block>
    <fo:block font-size="12pt" text-align="center" margin-top="0.4cm" font-weight="bold" >
        <xsl:value-of select="location" />
    </fo:block>
    <fo:block font-size="12pt" text-align="center" margin-top="0.4cm" >
        <xsl:value-of select="customer-person" />
    </fo:block>
    <fo:block font-size="10pt" text-align="center" margin-top="0.6cm">
        <fo:inline>List of Tickets in Batch </fo:inline>
        <xsl:value-of select="batch-id" />
    </fo:block>

    <fo:block font-size="12pt" text-align="center" margin-top="0.3cm">
        <xsl:for-each select="ticket-list">
            <xsl:apply-templates select="ticket-id" />
        </xsl:for-each>
    </fo:block>

    <fo:block font-size="10pt" text-align="center" margin-top="0.3cm">
        <xsl:value-of select="numUnits" /> Tickets in <xsl:value-of select="batch-id" />
    </fo:block>
    <fo:block font-family="Free3of9" text-align="center" margin-top="0.5cm" font-size="30pt">
        <xsl:value-of select="barcode" />
    </fo:block>
    <fo:block font-size="18pt" text-align="center" margin-top="0.8cm" font-weight="bold">
        <xsl:value-of select="batch-id" />
    </fo:block>

    <fo:block font-size="14pt" text-align="center" margin-top="0.4cm">

        <fo:block font-size="14pt" text-align="center">
            <fo:instream-foreign-object>
                <svg:svg xmlns:svg="http://www.w3.org/2000/svg" width="30" height="30" xml:space="preserve">
                        <svg:g style="fill:none; stroke:black; stroke-width:1">
                            <svg:rect x="0" y="0" width="30" height="30"/>
                        </svg:g>
                </svg:svg>
            </fo:instream-foreign-object>
            <fo:inline baseline-shift="9pt"> Mark If Closed</fo:inline>
        </fo:block>

    </fo:block>

    <fo:block font-size="10pt" text-align="center" margin-top="0.4cm">
        <xsl:value-of select="batch-id" /><xsl:text> </xsl:text><xsl:value-of select="batch-id" /><xsl:text> </xsl:text><xsl:value-of select="batch-id" />
    </fo:block>
    <fo:block font-size="10pt" text-align="center" page-break-after="always">
        .................................................................
    </fo:block>
</xsl:template>

</xsl:stylesheet>
